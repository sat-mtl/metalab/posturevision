#include "triggergrabber.h"
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/ply_io.h>

#include <chrono>

namespace posture
{
    TriggerGrabber::TriggerGrabber(float xMax, float yMax, float zMax, std::string modelPath):
        _running(true),
        _modelPath(modelPath),
        _xMax(xMax),
        _yMax(yMax),
        _zMax(zMax)
    {
        _noiseSignal = createSignal<void (const boost::shared_ptr<const pcl::PointCloud<pcl::PointXYZRGBA>>&)>();
    }

    TriggerGrabber::~TriggerGrabber() {}

    void TriggerGrabber::stop ()
    {
        _running = false;
    }

    std::string TriggerGrabber::getName() const
    {
        return "TriggerGrabber";
    }

    bool TriggerGrabber::isRunning() const
    {
        return _running;
    }

    float TriggerGrabber::getFramesPerSecond() const
    {
        return 0.0f;
    }

    void TriggerGrabber::signalsChanged()
    {
        if (_running)
            start();
    }

    void TriggerGrabber::update()
    {
        _thread = std::make_shared<std::thread>(&TriggerGrabber::start, this);
        _thread->join();
    }

    void TriggerGrabber::start()
    {
        while (_running)
        {
            if (!_modelPath.empty())
            {
                boost::shared_ptr<const pcl::PointCloud<pcl::PointXYZRGBA>> cloud = loadFromPLY(_modelPath);
                _noiseSignal->operator()(cloud);
            }
            else
            {
                boost::shared_ptr<const pcl::PointCloud<pcl::PointXYZRGBA>> cloud = generateNoise();
                _noiseSignal->operator()(cloud);
            }
        }
    }

    boost::shared_ptr<const pcl::PointCloud<pcl::PointXYZRGBA>> TriggerGrabber::loadFromPLY(std::string path) const
    {
        std::shared_ptr<pcl::PointCloud<pcl::PointXYZ>> inputCloud(new pcl::PointCloud<pcl::PointXYZ>());
        pcl::io::loadPLYFile(path, *inputCloud);

        boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGBA>> cloud(new pcl::PointCloud<pcl::PointXYZRGBA>());
        size_t pointCount = inputCloud->size();
        size_t width = ceil(std::sqrt(pointCount));
        size_t height = width;
        cloud->width = width;
        cloud->height = height;
        cloud->is_dense = true;
        cloud->resize(width * height);
        for (size_t i = 0; i < pointCount; i++)
        {
            cloud->points[i].x = inputCloud->points[i].x;
            cloud->points[i].y = inputCloud->points[i].y;
            cloud->points[i].z = inputCloud->points[i].z;

            cloud->points[i].r = 255.0f;
            cloud->points[i].g = 255.0f;
            cloud->points[i].b = 255.0f;
            cloud->points[i].a = 255.0f;
        }

        return cloud;
    }

    boost::shared_ptr<const pcl::PointCloud<pcl::PointXYZRGBA>> TriggerGrabber::generateNoise() const
    {
        // Initialize some data
        boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGBA>> cloud(new pcl::PointCloud<pcl::PointXYZRGBA>());
        cloud->width = _xMax * 100;
        cloud->height = _zMax * 100;
        size_t pointCount = cloud->width * cloud->height;
        cloud->is_dense = true;

        cloud->resize(pointCount);
        for (int i = 0; i < pointCount; ++i)
        {
            int plane = rand() % 3 + 1;
            float sign = round(((float)rand() / RAND_MAX)) * 2.0f - 1.0f;
            float x,y,z;

            switch (plane)
            {
                case 1:
                {
                    x = ((float)rand() / RAND_MAX)*0.01+(_xMax/2.0f) * sign;
                    y = (float)rand() / RAND_MAX*_yMax-(_yMax/2.0f);
                    z = (float)rand() / RAND_MAX*_zMax-(_zMax/2.0f);
                }
                break;
                case 2:
                {
                    x = (float)rand() / RAND_MAX*_xMax-(_xMax/2.0f);
                    y = ((float)rand() / RAND_MAX)*0.01+(_yMax/2.0f) * sign;
                    z = (float)rand() / RAND_MAX*_zMax-(_zMax/2.0f);
                }
                break;
                case 3:
                {
                    x = (float)rand() / RAND_MAX*_xMax-(_xMax/2.0f);
                    y = (float)rand() / RAND_MAX*_yMax-(_yMax/2.0f);
                    z = ((float)rand() / RAND_MAX)*0.01+(_zMax/2.0f) * sign;
                }
                break;
            }

            cloud->points[i].x = x;
            cloud->points[i].y = y;
            cloud->points[i].z = z;

            cloud->points[i].r = (float)rand() / ((float)RAND_MAX / (255.0f));
            cloud->points[i].g = (float)rand() / ((float)RAND_MAX / (255.0f));
            cloud->points[i].b = (float)rand() / ((float)RAND_MAX / (255.0f));
            cloud->points[i].a = 255.0f;
        }

        return cloud;
    }
}
