#include "meshmerger.h"
#include "meshmerger-impl.h"

using namespace std;

namespace posture
{
    /*************/
    MeshMerger::MeshMerger(const unsigned int meshNbr)
    {
        _impl = make_shared<MeshMergerImpl>(meshNbr);
    }
    
    /*************/
    MeshMerger::~MeshMerger()
    {
    }

    /*************/
    void MeshMerger::setApplyCalibration(bool apply) const
    {
        if (!_impl)
            return;
        
        _impl->setApplyCalibration(apply);
    }
    
    /*************/
    void MeshMerger::setCalibration(const vector<CalibrationParams>& calibrations) const
    {
        if (!_impl)
            return;
        
        return _impl->setCalibration(calibrations);
    }

    /*************/
    void MeshMerger::setInputMesh(const unsigned int index, const vector<unsigned char>& mesh) const
    {
        if (!_impl)
            return;

        _impl->setInputMesh(index, mesh);
    }

    /*************/
    void MeshMerger::setInputMesh(const unsigned int index, const pcl::PolygonMesh::Ptr mesh) const
    {
        if (!_impl)
            return;

        _impl->setInputMesh(index, mesh);
    }

    /*************/
    void MeshMerger::setInputMesh(const unsigned int index, const pcl::TextureMesh::Ptr mesh) const
    {
        if (!_impl)
            return;

        _impl->setInputMesh(index, mesh);
    }

    /*************/
    void MeshMerger::setInputMesh(const unsigned int index, vector<unsigned char>&& mesh) const
    {
        if (!_impl)
            return;

        _impl->setInputMesh(index, std::move(mesh));
    }

    /*************/
    unsigned long long MeshMerger::getMesh(std::vector<unsigned char>& mesh) const
    {
        if (!_impl)
            return 0;

        return _impl->getMesh(mesh);
    }

    /*************/
    unsigned long long MeshMerger::getMesh(pcl::TextureMesh::Ptr mesh) const
    {
        if (!_impl)
            return 0;

        return _impl->getMesh(mesh);
    }

    /*************/
    unsigned long long MeshMerger::getMesh(pcl::PolygonMesh::Ptr mesh) const
    {
        if (!_impl)
            return 0;

        return _impl->getMesh(mesh);
    }
    
    /*************/
    void MeshMerger::start() const
    {
        if (!_impl)
            return;

        return _impl->start();
    }
    
    /*************/
    void MeshMerger::stop() const
    {
        if (!_impl)
            return;

        return _impl->stop();
    }

} // end of namespace
