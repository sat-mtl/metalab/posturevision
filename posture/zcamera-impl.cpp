#include "zcamera-impl.h"

#include <chrono>
#include <cmath>
#include <iostream>
#include <thread>

#include <boost/make_shared.hpp>
#if WITH_OPENNI2
#include <pcl/io/openni2/openni2_device_manager.h>
#endif

#include <glm/ext.hpp>
#include <glm/glm.hpp>
#include <pcl/io/ply_io.h>

#if WITH_FREENECT2
#include <libfreenect2/frame_listener_impl.h>
#include <libfreenect2/registration.h>
#endif

#include "shadersources.h"

using namespace std;
using namespace pcl;
#if WITH_OPENNI and not WITH_OPENNI2 and not WITH_REALSENSE
using pcl::OpenNIGrabber;
#endif

namespace posture
{
/*************/
#if WITH_REALSENSE
unique_ptr<rs::context> ZCameraImpl::_rsContext{nullptr};
#elif WITH_FREENECT2
unique_ptr<libfreenect2::Freenect2> ZCameraImpl::_freenectContext{nullptr};
#endif

/*************/
#if WITH_REALSENSE
ZCameraImpl::ZCameraImpl(const int index, const ZCamera::CaptureMode mode, const bool grabVideo)
#elif WITH_FREENECT2
ZCameraImpl::ZCameraImpl(const int index, const ZCamera::CaptureMode mode, const bool grabVideo)
#elif WITH_OPENNI or WITH_OPENNI2
ZCameraImpl::ZCameraImpl(const int index, const PclGrabber::Mode mode, const bool grabVideo)
#endif
{
    _deviceIndex = index;

#if WITH_REALSENSE
    if (!_rsContext)
        _rsContext = unique_ptr<rs::context>(new rs::context());
#elif WITH_FREENECT2
    if (!_freenectContext)
        _freenectContext = unique_ptr<libfreenect2::Freenect2>(new libfreenect2::Freenect2());
#elif WITH_OPENNI2
    _grabMode = io::OpenNI2Grabber::OpenNI_Default_Mode;
    boost::shared_ptr<pcl::io::openni2::OpenNI2DeviceManager> deviceManager = pcl::io::openni2::OpenNI2DeviceManager::getInstance();
    _deviceURIs = deviceManager->getConnectedDeviceURIs();
#elif WITH_OPENNI
    _grabMode = OpenNIGrabber::OpenNI_Default_Mode;
#endif

    _depthFilter = boost::make_shared<FastBilateralFilterOMP<PointXYZRGB>>();

    _normalEstimator = boost::make_shared<IntegralImageNormalEstimation<PointXYZRGB, Normal>>();
    _normalEstimator->setNormalEstimationMethod(_normalEstimator->AVERAGE_DEPTH_CHANGE);
    _normalEstimator->setMaxDepthChangeFactor(0.01f);
    _normalEstimator->setNormalSmoothingSize(10.0f);
    _normalEstimator->setDepthDependentSmoothing(true);
    _normalEstimator->useSensorOriginAsViewPoint();

    _outlierRemoval = boost::make_shared<StatisticalOutlierRemoval<PointXYZRGB>>();
    _outlierRemoval->setMeanK(_filterMeanK);
    _outlierRemoval->setStddevMulThresh(_filterStddevMulThresh);
    _outlierRemoval->setNegative(0);
    _outlierRemoval->setKeepOrganized(true);

    _fastMesh = boost::make_shared<OrganizedFastMesh<PointXYZRGB>>();
    _fastMesh->setTriangulationType(OrganizedFastMesh<PointXYZRGB>::TRIANGLE_ADAPTIVE_CUT);
    _fastMesh->storeShadowedFaces(false);
    _fastMesh->setTrianglePixelSize(1);

    // Filters, only applied on RS cameras
    _glEngine = make_shared<GLEngine>();
    _glEngine->setBufferNumber(1);

    _shaderBilateralFilter = make_shared<Shader>(_glEngine, Shader::prgCompute);
    _shaderBilateralFilter->setSource(ShaderSources.BILATERAL_FILTER, Shader::compute);
    _shaderBilateralFilter->compileProgram();

    _shaderDilateFilter = make_shared<Shader>(_glEngine, Shader::prgCompute);
    _shaderDilateFilter->setSource(ShaderSources.DILATE_FILTER, Shader::compute);
    _shaderDilateFilter->compileProgram();

    _shaderErodeFilter = make_shared<Shader>(_glEngine, Shader::prgCompute);
    _shaderErodeFilter->setSource(ShaderSources.ERODE_FILTER, Shader::compute);
    _shaderErodeFilter->compileProgram();
}

/*************/
ZCameraImpl::~ZCameraImpl()
{
    if (_ready)
        stop();

#if not WITH_REALSENSE and (WITH_OPENNI or WITH_OPENNI2)
    _deviceURIs.reset();
#endif
}

/*************/
float ZCameraImpl::getRGBFocal()
{
    if (_grabber == nullptr)
        return 0.0;
    else
    {
        double fx, fy;
#if WITH_REALSENSE
        auto intrinsics = _grabber->get_stream_intrinsics(rs::stream::color);
        fx = intrinsics.fx;
#elif WITH_FREENECT2
        auto colorCameraParams = _grabber->getColorCameraParams();
        fx = colorCameraParams.fx;
#elif WITH_OPENNI or WITH_OPENNI2
        _grabber->getRGBFocalLength(fx, fy);
        if (std::isnan(fx))
        {
#if WITH_OPENNI2
            fx = _grabber->getDevice()->getColorFocalLength();
#elif WITH_OPENNI
            fx = _grabber->getDevice()->getImageFocalLength();
#endif
        }
#endif
        return fx;
    }
}

/*************/
float ZCameraImpl::getDepthFocal()
{
    if (_grabber == nullptr)
        return 0.0;
    else
    {
        double fx, fy;
#if WITH_REALSENSE
        auto intrinsics = _grabber->get_stream_intrinsics(rs::stream::depth);
        fx = intrinsics.fx;
#elif WITH_FREENECT2
        auto depthCameraParams = _grabber->getIrCameraParams();
        fx = depthCameraParams.fx;
#elif WITH_OPENNI or WITH_OPENNI2
        _grabber->getDepthFocalLength(fx, fy);
        if (std::isnan(fx))
            fx = _grabber->getDevice()->getDepthFocalLength();
#endif
        return fx;
    }
}

/*************/
unsigned long long ZCameraImpl::getCloud(vector<char>& cloud) const
{
    lock_guard<mutex> lock(_depthMutex);
    cloud = _serializer.serialize(_pointCloud, _timestamp);
    ;
    _newCloud = false;

    return _timestamp;
}

/*************/
unsigned long long ZCameraImpl::getCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud) const
{
    lock_guard<mutex> lock(_depthMutex);
    cloud = _pointCloud;
    _newCloud = false;

    return _timestamp;
}

/*************/
unsigned int ZCameraImpl::getDeviceCount()
{
#if WITH_REALSENSE
    if (!_rsContext)
        _rsContext = unique_ptr<rs::context>(new rs::context());

    if (!_rsContext)
        return 0;

    int deviceCount = 0;
    try
    {
        deviceCount = _rsContext->get_device_count();
    }
    catch (exception& e)
    {
        cerr << "Unable to get the number of Intel RealSense devices" << endl;
    }

    return deviceCount;
#elif WITH_FREENECT2
    if (!_freenectContext)
        _freenectContext = unique_ptr<libfreenect2::Freenect2>(new libfreenect2::Freenect2());

    if (!_freenectContext)
        return 0;

    return _freenectContext->enumerateDevices();
#else
    return 8;
#endif
}

/*************/
unsigned long long ZCameraImpl::getRgbImage(vector<unsigned char>& rgb, unsigned int& width, unsigned int& height) const
{
    lock_guard<mutex> lock(_imageMutex);
    rgb.resize(_rgbImage.size());
    memcpy(&rgb[0], reinterpret_cast<const unsigned char*>(&_rgbImage[0]), _rgbImage.size());

    width = _rgbWidth;
    height = _rgbHeight;

    _newImage = false;

    return _timestamp;
}

/*************/
unsigned long long ZCameraImpl::getDepthImage(vector<unsigned char>& depth, unsigned int& width, unsigned int& height) const
{
    lock_guard<mutex> lock(_depthImageMutex);
    depth.resize(_depthImage.size());
    memcpy(&depth[0], reinterpret_cast<const unsigned char*>(&_depthImage[0]), _depthImage.size());

    width = _depthWidth;
    height = _depthHeight;

    _newImage = false;

    return _timestamp;
}

/*************/
unsigned long long ZCameraImpl::getIRImage(vector<unsigned char>& ir, unsigned int& width, unsigned int& height) const
{
    lock_guard<mutex> lock(_irImageMutex);
    ir.resize(_irImage.size());
    memcpy(&ir[0], reinterpret_cast<const unsigned char*>(&_irImage[0]), _irImage.size());

    width = _irWidth;
    height = _irHeight;

    _newIRImage = false;

    return _timestamp;
}

/*************/
ZCamera::CaptureFormat ZCameraImpl::getCaptureFormat() const
{
#if WITH_REALSENSE
    if (_grabber)
    {
        /*
        // TODO: the camera seem to always return YUYV as a format, so we force it to RGB for now
        auto format = _grabber->get_stream_format(rs::stream::color);

        switch (format)
        {
        default:
            return ZCamera::CaptureFormat::BGR;
        case rs::format::rgb8:
            return ZCamera::CaptureFormat::RGB;
        case rs::format::bgr8:
            return ZCamera::CaptureFormat::BGR;
        }
        */

        return ZCamera::CaptureFormat::RGB;
    }
    else
    {
        return ZCamera::CaptureFormat::RGB;
    }
#elif WITH_FREENECT2
    return ZCamera::CaptureFormat::BGR;
#elif WITH_OPENNI or WITH_OPENNI2
    return ZCamera::CaptureFormat::BGR;
#endif
}

/*************/
#if WITH_REALSENSE
bool ZCameraImpl::setCaptureMode(ZCamera::CaptureMode mode)
#elif WITH_FREENECT2
bool ZCameraImpl::setCaptureMode(ZCamera::CaptureMode mode)
#elif WITH_OPENNI2 or WITH_OPENNI
bool ZCameraImpl::setCaptureMode(PclGrabber::Mode mode)
#endif
{
    _grabMode = mode;
    return true;
}

/*************/
bool ZCameraImpl::setCaptureMode(int mode)
{
#if WITH_REALSENSE
    _grabMode = (ZCamera::CaptureMode)mode;
#elif WITH_FREENECT2
    _grabMode = (ZCamera::CaptureMode)mode;
#elif WITH_OPENNI or WITH_OPENNI2
    _grabMode = (PclGrabber::Mode)mode;
#endif
    return true;
}

/*************/
bool ZCameraImpl::setCaptureIR(bool ir)
{
    _grabIR = ir;
    return true;
}

/*************/
bool ZCameraImpl::setRandomNoise(bool rn)
{
    _randomNoise = rn;
    return true;
}

/*************/
bool ZCameraImpl::setModelPath(std::string path)
{
    _modelPath = path;
    return true;
}

/*************/
void ZCameraImpl::setCompression(bool compress)
{
    _serializer.setCompression(compress);
    _meshSerializer.setCompress(compress);
}

/*************/
void ZCameraImpl::setDepthFocal(float focal)
{
#if not WITH_REALSENSE and (WITH_OPENNI or WITH_OPENNI2)
    lock_guard<mutex> lock(_depthMutex);
    _grabber->setDepthFocalLength(focal);
#endif
}

/*************/
bool ZCameraImpl::setDeviceIndex(unsigned int index)
{
#if not WITH_REALSENSE and WITH_OPENNI2
    if (index >= _deviceURIs->size())
        return false;
#endif
    _deviceIndex = index;
    return true;
}

/*************/
void ZCameraImpl::setBuildMesh(bool active)
{
    lock_guard<mutex> lock(_depthMutex);
    _buildFastMesh = active;
}

/*************/
void ZCameraImpl::setBuildEdgeLength(int length)
{
    lock_guard<mutex> lock(_depthMutex);
    _fastMesh->setTrianglePixelSize(max(1, length));
}

/*************/
void ZCameraImpl::setOutlierFilterParameters(bool active, int meanK, double stddevMul)
{
    lock_guard<mutex> lock(_depthMutex);

    _filterOutliers = active;
    _filterMeanK = max(1, meanK);
    _filterStddevMulThresh = max(0.0, stddevMul);

    _outlierRemoval->setMeanK(_filterMeanK);
    _outlierRemoval->setStddevMulThresh(_filterStddevMulThresh);
}

/*************/
void ZCameraImpl::setCallbackCloud(function<void(void*, vector<char>&&)> cb, void* user_data)
{
    if (cb != nullptr)
    {
        _cbCloud = cb;
        _cbCloudUserData = user_data;
    }
}

/*************/
void ZCameraImpl::setCallbackCloud(std::function<void(void*, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr)> cb, void* user_data)
{
    if (cb != nullptr)
    {
        _cbCloudUnserialized = cb;
        _cbCloudUserData = user_data;
    }
}

/*************/
void ZCameraImpl::setCallbackMesh(function<void(void*, vector<unsigned char>&&)> cb, void* user_data)
{
    if (cb != nullptr)
    {
        _cbMesh = cb;
        _cbMeshUserData = user_data;
    }
}

/*************/
void ZCameraImpl::setCallbackMesh(std::function<void(void*, pcl::TextureMesh::Ptr)> cb, void* user_data)
{
    if (cb != nullptr)
    {
        _cbMeshTexture = cb;
        _cbMeshUserData = user_data;
    }
}

/*************/
void ZCameraImpl::setCallbackMesh(std::function<void(void*, pcl::PolygonMesh::Ptr)> cb, void* user_data)
{
    if (cb != nullptr)
    {
        _cbMeshUnserialized = cb;
        _cbMeshUserData = user_data;
    }
}

/*************/
void ZCameraImpl::setCallbackDepth(function<void(void*, vector<unsigned char>&, int, int)> cb, void* user_data)
{
    if (cb != nullptr)
    {
        _cbDepth = cb;
        _cbDepthUserData = user_data;
    }
}

/*************/
void ZCameraImpl::setCallbackRgb(function<void(void*, vector<unsigned char>&, int, int)> cb, void* user_data)
{
    if (cb != nullptr)
    {
        _cbRGB = cb;
        _cbRGBUserData = user_data;
    }
}

/*************/
void ZCameraImpl::setCallbackIR(function<void(void*, vector<unsigned char>&, int, int)> cb, void* user_data)
{
    if (cb != nullptr)
    {
        _cbIr = cb;
        _cbIrUserData = user_data;
    }
}

/*************/
void ZCameraImpl::setCallbackNoise(function<void(void*, vector<char>&&)> cb, void* user_data)
{
    if (cb != nullptr)
    {
        _cbNoise = cb;
        _cbNoiseUserData = user_data;
    }
}

/*************/
void ZCameraImpl::setCallbackNoise(std::function<void(void*, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr)> cb, void* user_data)
{
    if (cb != nullptr)
    {
        _cbNoiseUnserialized = cb;
        _cbNoiseUserData = user_data;
    }
}

/*************/
void ZCameraImpl::setDownsampling(bool active, float resolution)
{
    // if (resolution < 0.01f)
    //    return;

    lock_guard<mutex> lock(_depthMutex);

    _downsample = active;
    _voxelGrid.setLeafSize(resolution, resolution, resolution);
}

/*************/
void ZCameraImpl::start()
{
    // Initialize some data
    _pointCloud = boost::make_shared<PointCloud<PointXYZRGBNormal>>();
    _pointCloud->width = 0;
    _pointCloud->height = 0;
    _pointCloud->is_dense = false;
    _pointCloud->points.resize(_pointCloud->width * _pointCloud->height);

    _timestamp = 0;
    _newCloud = false;

    bool error = false;

#if WITH_REALSENSE
    if (!_rsContext)
        return;

    try
    {
        _grabber = _rsContext->get_device(_deviceIndex);
    }
    catch (exception& e)
    {
        cerr << "Unable to connect to Intel Realsense device #" << _deviceIndex << endl;
        return;
    }

    _grabber->enable_stream(rs::stream::depth, rs::preset::best_quality);
    _grabber->enable_stream(rs::stream::color, rs::preset::best_quality);
    _ready = true;

    // Update a few options to get the best possible quality without filtering
    rs::apply_depth_control_preset(_grabber, 4); // Set outlier removal to the optimized setting
    setRsOption(rs::option::r200_lr_auto_exposure_enabled, 1.0);
    setRsOption(rs::option::r200_depth_clamp_max, _depthClamp);

    _rsUpdateThread = thread([&]() { rsUpdateLoop(); });
#elif WITH_FREENECT2
    if (!_freenectContext)
        return;

    _grabber = _freenectContext->openDevice(_deviceIndex);
    if (!_grabber)
    {
        cout << "Unable to open the grabber " << _deviceIndex << endl;
        return;
    }

    libfreenect2::Freenect2Device::Config config;
    config.EnableBilateralFilter = true;
    _grabber->setConfiguration(config);

    _freenect2UpdateThread = thread([&]() {
        _ready = true;
        freenectUpdateLoop();
    });
#elif WITH_OPENNI2 or WITH_OPENNI
    try
    {
        _grabber = make_shared<PclGrabber>("#" + to_string(_deviceIndex + 1), _grabMode, _grabMode);

// Forcing the depth to mm
#if WITH_OPENNI2
        boost::shared_ptr<pcl::io::openni2::OpenNI2Device> device = _grabber->getDevice();
        pcl::io::openni2::OpenNI2VideoMode depthMode = device->getDepthVideoMode();
        depthMode.pixel_format_ = pcl::io::openni2::PIXEL_FORMAT_DEPTH_100_UM;
        device->setDepthVideoMode(depthMode);
#endif

        if (!_grabIR)
        {
            _grabber->registerCallback((boost::function<void(const PointCloud<PointXYZRGBA>::ConstPtr&)>)boost::bind(&ZCameraImpl::onCameraCloud, this, _1));

            if (_grabber->providesCallback<PclGrabber::sig_cb_openni_image_depth_image>())
                _grabber->registerCallback((boost::function<void(const boost::shared_ptr<Image>&, const boost::shared_ptr<DepthImage>&, float)>)boost::bind(
                    &ZCameraImpl::onCameraImageDepth, this, _1, _2, _3));
            else if (_grabber->providesCallback<PclGrabber::sig_cb_openni_image>())
                _grabber->registerCallback((boost::function<void(const boost::shared_ptr<Image>&)>)boost::bind(&ZCameraImpl::onCameraImage, this, _1));
        }
        else
            _grabber->registerCallback((boost::function<void(const boost::shared_ptr<IRImage>&)>)boost::bind(&ZCameraImpl::onCameraIRImage, this, _1));
    }
    catch (const IOException& e)
    {
        if (_randomNoise)
        {
            if (!_modelPath.empty())
                _triggerGrabber = make_shared<TriggerGrabber>(1, 1, 1, _modelPath);
            else
                _triggerGrabber = make_shared<TriggerGrabber>(1, 1, 1);
            //_noiseThread = std::thread(&NoiseGrabber::start, _triggerGrabber);
            boost::function<void(const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr&)> noiseFunction = boost::bind(&ZCameraImpl::onCameraNoise, this, _1);
            _triggerGrabber->registerCallback(noiseFunction);
        }
        else
        {
            cout << "IOException: " << e.what() << endl;
            error = true;
        }
    }
    catch (const exception& e)
    {
        cout << e.what() << endl;
        error = true;
        throw(e);
    }

    if (!error)
    {
        _ready = true;
        if (_randomNoise)
            _triggerGrabber->update();
        else
            _grabber->start();
    }
#endif
}

/*************/
void ZCameraImpl::stop()
{
    if (_randomNoise)
        _triggerGrabber->stop();
    else
    {
#if WITH_REALSENSE
        if (_ready && _grabber->is_streaming())
        {
            _rsStopLoop = true;
            if (_rsUpdateThread.joinable())
                _rsUpdateThread.join();
        }
#elif WITH_FREENECT2
        if (_ready && _grabber)
        {
            _freenect2StopLoop = true;
            if (_freenect2UpdateThread.joinable())
                _freenect2UpdateThread.join();
        }
#elif WITH_OPENNI2 or WITH_OPENNI
        if (_ready && _grabber->isRunning())
            _grabber->stop();
#endif
    }
    _ready = false;

    unique_lock<mutex> lockDepth(_depthMutex);
    unique_lock<mutex> lockImage(_imageMutex);
    unique_lock<mutex> lockDepthImage(_depthImageMutex);
    unique_lock<mutex> lockIRImage(_irImageMutex);
}

/*************/
void ZCameraImpl::onCameraNoise(const PointCloud<PointXYZRGBA>::ConstPtr cloud)
{
    lock_guard<mutex> lock(_depthMutex);

    auto now = chrono::high_resolution_clock::now();
    _timestamp = chrono::duration_cast<chrono::milliseconds>(now.time_since_epoch()).count();
    PointCloud<PointXYZRGBNormal>::Ptr rgbNormalCloud = boost::make_shared<PointCloud<PointXYZRGBNormal>>();

    PointCloud<PointXYZRGB>::Ptr rgbCloud = boost::make_shared<PointCloud<PointXYZRGB>>();
    copyPointCloud(*cloud, *rgbCloud);

    PointCloud<Normal>::Ptr normals = boost::make_shared<PointCloud<Normal>>();
    //_normalEstimator->setInputCloud(rgbCloud);
    //_normalEstimator->compute(*normals);
    concatenateFields(*rgbCloud, *normals, *rgbNormalCloud);

    _pointCloud = rgbNormalCloud;

    _newCloud = true;
    if (_cbNoise != nullptr)
        _cbNoise(_cbNoiseUserData, _serializer.serialize(_pointCloud, _timestamp));
    if (_cbNoiseUnserialized != nullptr)
        _cbNoiseUnserialized(_cbNoiseUserData, _pointCloud);

    // Mesh from noise
    if ((_cbMesh || _cbMeshUnserialized) && _buildFastMesh)
    {
        _fastMesh->setInputCloud(rgbCloud);
        PolygonMesh::Ptr mesh = boost::make_shared<PolygonMesh>();
        _fastMesh->reconstruct(*mesh);

        TextureMesh::Ptr tMesh = boost::make_shared<TextureMesh>();
        tMesh->cloud = mesh->cloud;
        tMesh->tex_polygons.push_back(mesh->polygons);
        tMesh->tex_coordinates.resize(1);

        for (auto& poly : tMesh->tex_polygons[0])
        {
            for (auto& v : poly.vertices)
            {
                PointXYZRGB point = rgbCloud->at(v);
                float x = static_cast<float>(((point.x - _calibrationParams.rgbd_distance) / point.z + _rgbWidth / 2) / _rgbWidth);
                float y = 1.f - static_cast<float>((point.y / point.z) + _rgbHeight / 2 / _rgbHeight);
                tMesh->tex_coordinates[0].emplace_back(Eigen::Vector2f(x, y));
            }
        }
        _mesh = tMesh;
        _polyMesh = mesh;

        if (_cbMesh)
            _cbMesh(_cbMeshUserData, _meshSerializer.serialize(_mesh, _timestamp));
        if (_cbMeshUnserialized)
            _cbMeshUnserialized(_cbMeshUserData, _polyMesh);
    }
}

/*********/
Eigen::Matrix4f getTransformationForCamera(CalibrationParams calibrationParams)
{
    glm::mat4 translation = glm::translate(glm::mat4(1.f), glm::vec3(calibrationParams.offset_x, calibrationParams.offset_y, calibrationParams.offset_z));

    float angle = M_PI - calibrationParams.rot_x * (M_PI / 180.f);
    glm::mat4 rotationX = glm::rotate(glm::mat4(1.f), angle, glm::vec3(1.f, 0.f, 0.f));

    angle = -calibrationParams.rot_y * (M_PI / 180.f);
    glm::mat4 rotationY = glm::rotate(glm::mat4(1.f), angle, glm::vec3(0.f, 1.f, 0.f));

    angle = -calibrationParams.rot_z * (M_PI / 180.f);
    glm::mat4 rotationZ = glm::rotate(glm::mat4(1.f), angle, glm::vec3(0.f, 0.f, 1.f));

    auto glmMatrix = rotationX * rotationY * rotationZ; // * translation;

    Eigen::Matrix4f transformations = Eigen::Matrix4f::Identity();

    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            transformations(i, j) = glmMatrix[i][j];
        }
    }

    transformations(0, 3) = calibrationParams.offset_x;
    transformations(1, 3) = calibrationParams.offset_y;
    transformations(2, 3) = calibrationParams.offset_z;
    transformations(3, 3) = 1.0;

    return transformations;
}

#if WITH_REALSENSE
/*************/
void ZCameraImpl::rsUpdateLoop()
{
    _grabber->start();
    _rsStopLoop = false;

    while (!_rsStopLoop)
    {
        _grabber->wait_for_frames();

        // Depth for RS200 is stored in millimeters by default, which is exactly what we need
        rs::intrinsics depthIntrinsics = _grabber->get_stream_intrinsics(rs::stream::depth_aligned_to_rectified_color);
        rs::intrinsics colorIntrinsics = _grabber->get_stream_intrinsics(rs::stream::rectified_color);
        rs::extrinsics depthToColor = _grabber->get_extrinsics(rs::stream::depth_aligned_to_rectified_color, rs::stream::rectified_color);

        _depthWidth = depthIntrinsics.width;
        _depthHeight = depthIntrinsics.height;
        _rgbWidth = colorIntrinsics.width;
        _rgbHeight = colorIntrinsics.height;

        const uint16_t* depthImage = static_cast<const uint16_t*>(_grabber->get_frame_data(rs::stream::depth_aligned_to_rectified_color));
        const uint8_t* colorImage = static_cast<const uint8_t*>(_grabber->get_frame_data(rs::stream::rectified_color));
        _timestamp = static_cast<uint64_t>(_grabber->get_frame_timestamp(rs::stream::depth_aligned_to_rectified_color));

        lock_guard<mutex> rgbLock(_imageMutex);
        lock_guard<mutex> depthLock(_depthImageMutex);

        _rgbImage.resize(_rgbWidth * _rgbHeight * 3 * sizeof(uint8_t));
        _depthImage.resize(_depthWidth * _depthHeight * 2 * sizeof(uint8_t));

        memcpy(&_rgbImage[0], colorImage, _rgbWidth * _rgbHeight * 3);
        // memcpy(&_depthImage[0], depthImage, _depthWidth * _depthHeight * 2);

        // Filter the depth map
        // TODO: _depthImage should be stored as a uint16.
        vector<uint16_t> glDepthImage;
        glDepthImage.resize(_depthWidth * _depthHeight);
        memcpy(glDepthImage.data(), depthImage, _depthImage.size());

        _glEngine->lockContext();
        _glEngine->setBuffer(glDepthImage, 0, 1);

        // Bilateral filter
        for (int i = 0; i < _bilateralFilterIterations; ++i)
        {
            _shaderBilateralFilter->setUniform("_size", {(float)_depthWidth, (float)_depthHeight});
            _shaderBilateralFilter->setUniform("_kernelShift", {(int)_bilateralFilterKernelSize / 2});
            _shaderBilateralFilter->setUniform("_sigmaPos", {(float)_bilateralFilterSigmaPos});
            _shaderBilateralFilter->setUniform("_sigmaValue", {(float)_bilateralFilterSigmaValue});
            _shaderBilateralFilter->setComputeSize(_depthWidth / 32 + 1, _depthHeight / 32 + 1);
            _glEngine->setShader(_shaderBilateralFilter);
            _glEngine->run();
        }

        // Hole filling
        _shaderDilateFilter->setUniform("_size", {(float)_depthWidth, (float)_depthHeight});
        _shaderDilateFilter->setUniform("_kernelSize", {(int)_holeFillingKernelSize});
        _shaderDilateFilter->setComputeSize(_depthWidth / 32 + 1, _depthHeight / 32 + 1);
        _glEngine->setShader(_shaderDilateFilter);
        for (int i = 0; i < _holeFillingIterations; ++i)
            _glEngine->run();

        _shaderErodeFilter->setUniform("_size", {(float)_depthWidth, (float)_depthHeight});
        _shaderErodeFilter->setUniform("_kernelSize", {(int)_holeFillingKernelSize});
        _shaderErodeFilter->setComputeSize(_depthWidth / 32 + 1, _depthHeight / 32 + 1);
        _glEngine->setShader(_shaderErodeFilter);
        for (int i = 0; i < _holeFillingIterations; ++i)
            _glEngine->run();

        // Noise reduction
        _shaderErodeFilter->setUniform("_size", {(float)_depthWidth, (float)_depthHeight});
        _shaderErodeFilter->setUniform("_kernelSize", {(int)_holeFillingKernelSize});
        _shaderErodeFilter->setComputeSize(_depthWidth / 32 + 1, _depthHeight / 32 + 1);
        _glEngine->setShader(_shaderErodeFilter);
        for (int i = 0; i < _holeFillingIterations; ++i)
            _glEngine->run();

        _shaderDilateFilter->setUniform("_size", {(float)_depthWidth, (float)_depthHeight});
        _shaderDilateFilter->setUniform("_kernelSize", {(int)_holeFillingKernelSize});
        _shaderDilateFilter->setComputeSize(_depthWidth / 32 + 1, _depthHeight / 32 + 1);
        _glEngine->setShader(_shaderDilateFilter);
        for (int i = 0; i < _holeFillingIterations; ++i)
            _glEngine->run();

        _glEngine->unlockContext();

        glDepthImage = _glEngine->getFeedbackBuffer<uint16_t>(0);
        memcpy(_depthImage.data(), glDepthImage.data(), _depthImage.size());

        // Point cloud creation
        float scale = _grabber->get_depth_scale();
        PointCloud<PointXYZRGBNormal>::Ptr cloud = boost::make_shared<PointCloud<PointXYZRGBNormal>>();
        cloud->reserve(depthIntrinsics.height * depthIntrinsics.width);

        PointXYZRGBNormal point;
        for (int y = 0; y < depthIntrinsics.height; ++y)
        {
            int dataShift = y * depthIntrinsics.width;

            for (int x = 0; x < depthIntrinsics.width; ++x)
            {
                uint16_t depthValue = glDepthImage[dataShift + x];
                if (depthValue == 0 || depthValue > _clippingDistance)
                {
                    point.x = numeric_limits<float>::quiet_NaN();
                    point.y = numeric_limits<float>::quiet_NaN();
                    point.z = numeric_limits<float>::quiet_NaN();
                    point.r = 0.f;
                    point.g = 0.f;
                    point.b = 0.f;
                    point.normal_x = 0.f;
                    point.normal_y = 0.f;
                    point.normal_z = 0.f;
                }
                else
                {
                    float depthInMeters = depthValue * scale;
                    rs::float2 depthPixel = {(float)x, (float)y};
                    rs::float3 depthPoint = depthIntrinsics.deproject(depthPixel, depthInMeters);
                    rs::float3 colorPoint = depthToColor.transform(depthPoint);
                    rs::float2 colorPixel = colorIntrinsics.project(colorPoint);
                    const uint8_t* color = colorImage + ((int)colorPixel.x + (int)colorPixel.y * colorIntrinsics.width) * 3;

                    point.x = depthPoint.x;
                    point.y = depthPoint.y;
                    point.z = depthPoint.z;
                    point.r = color[0];
                    point.g = color[1];
                    point.b = color[2];
                    point.normal_x = 0.f;
                    point.normal_y = 0.f;
                    point.normal_z = 0.f;
                }

                cloud->push_back(point);
            }
        }
        cloud->width = depthIntrinsics.width;
        cloud->height = depthIntrinsics.height;

        _pointCloud = cloud;

        // Depth callback
        if (_cbDepth)
            _cbDepth(_cbDepthUserData, _depthImage, _depthWidth, _depthHeight);

        // RGB callback
        if (_cbRGB)
            _cbRGB(_cbRGBUserData, _rgbImage, _rgbWidth, _rgbHeight);

        // Point clouds callback
        if (_cbCloud != nullptr)
            _cbCloud(_cbCloudUserData, _serializer.serialize(_pointCloud, _timestamp));
        if (_cbCloudUnserialized != nullptr)
            _cbCloudUnserialized(_cbCloudUserData, _pointCloud);

        // Mesh callback
        if ((_cbMesh || _cbMeshUnserialized || _cbMeshTexture) && _buildFastMesh)
        {
            auto rgbCloud = boost::make_shared<PointCloud<PointXYZRGB>>();
            copyPointCloud(*cloud, *rgbCloud);

            _fastMesh->setInputCloud(rgbCloud);
            auto mesh = boost::make_shared<PolygonMesh>();
            _fastMesh->reconstruct(*mesh);

            if (mesh->polygons.size() != 0)
            {
                auto tMesh = boost::make_shared<TextureMesh>();
                tMesh->cloud = mesh->cloud;
                tMesh->tex_polygons.push_back(mesh->polygons);
                tMesh->tex_coordinates.resize(1);

                float f = getRGBFocal();
                for (auto& poly : tMesh->tex_polygons[0])
                {
                    for (auto& v : poly.vertices)
                    {
                        PointXYZRGB point = rgbCloud->at(v);
                        float x = static_cast<float>((f * ((point.x - _calibrationParams.rgbd_distance) / point.z) + _rgbWidth / 2) / _rgbWidth);
                        float y = 1.f - static_cast<float>((f * (point.y / point.z) + _rgbHeight / 2) / _rgbHeight);
                        tMesh->tex_coordinates[0].emplace_back(Eigen::Vector2f(x, y));
                    }
                }

                _filter.filterNaN(tMesh);
                _filter.filterNaN(mesh);

                _mesh = tMesh;
                _polyMesh = mesh;

                if (_mesh->cloud.width != 0)
                {
                    if (_cbMesh)
                        _cbMesh(_cbMeshUserData, _meshSerializer.serialize(_mesh, _timestamp));
                    if (_cbMeshTexture)
                        _cbMeshTexture(_cbMeshUserData, _mesh);
                }

                if (_polyMesh->cloud.width != 0)
                    if (_cbMeshUnserialized)
                        _cbMeshUnserialized(_cbMeshUserData, _polyMesh);
            }
        }

        _newImage = true;
    }

    _grabber->stop();
}
#endif

#if WITH_FREENECT2
/*************/
void ZCameraImpl::freenectUpdateLoop()
{
    _freenect2StopLoop = false;

    libfreenect2::SyncMultiFrameListener listener(libfreenect2::Frame::Color | libfreenect2::Frame::Ir | libfreenect2::Frame::Depth);
    libfreenect2::FrameMap frames;

    _grabber->setColorFrameListener(&listener);
    _grabber->setIrAndDepthFrameListener(&listener);
    if (!_grabber->start())
    {
        cout << "Unable to start the grabber " << _deviceIndex << endl;
        return;
    }

    auto registration = unique_ptr<libfreenect2::Registration>(new libfreenect2::Registration(_grabber->getIrCameraParams(), _grabber->getColorCameraParams()));
    libfreenect2::Frame undistorted(512, 424, 4), registered(512, 424, 4);

    while (!_freenect2StopLoop)
    {
        if (!listener.waitForNewFrame(frames, 10 * 1000)) // 10 seconds
            break;

        auto now = chrono::high_resolution_clock::now();
        _timestamp = chrono::duration_cast<chrono::milliseconds>(now.time_since_epoch()).count();

        libfreenect2::Frame* rgb = frames[libfreenect2::Frame::Color];
        libfreenect2::Frame* depth = frames[libfreenect2::Frame::Depth];

        registration->apply(rgb, depth, &undistorted, &registered);

        _depthWidth = depth->width;
        _depthHeight = depth->height;
        _rgbWidth = 512;  // rgb->width;
        _rgbHeight = 424; // rgb->height;

        _rgbImage.resize(_rgbWidth * _rgbHeight * 3 * sizeof(uint8_t));
        _depthImage.resize(_depthWidth * _depthHeight * 2 * sizeof(uint8_t));

        const uint8_t* rgbRaw = reinterpret_cast<const uint8_t*>(registered.data);
        for (uint y = 0; y < _rgbHeight; ++y)
            for (uint x = 0; x < _rgbWidth; ++x)
            {
                _rgbImage[(y * _rgbWidth + x) * 3 + 0] = rgbRaw[(y * _rgbWidth + x) * 4 + 0];
                _rgbImage[(y * _rgbWidth + x) * 3 + 1] = rgbRaw[(y * _rgbWidth + x) * 4 + 1];
                _rgbImage[(y * _rgbWidth + x) * 3 + 2] = rgbRaw[(y * _rgbWidth + x) * 4 + 2];
            }

        const float* depthRaw = reinterpret_cast<const float*>(depth->data);
        uint16_t* depthPtr = reinterpret_cast<uint16_t*>(_depthImage.data());
        for (uint y = 0; y < _depthHeight; ++y)
            for (uint x = 0; x < _depthWidth; ++x)
            {
                auto& value = depthRaw[y * _depthWidth + x];
                if (isnan(value) || 0.f > value || isinf(value))
                    depthPtr[y * _depthWidth + x] = 0;
                else
                    depthPtr[y * _depthWidth + x] = static_cast<uint16_t>(value);
            }

        listener.release(frames);

        // Filter the depth map
        // TODO: _depthImage should be stored as a uint16.
        vector<uint16_t> glDepthImage;
        glDepthImage.resize(_depthWidth * _depthHeight);
        memcpy(glDepthImage.data(), _depthImage.data(), _depthImage.size());

        _glEngine->lockContext();
        _glEngine->setBuffer(glDepthImage, 0, 1);

        // Bilateral filter
        for (int i = 0; i < _bilateralFilterIterations; ++i)
        {
            _shaderBilateralFilter->setUniform("_size", {(float)_depthWidth, (float)_depthHeight});
            _shaderBilateralFilter->setUniform("_kernelShift", {(int)_bilateralFilterKernelSize / 2});
            _shaderBilateralFilter->setUniform("_sigmaPos", {(float)_bilateralFilterSigmaPos});
            _shaderBilateralFilter->setUniform("_sigmaValue", {(float)_bilateralFilterSigmaValue});
            _shaderBilateralFilter->setComputeSize(_depthWidth / 32 + 1, _depthHeight / 32 + 1);
            _glEngine->setShader(_shaderBilateralFilter);
            _glEngine->run();
        }

        // Hole filling
        _shaderDilateFilter->setUniform("_size", {(float)_depthWidth, (float)_depthHeight});
        _shaderDilateFilter->setUniform("_kernelSize", {(int)_holeFillingKernelSize});
        _shaderDilateFilter->setComputeSize(_depthWidth / 32 + 1, _depthHeight / 32 + 1);
        _glEngine->setShader(_shaderDilateFilter);
        for (int i = 0; i < _holeFillingIterations; ++i)
            _glEngine->run();

        _shaderErodeFilter->setUniform("_size", {(float)_depthWidth, (float)_depthHeight});
        _shaderErodeFilter->setUniform("_kernelSize", {(int)_holeFillingKernelSize});
        _shaderErodeFilter->setComputeSize(_depthWidth / 32 + 1, _depthHeight / 32 + 1);
        _glEngine->setShader(_shaderErodeFilter);
        for (int i = 0; i < _holeFillingIterations; ++i)
            _glEngine->run();

        // Noise reduction
        _shaderErodeFilter->setUniform("_size", {(float)_depthWidth, (float)_depthHeight});
        _shaderErodeFilter->setUniform("_kernelSize", {(int)_holeFillingKernelSize});
        _shaderErodeFilter->setComputeSize(_depthWidth / 32 + 1, _depthHeight / 32 + 1);
        _glEngine->setShader(_shaderErodeFilter);
        for (int i = 0; i < _holeFillingIterations; ++i)
            _glEngine->run();

        _shaderDilateFilter->setUniform("_size", {(float)_depthWidth, (float)_depthHeight});
        _shaderDilateFilter->setUniform("_kernelSize", {(int)_holeFillingKernelSize});
        _shaderDilateFilter->setComputeSize(_depthWidth / 32 + 1, _depthHeight / 32 + 1);
        _glEngine->setShader(_shaderDilateFilter);
        for (int i = 0; i < _holeFillingIterations; ++i)
            _glEngine->run();

        _glEngine->unlockContext();

        glDepthImage = _glEngine->getFeedbackBuffer<uint16_t>(0);
        memcpy(_depthImage.data(), glDepthImage.data(), _depthImage.size());

        // Point cloud creation
        PointCloud<PointXYZRGBNormal>::Ptr cloud = boost::make_shared<PointCloud<PointXYZRGBNormal>>();
        cloud->reserve(_depthWidth * _depthHeight);

        PointXYZRGBNormal point;
        depthPtr = reinterpret_cast<uint16_t*>(_depthImage.data());
        for (int y = 0; y < _depthHeight; ++y)
        {
            int dataShift = y * _depthWidth;

            for (int x = 0; x < _depthWidth; ++x)
            {
                uint16_t depthValue = depthPtr[dataShift + x];
                if (depthValue == 0 || depthValue > _clippingDistance)
                {
                    point.x = numeric_limits<float>::quiet_NaN();
                    point.y = numeric_limits<float>::quiet_NaN();
                    point.z = numeric_limits<float>::quiet_NaN();
                    point.r = 0.f;
                    point.g = 0.f;
                    point.b = 0.f;
                    point.normal_x = 0.f;
                    point.normal_y = 0.f;
                    point.normal_z = 0.f;
                }
                else
                {
                    const uint8_t* color = &_rgbImage[(dataShift + x) * 3];
                    float depthPoint[3];
                    float depthMeter = static_cast<float>(depthValue) / 1000.f;
                    depthPoint[2] = depthMeter;
                    depthPoint[0] = (static_cast<float>(x) - static_cast<float>(_depthWidth) / 2.f) * (depthMeter / getDepthFocal());
                    depthPoint[1] = (static_cast<float>(y) - static_cast<float>(_depthHeight) / 2.f) * (depthMeter / getDepthFocal());

                    point.x = depthPoint[0];
                    point.y = depthPoint[1];
                    point.z = depthPoint[2];
                    point.r = color[0];
                    point.g = color[1];
                    point.b = color[2];
                    point.normal_x = 0.f;
                    point.normal_y = 0.f;
                    point.normal_z = 0.f;
                }

                cloud->push_back(point);
            }
        }
        cloud->width = _depthWidth;
        cloud->height = _depthHeight;

        _pointCloud = cloud;

        // Depth callback
        if (_cbDepth)
            _cbDepth(_cbDepthUserData, _depthImage, _depthWidth, _depthHeight);

        // RGB callback
        if (_cbRGB)
            _cbRGB(_cbRGBUserData, _rgbImage, _rgbWidth, _rgbHeight);

        // Point clouds callback
        if (_cbCloud != nullptr)
            _cbCloud(_cbCloudUserData, _serializer.serialize(_pointCloud, _timestamp));
        if (_cbCloudUnserialized != nullptr)
            _cbCloudUnserialized(_cbCloudUserData, _pointCloud);

        // Mesh callback
        if ((_cbMesh || _cbMeshUnserialized || _cbMeshTexture) && _buildFastMesh)
        {
            auto rgbCloud = boost::make_shared<PointCloud<PointXYZRGB>>();
            copyPointCloud(*cloud, *rgbCloud);

            _fastMesh->setInputCloud(rgbCloud);
            auto mesh = boost::make_shared<PolygonMesh>();
            _fastMesh->reconstruct(*mesh);

            if (mesh->polygons.size() != 0)
            {
                auto tMesh = boost::make_shared<TextureMesh>();
                tMesh->cloud = mesh->cloud;
                tMesh->tex_polygons.push_back(mesh->polygons);
                tMesh->tex_coordinates.resize(1);

                float f = getDepthFocal();
                for (auto& poly : tMesh->tex_polygons[0])
                {
                    for (auto& v : poly.vertices)
                    {
                        PointXYZRGB point = rgbCloud->at(v);
                        float x = static_cast<float>((f * ((point.x - _calibrationParams.rgbd_distance) / point.z) + _rgbWidth / 2) / _rgbWidth);
                        float y = 1.f - static_cast<float>((f * (point.y / point.z) + _rgbHeight / 2) / _rgbHeight);
                        tMesh->tex_coordinates[0].emplace_back(Eigen::Vector2f(x, y));
                    }
                }

                _filter.filterNaN(tMesh);
                _filter.filterNaN(mesh);

                _mesh = tMesh;
                _polyMesh = mesh;

                if (_mesh->cloud.width != 0)
                {
                    if (_cbMesh)
                        _cbMesh(_cbMeshUserData, _meshSerializer.serialize(_mesh, _timestamp));
                    if (_cbMeshTexture)
                        _cbMeshTexture(_cbMeshUserData, _mesh);
                }

                if (_polyMesh->cloud.width != 0)
                    if (_cbMeshUnserialized)
                        _cbMeshUnserialized(_cbMeshUserData, _polyMesh);
            }
        }

        _newImage = true;
    }

    _grabber->stop();
    _grabber->close();
}
#endif

#if not WITH_REALSENSE and not WITH_FREENECT2
/*************/
void ZCameraImpl::onCameraCloud(const PointCloud<PointXYZRGBA>::ConstPtr cloud)
{
    lock_guard<mutex> lock(_depthMutex);

    auto now = chrono::high_resolution_clock::now();
    _timestamp = chrono::duration_cast<chrono::milliseconds>(now.time_since_epoch()).count();
    PointCloud<PointXYZRGBNormal>::Ptr rgbNormalCloud = boost::make_shared<PointCloud<PointXYZRGBNormal>>();

    PointCloud<PointXYZRGB>::Ptr rgbCloud = boost::make_shared<PointCloud<PointXYZRGB>>();
    PointCloud<PointXYZRGB>::Ptr rgbCloudNoOutliers = boost::make_shared<PointCloud<PointXYZRGB>>();

    copyPointCloud(*cloud, *rgbCloud);

    // Filter the depth
    //_depthFilter->setInputCloud(rgbCloud);
    //_depthFilter->filter(*rgbCloud);

    // Apply some outlier filtering
    if (_filterOutliers)
    {
        _outlierRemoval->setInputCloud(rgbCloud);
        _outlierRemoval->filter(*rgbCloudNoOutliers);
        rgbCloud.swap(rgbCloudNoOutliers);
    }

    // Scale if needed, according to the configuration file
    if (_calibrationParams.scale != 1.f)
    {
        auto scale = _calibrationParams.scale;
        for (auto& point : rgbCloud->points)
        {
            point.x *= scale;
            point.y *= scale;
            point.z *= scale;
        }
    }

    // Find the normals
    PointCloud<Normal>::Ptr normals = boost::make_shared<PointCloud<Normal>>();
    _normalEstimator->setInputCloud(rgbCloud);
    _normalEstimator->compute(*normals);
    concatenateFields(*rgbCloud, *normals, *rgbNormalCloud);

    std::vector<int> removedIndices;
    pcl::removeNaNFromPointCloud(*rgbNormalCloud, *rgbNormalCloud, removedIndices);
    pcl::removeNaNNormalsFromPointCloud(*rgbNormalCloud, *rgbNormalCloud, removedIndices);

    // Eigen::Matrix4f transformations = getTransformationForCamera(_calibrationParams);
    // pcl::transformPointCloud(*rgbNormalCloud, *rgbNormalCloud, transformations);
    rgbNormalCloud = _filter.applyShapeClipping<PointXYZRGBNormal>(rgbNormalCloud, _calibrationParams);
    // rgbNormalCloud = _filter.applyClipping<PointXYZRGBNormal>(rgbNormalCloud, _calibrationParams);

    // Resample the cloud
    if (_downsample)
    {
        _voxelGrid.setInputCloud(rgbNormalCloud);
        _voxelGrid.filter(*_pointCloud);
    }
    else
        _pointCloud = rgbNormalCloud;

    _newCloud = true;
    if (_cbCloud != nullptr)
    {
        _cbCloud(_cbCloudUserData, _serializer.serialize(_pointCloud, _timestamp));
    }
    if (_cbCloudUnserialized != nullptr)
    {
        _cbCloudUnserialized(_cbCloudUserData, _pointCloud);
    }

    if ((_cbMesh || _cbMeshUnserialized || _cbMeshTexture) && _buildFastMesh)
    {
        _fastMesh->setInputCloud(rgbCloud);
        PolygonMesh::Ptr mesh = boost::make_shared<PolygonMesh>();
        _fastMesh->reconstruct(*mesh);

        TextureMesh::Ptr tMesh = boost::make_shared<TextureMesh>();
        tMesh->cloud = mesh->cloud;
        tMesh->tex_polygons.push_back(mesh->polygons);
        tMesh->tex_coordinates.resize(1);
        // The texture coordinates are filled from the cloud structure
        float w = static_cast<float>(rgbCloud->width) - 1.f;
        float h = static_cast<float>(rgbCloud->height) - 1.f;

        float f = getRGBFocal();
#if WITH_OPENNI2
        float f = _grabber->getDevice()->getColorFocalLength();
#elif WITH_OPENNI
        float f = _grabber->getDevice()->getImageFocalLength();
#endif
        for (auto& poly : tMesh->tex_polygons[0])
        {
            for (auto& v : poly.vertices)
            {
                PointXYZRGB point = rgbCloud->at(v);
                float x = static_cast<float>((f * ((point.x - _calibrationParams.rgbd_distance) / point.z) + _rgbWidth / 2) / _rgbWidth);
                float y = 1.f - static_cast<float>((f * (point.y / point.z) + _rgbHeight / 2) / _rgbHeight);
                tMesh->tex_coordinates[0].emplace_back(Eigen::Vector2f(x, y));
            }
        }
        _mesh = tMesh;

        _polyMesh = mesh;

        if (_cbMesh)
            _cbMesh(_cbMeshUserData, _meshSerializer.serialize(_mesh, _timestamp));
        if (_cbMeshTexture)
            _cbMeshTexture(_cbMeshUserData, _mesh);
        if (_cbMeshUnserialized)
            _cbMeshUnserialized(_cbMeshUserData, _polyMesh);
    }
}

/*************/
void ZCameraImpl::onCameraImage(const boost::shared_ptr<Image>& rgb)
{
    lock_guard<mutex> lock(_imageMutex);

    _rgbWidth = rgb->getWidth();
    _rgbHeight = rgb->getHeight();
    _rgbImage.resize(_rgbWidth * _rgbHeight * 3 * sizeof(unsigned char));

    if (rgb->getEncoding() != Image::RGB)
        rgb->fillRGB(_rgbWidth, _rgbHeight, &_rgbImage[0]);
    else
#if WITH_OPENNI2
        memcpy(&_rgbImage[0], rgb->getData(), _rgbImage.size());
#elif WITH_OPENNI
        memcpy(&_rgbImage[0], rgb->getMetaData().Data(), _rgbImage.size());
#endif

    _newImage = true;

    if (_cbRGB != nullptr)
        _cbRGB(_cbRGBUserData, _rgbImage, _rgbWidth, _rgbHeight);
}

/*************/
void ZCameraImpl::onCameraImageDepth(const boost::shared_ptr<Image>& rgb, const boost::shared_ptr<DepthImage>& depth, float constant)
{
    lock_guard<mutex> lock(_depthImageMutex);

    _rgbWidth = rgb->getWidth();
    _rgbHeight = rgb->getHeight();
    _rgbImage.resize(_rgbWidth * _rgbHeight * 3 * sizeof(unsigned char));

    _depthWidth = depth->getWidth();
    _depthHeight = depth->getHeight();
    _depthImage.resize(_depthWidth * _depthHeight * 2 * sizeof(unsigned char));

    if (rgb->getEncoding() != Image::RGB)
        rgb->fillRGB(_rgbWidth, _rgbHeight, &_rgbImage[0]);
    else
#if WITH_OPENNI2
        memcpy(&_rgbImage[0], rgb->getData(), _rgbImage.size());
#elif WITH_OPENNI
        memcpy(&_rgbImage[0], rgb->getMetaData().Data(), _rgbImage.size());
#endif

#if WITH_OPENNI2
    memcpy(&_depthImage[0], depth->getData(), _depthImage.size());
#elif WITH_OPENNI
    memcpy(&_depthImage[0], reinterpret_cast<const unsigned char*>(&depth->getDepthMetaData().Data()[0]), _depthImage.size());
#endif

    _newImage = true;

    // Scale the depth information according to the scale parameter
    if (_calibrationParams.scale != 1.f)
    {
        auto size = _depthWidth * _depthHeight;
        auto scale = _calibrationParams.scale;

        uint16_t* depthPixel = reinterpret_cast<uint16_t*>(_depthImage.data());
        for (int p = 0; p < size; ++p)
            depthPixel[p] *= scale;
    }

    if (_cbDepth != nullptr)
        _cbDepth(_cbDepthUserData, _depthImage, _depthWidth, _depthHeight);
    if (_cbRGB != nullptr)
        _cbRGB(_cbRGBUserData, _rgbImage, _rgbWidth, _rgbHeight);
}

/*************/
void ZCameraImpl::onCameraIRImage(const boost::shared_ptr<IRImage>& ir)
{
    lock_guard<mutex> lock(_irImageMutex);

    _irWidth = ir->getWidth();
    _irHeight = ir->getHeight();
    _irImage.resize(_irWidth * _irHeight * 2 * sizeof(unsigned char));

// ir->fillRaw(_irWidth, _irHeight, &_irImage[0]);
#if WITH_OPENNI2
    memcpy(&_irImage[0], ir->getData(), _irImage.size());
#elif WITH_OPENI
    memcpy(&_irImage[0], reinterpret_cast<const unsigned char*>(&ir->getMetaData().Data()[0]), _irImage.size());
#endif
    _newIRImage = true;

    if (_cbIr != nullptr)
        _cbIr(_cbIrUserData, _irImage, _irWidth, _irHeight);
}
#endif

/*************/
void ZCameraImpl::getNoise(float xMax, float yMax, float zMax, int nbrPoints, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
{
    cloud->resize(nbrPoints);
    for (int i = 0; i < nbrPoints; ++i)
    {
        cloud->points[i].x = (float)rand() / RAND_MAX * xMax;
        cloud->points[i].y = (float)rand() / RAND_MAX * yMax;
        cloud->points[i].z = (float)rand() / RAND_MAX * zMax;
        cloud->points[i].r = (float)rand() / ((float)RAND_MAX / (255.0f));
        cloud->points[i].g = (float)rand() / ((float)RAND_MAX / (255.0f));
        cloud->points[i].b = (float)rand() / ((float)RAND_MAX / (255.0f));
        cloud->points[i].normal_x = 1.0f;
        cloud->points[i].normal_y = 1.0f;
        cloud->points[i].normal_z = 1.0f;
    }
}

/*************/
void ZCameraImpl::getEmptyCubeNoise(float xMax, float yMax, float zMax, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
{
    cloud->width = xMax * 1000;
    cloud->height = yMax * 1000;
    size_t pointCount = cloud->width * cloud->height;
    cloud->resize(pointCount);
    cloud->is_dense = true;

    for (int i = 0; i < pointCount; ++i)
    {
        int plane = rand() % 3 + 1;
        float sign = round(((float)rand() / RAND_MAX)) * 2.0f - 1.0f;
        float x, y, z;

        switch (plane)
        {
        case 1:
        {
            x = ((float)rand() / RAND_MAX) * 0.01 + (xMax / 2.0f) * sign;
            y = (float)rand() / RAND_MAX * yMax - (yMax / 2.0f);
            z = (float)rand() / RAND_MAX * zMax - (zMax / 2.0f);
        }
        break;
        case 2:
        {
            x = (float)rand() / RAND_MAX * xMax - (xMax / 2.0f);
            y = ((float)rand() / RAND_MAX) * 0.01 + (yMax / 2.0f) * sign;
            z = (float)rand() / RAND_MAX * zMax - (zMax / 2.0f);
        }
        break;
        case 3:
        {
            x = (float)rand() / RAND_MAX * xMax - (xMax / 2.0f);
            y = (float)rand() / RAND_MAX * yMax - (yMax / 2.0f);
            z = ((float)rand() / RAND_MAX) * 0.01 + (zMax / 2.0f) * sign;
        }
        break;
        }

        cloud->points[i].x = x;
        cloud->points[i].y = y;
        cloud->points[i].z = z;

        cloud->points[i].r = (float)rand() / ((float)RAND_MAX / (255.0f));
        cloud->points[i].g = (float)rand() / ((float)RAND_MAX / (255.0f));
        cloud->points[i].b = (float)rand() / ((float)RAND_MAX / (255.0f));

        cloud->points[i].normal_x = 1.0f;
        cloud->points[i].normal_y = 1.0f;
        cloud->points[i].normal_z = 1.0f;
    }

    // pcl::io::savePLYFileASCII("pointCloud_0.ply", *cloud);
}

/*************/
void ZCameraImpl::loadFromPLY(std::string path, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
{
    pcl::io::loadPLYFile(path, *cloud);

    size_t pointCount = cloud->size();
    size_t width = ceil(std::sqrt(pointCount));
    size_t height = width;
    cloud->width = width;
    cloud->height = height;
    cloud->is_dense = false;
    cloud->resize(width * height);
    for (size_t i = 0; i < pointCount; i++)
    {
        cloud->points[i].r = 255.0f;
        cloud->points[i].g = 255.0f;
        cloud->points[i].b = 255.0f;

        cloud->points[i].normal_x = 1.0f;
        cloud->points[i].normal_y = 1.0f;
        cloud->points[i].normal_z = 1.0f;
    }
}

/*************/
void ZCameraImpl::getNoise(float xMax, float yMax, float zMax, int nbrPoints, std::vector<char>& cloud)
{
    PointCloudSerializer<pcl::PointXYZRGBNormal> serializer;
    unsigned long long timestamp = 0;
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr interm_cloud = boost::make_shared<pcl::PointCloud<pcl::PointXYZRGBNormal>>();

    ZCameraImpl::getNoise(xMax, yMax, zMax, nbrPoints, interm_cloud);

    cloud = serializer.serialize(interm_cloud, timestamp);
}

/*************/
void ZCameraImpl::getRandomMesh(float xMax, float yMax, float zMax, int nbrPolygons, pcl::PolygonMesh::Ptr mesh)
{
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud;
    cloud = boost::make_shared<pcl::PointCloud<pcl::PointXYZRGBNormal>>();
    getNoise(xMax, yMax, zMax, nbrPolygons * 3, cloud);

    pcl::toPCLPointCloud2(*cloud, mesh->cloud);

    mesh->polygons.resize(nbrPolygons);
    for (unsigned int i = 0; i < nbrPolygons; ++i)
    {
        mesh->polygons[i].vertices = {3 * i, 3 * i + 1, 3 * i + 2};
    }
}

/*************/
void ZCameraImpl::getRandomMesh(float xMax, float yMax, float zMax, int nbrPolygons, std::vector<unsigned char>& mesh)
{
    MeshSerializer serializer;
    unsigned long long timestamp = 0;

    pcl::PolygonMesh::Ptr interm_mesh = boost::make_shared<pcl::PolygonMesh>();

    ZCameraImpl::getRandomMesh(xMax, yMax, zMax, nbrPolygons, interm_mesh);

    mesh = serializer.serialize(interm_mesh, timestamp);
}

#if WITH_REALSENSE
/*************/
void ZCameraImpl::setRsOption(rs::option option, double value)
{
    if (_grabber->supports_option(option))
        _grabber->set_option(option, value);
    else
        cerr << "Realsense camera: tried to set unsupported option " << option << endl;
}
#endif

} // end of namespace
