#include "config.h"
#include "colorizeGL.h"
#if HAVE_GLFW
    #include "colorizeGL-impl.h"
#endif

using namespace std;

namespace posture
{
    /*********/
    ColorizeGL::ColorizeGL()
    {
#if HAVE_GLFW
        _impl = make_shared<ColorizeGLImpl>();
#endif
    }

    /*********/
    ColorizeGL::~ColorizeGL()
    {
    }

    /*********/
    vector<unsigned char> ColorizeGL::getTexture(unsigned int& width, unsigned int& height) const
    {
        if (!_impl)
            return vector<unsigned char>();

#if HAVE_GLFW
        return _impl->getTexture(width, height);
#endif
    }

    /*********/
    unsigned long long ColorizeGL::getTexturedMesh(std::vector<unsigned char>& mesh, bool threaded) const
    {
        if (!_impl)
            return 0;

#if HAVE_GLFW
        return _impl->getTexturedMesh(mesh, threaded);
#endif
    }

    /*********/
    unsigned long long ColorizeGL::getTexturedMesh(pcl::TextureMesh::Ptr mesh) const
    {
        if (!_impl)
            return 0;

#if HAVE_GLFW
        return _impl->getTexturedMesh(mesh);
#endif
    }

    /*********/
    void ColorizeGL::setCalibration(const vector<CalibrationParams>& calibrations) const
    {
        if (!_impl)
            return;
        
#if HAVE_GLFW
        return _impl->setCalibration(calibrations);
#endif
    }

    /*********/
    void ColorizeGL::setCompressMesh(bool active) const
    {
        if (!_impl)
            return;

#if HAVE_GLFW
        _impl->setCompressMesh(active);
#endif
    }

    /*********/
    void ColorizeGL::setInput(vector<unsigned char> mesh, vector<vector<unsigned char>> images, vector<vector<unsigned int>> dims) const
    {
        if (!_impl)
            return;

#if HAVE_GLFW
        _impl->setInput(mesh, images, dims);
#endif
    }

    /*************/
    void ColorizeGL::setInput(pcl::TextureMesh::Ptr mesh, std::vector<std::vector<unsigned char>> images, std::vector<std::vector<unsigned int>> dims) const
    {
        if (!_impl)
            return;

#if HAVE_GLFW
        _impl->setInput(mesh, images, dims);
#endif
    }

    /*************/
    void ColorizeGL::setInput(pcl::PolygonMesh::Ptr mesh, std::vector<std::vector<unsigned char>> images, std::vector<std::vector<unsigned int>> dims) const
    {
        if (!_impl)
            return;

#if HAVE_GLFW
        _impl->setInput(mesh, images, dims);
#endif
    }

    /*************/
    void ColorizeGL::setInput(std::vector<pcl::PolygonMesh::Ptr> multimesh, std::vector<std::vector<unsigned char>>& images, std::vector<std::vector<unsigned int>>& dims) const
    {
        if (!_impl)
            return;

#if HAVE_GLFW
        _impl->setInput(multimesh, images, dims);
#endif
    }

} // end of namespace
