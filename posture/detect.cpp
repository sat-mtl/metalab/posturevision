#include "detect.h"
#include "detect-impl.h"

using namespace std;

namespace posture
{
    /*********/
    Detect::Detect()
    {
        _impl = make_shared<DetectImpl>();
    }

    /*********/
    Detect::~Detect()
    {
    }

    /*********/
    bool Detect::detect()
    {
        if (!_impl)
            return false;

        return _impl->detect();
    }

    /*********/
    unsigned long long Detect::getProcessedCloud(std::vector<char>& cloud)
    {
        if (!_impl)
            return 0;

        return _impl->getProcessedCloud(cloud);
    }

    /*********/
    unsigned long long Detect::getProcessedCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
    {
        if (!_impl)
            return 0;

        return _impl->getProcessedCloud(cloud);
    }

    /*********/
    unsigned long long Detect::getConvexHull (std::vector<unsigned char> mesh, unsigned int index)
    {
        if (!_impl)
            return 0;

        return _impl->getConvexHull(mesh, index);
    }

    /*********/
    unsigned long long Detect::getConvexHull (pcl::PolygonMesh::Ptr mesh, unsigned int index)
    {
        if (!_impl)
            return 0;

        return _impl->getConvexHull(mesh, index);
    }

    /*********/
    vector<Shape> Detect::getDetectedShapes()
    {
        if (!_impl)
            return vector<Shape>();

        return _impl->getDetectedShapes();
    }

    /*********/
    void Detect::setInputCloud(const vector<char>& cloud, const bool compressed)
    {
        if (!_impl)
            return;

        _impl->setInputCloud(cloud, compressed);
    }

    /*********/
    void Detect::setInputCloud(const pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr& cloud)
    {
        if (!_impl)
            return;

        _impl->setInputCloud(cloud);
    }

} // end of namespace
