#include "detect-impl.h"

#include <algorithm>

#include <pcl/io/obj_io.h>

using namespace std;
using namespace pcl;

namespace posture
{
    /*********/
    DetectImpl::DetectImpl()
    {
        _sacSegmentation.setModelType(pcl::SACMODEL_NORMAL_PLANE);
        _sacSegmentation.setMethodType(pcl::SAC_RANSAC);
        _sacSegmentation.setNormalDistanceWeight(0.1);
        _sacSegmentation.setMaxIterations(100);
        _sacSegmentation.setDistanceThreshold(0.03);
    }

    /*********/
    DetectImpl::~DetectImpl()
    {
    }

    /*********/
    bool DetectImpl::detect()
    {
        lock_guard<mutex> lock(_mutex);

        _detectedCloud.resize(0);

        PointCloud<PointXYZRGBNormal>::Ptr remainingCloud = boost::make_shared<PointCloud<PointXYZRGBNormal>>();
        copyPointCloud(*_inputCloud, *remainingCloud);

        unsigned int nbrObjects = 0;
        while (nbrObjects < _maxObjects && _inputCloud->size() > _minPoints)
        {
            _sacSegmentation.setInputCloud(_inputCloud);
            _sacSegmentation.setInputNormals(_inputCloud);

            PointIndices::Ptr inliers = boost::make_shared<PointIndices>();
            ModelCoefficients::Ptr coeff = boost::make_shared<ModelCoefficients>();
            _sacSegmentation.segment(*inliers, *coeff);
            if (inliers->indices.size() == 0)
                break;

            _extract.setInputCloud(_inputCloud);
            _extract.setIndices(inliers);
            _extract.setNegative(false);

            PointCloud<PointXYZRGBNormal>::Ptr detectedCloud = boost::make_shared<PointCloud<PointXYZRGBNormal>>();
            _extract.filter(*detectedCloud);
            DetectedCloud object(detectedCloud, coeff);
            _detectedCloud.push_back(object);

            _extract.setNegative(true);
            _extract.filter(*remainingCloud);
            _inputCloud = remainingCloud;

            nbrObjects++;
        }

        if (nbrObjects == 0)
            return false;

        sort(_detectedCloud.begin(), _detectedCloud.end(), [](DetectedCloud a, DetectedCloud b) {
            return a.first->size() > b.first->size();
        });

        return true;
    }

    /*********/
    unsigned long long DetectImpl::getProcessedCloud(std::vector<char>& cloud)
    {
        pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr tempCloud = boost::make_shared<pcl::PointCloud<pcl::PointXYZRGBNormal>>();
        getProcessedCloud(tempCloud);
        cloud = _serializer.serialize(tempCloud, _timestamp);
        return _timestamp;
    }

    /*************/
    unsigned long long DetectImpl::getProcessedCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
    {
        lock_guard<mutex> lock(_mutex);

        if (_detectedCloud.size() == 0)
             return 0;

        for (auto& cloudd : _detectedCloud)
            *cloud += *(cloudd.first);

        return _timestamp;
    }

    /*********/
    unsigned long long DetectImpl::getConvexHull (std::vector<unsigned char> mesh, unsigned int index)
    {
        pcl::PolygonMesh::Ptr tempMesh = boost::make_shared<pcl::PolygonMesh>();
        getConvexHull (tempMesh, index);

        mesh = _meshSerializer.serialize(tempMesh, _timestamp);
        return _timestamp;
    }

    /*************/
    unsigned long long DetectImpl::getConvexHull (pcl::PolygonMesh::Ptr mesh, unsigned int index)
    {
        lock_guard<mutex> lock(_mutex);

        if (index >= _detectedCloud.size())
            return 0;

        _hull.setAlpha(0.1);
        _hull.setInputCloud(_detectedCloud[index].first);
        _hull.reconstruct(*mesh);

        return _timestamp;
    }

    /*********/
    vector<Shape> DetectImpl::getDetectedShapes()
    {
        return vector<Shape>();
    }

    /*********/
    void DetectImpl::setInputCloud(const vector<char>& cloud, const bool compressed)
    {
        lock_guard<mutex> lock(_mutex);

        _inputCloud = _serializer.deserialize(cloud, compressed, _timestamp);
    }

    /*************/
    void DetectImpl::setInputCloud(const pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr& cloud)
    {
        lock_guard<mutex> lock(_mutex);

        _inputCloud = cloud;
    }

} // end of namespace
