#include "pointcloudserializer.h"

#include <boost/make_shared.hpp>

namespace posture
{
    /*************/
    template <>
    std::vector<char> PointCloudBlob<pcl::PointXYZRGBA>::toBlob(typename pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud, const unsigned long long timestamp)
    {
        // Get the size for the blob
        const size_t cloudSize = cloud->size();
        const unsigned int blobSize = cloudSize * (3 + 1); // each point is 3 floats for xyz and one float for the color
    
        std::vector<char> blob(blobSize * sizeof(float) + sizeof(unsigned long long)); // Yes, we store floats inside a vector of char...
        float* blobPtr = (float*)blob.data();

        *(unsigned long long*)blobPtr = timestamp;
        blobPtr = (float*)((char*)blobPtr + sizeof(unsigned long long));
    
        for (unsigned int i = 0; i < cloudSize; ++i)
        {
            pcl::PointXYZRGBA* point = &(cloud->at(i));
            blobPtr[i*4 + 0] = point->x;
            blobPtr[i*4 + 1] = point->y;
            blobPtr[i*4 + 2] = point->z;
            blobPtr[i*4 + 3] = point->rgb;
        }
    
        return blob;
    }

    /*************/
    template <>
    std::vector<char> PointCloudBlob<pcl::PointXYZRGBNormal>::toBlob(typename pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud, const unsigned long long timestamp)
    {
        // Get the size for the blob
        const size_t cloudSize = cloud->size();
        const unsigned int blobSize = cloudSize * (3 + 3 + 1); // each point is 3 floats for xyz and one float for the color
    
        std::vector<char> blob(blobSize * sizeof(float) + sizeof(unsigned long long)); // Yes, we store floats inside a vector of char...
        float* blobPtr = (float*)blob.data();

        *(unsigned long long*)blobPtr = timestamp;
        blobPtr = (float*)((char*)blobPtr + sizeof(unsigned long long));
    
        for (unsigned int i = 0; i < cloudSize; ++i)
        {
            pcl::PointXYZRGBNormal* point = &(cloud->at(i));
            blobPtr[i*7 + 0] = point->x;
            blobPtr[i*7 + 1] = point->y;
            blobPtr[i*7 + 2] = point->z;
            blobPtr[i*7 + 3] = point->rgb;
            blobPtr[i*7 + 4] = point->normal_x;
            blobPtr[i*7 + 5] = point->normal_y;
            blobPtr[i*7 + 6] = point->normal_z;
        }
    
        return blob;
    }

    /*************/
    template <>
    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr PointCloudBlob<pcl::PointXYZRGBA>::toCloud(const std::vector<char>& blob, unsigned long long& timestamp)
    {
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGBA>());
    
        if (blob.size() == 0)
            return cloud;

        // Get the size of the cloud
        const unsigned int cloudSize = (blob.size() - sizeof(unsigned long long)) / (4 * sizeof(float)); // XYZ + RGBA (so 4 floats) stored in a vector of char

        unsigned long long* timestampPtr = (unsigned long long*)blob.data();
        timestamp = *timestampPtr;
    
        cloud->reserve(cloudSize);
        float* blobPtr = (float*)(blob.data() + sizeof(unsigned long long));
    
        for (unsigned int i = 0; i < cloudSize; ++i)
        {
            pcl::PointXYZRGBA point;
            point.x = blobPtr[i*4 + 0];
            point.y = blobPtr[i*4 + 1];
            point.z = blobPtr[i*4 + 2];
            point.rgb = blobPtr[i*4 + 3];
    
            cloud->push_back(point);
        }
    
        return cloud;
    }

    /*************/
    template <>
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr PointCloudBlob<pcl::PointXYZRGBNormal>::toCloud(const std::vector<char>& blob, unsigned long long& timestamp)
    {
        pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGBNormal>());
    
        if (blob.size() == 0)
            return cloud;

        // Get the size of the cloud
        const unsigned int cloudSize = (blob.size() - sizeof(unsigned long long)) / (7 * sizeof(float)); // XYZ + RGBA (so 4 floats) stored in a vector of char

        unsigned long long* timestampPtr = (unsigned long long*)blob.data();
        timestamp = *timestampPtr;
    
        cloud->reserve(cloudSize);
        float* blobPtr = (float*)(blob.data() + sizeof(unsigned long long));
    
        for (unsigned int i = 0; i < cloudSize; ++i)
        {
            pcl::PointXYZRGBNormal point;
            point.x = blobPtr[i*7 + 0];
            point.y = blobPtr[i*7 + 1];
            point.z = blobPtr[i*7 + 2];
            point.rgb = blobPtr[i*7 + 3];
            point.normal_x = blobPtr[i*7 + 4];
            point.normal_y = blobPtr[i*7 + 5];
            point.normal_z = blobPtr[i*7 + 6];
    
            cloud->push_back(point);
        }
    
        return cloud;
    }

    /*************/
    template <>
    PointCloudSerializer<pcl::PointXYZRGBA>::PointCloudSerializer()
    {
        _networkPointCloudEncoder = std::make_shared<NetworkPointCloudCompression<pcl::PointXYZRGBA>>();
        _blober = std::make_shared<PointCloudBlob<pcl::PointXYZRGBA>>();
    }

    template <>
    PointCloudSerializer<pcl::PointXYZRGBNormal>::PointCloudSerializer()
    {
        _blober = std::make_shared<PointCloudBlob<pcl::PointXYZRGBNormal>>();
    }

    /*************/
    template <>
    void PointCloudSerializer<pcl::PointXYZRGBA>::setCompression(const bool compress, pio::compression_Profiles_e compressionProfile, bool showStatistics,
                                          const double pointResolution, const double octreeResolution,
                                          bool doVoxelGridDownDownSampling, const unsigned int iFrameRate,
                                          bool doColorEncoding, const unsigned char colorBitResolution)
    {
        _compress = compress;
        if (compress == false)
            return;
        _networkPointCloudEncoder = std::make_shared<NetworkPointCloudCompression<pcl::PointXYZRGBA>>(compressionProfile, showStatistics, pointResolution,
                                                                            octreeResolution, doVoxelGridDownDownSampling, iFrameRate,
                                                                            doColorEncoding, colorBitResolution);
    }

    template <>
    void PointCloudSerializer<pcl::PointXYZRGBNormal>::setCompression(const bool compress, pio::compression_Profiles_e compressionProfile, bool showStatistics,
                                          const double pointResolution, const double octreeResolution,
                                          bool doVoxelGridDownDownSampling, const unsigned int iFrameRate,
                                          bool doColorEncoding, const unsigned char colorBitResolution)
    {
    }

    /*************/
    template <>
    PointCloudSerializer<pcl::PointXYZRGBA>::cloudPtr PointCloudSerializer<pcl::PointXYZRGBA>::deserialize(const std::vector<char>& sCloud, bool compressed, unsigned long long& timestamp) const
    {
        std::lock_guard<std::mutex> lock(_mutex);
        cloudPtr cloudOut = boost::make_shared<pcl::PointCloud<pcl::PointXYZRGBA>>();
    
        if (compressed)
        {
            timestamp = *(unsigned long long*)sCloud.data();

            std::stringstream compressedData;
            compressedData.write(sCloud.data() + sizeof(unsigned long long), sCloud.size() - sizeof(unsigned long long));
            _networkPointCloudEncoder->decodeNetworkPointCloud(compressedData, cloudOut);
        }
        else
        {
            cloudOut = _blober->toCloud(sCloud, timestamp);
        }

        cloudOut->is_dense = false; // We consider we lost density in the process
    
        return cloudOut;
    }

    /*************/
    template <>
    PointCloudSerializer<pcl::PointXYZRGBNormal>::cloudPtr PointCloudSerializer<pcl::PointXYZRGBNormal>::deserialize(const std::vector<char>& sCloud, bool compressed, unsigned long long& timestamp) const
    {
        std::lock_guard<std::mutex> lock(_mutex);
        cloudPtr cloudOut = boost::make_shared<pcl::PointCloud<pcl::PointXYZRGBNormal>>();
    
        cloudOut = _blober->toCloud(sCloud, timestamp);

        cloudOut->is_dense = false; // We consider we lost density in the process
    
        return cloudOut;
    }

    /*************/
    template <>
    std::vector<char> PointCloudSerializer<pcl::PointXYZRGBA>::serialize(const cloudPtr &cloud, unsigned long long timestamp) const
    {
        if (cloud.get() == NULL || cloud->size() == 0)
            return std::vector<char>();

        std::lock_guard<std::mutex> lock(_mutex);

        cloudPtr lCloud = const_cast<cloudPtr&>(cloud);
        std::vector<char> readBuffer;

        if (_compress)
        {
            stringstream compressedData;
            // TODO: We should check if the next line is necessary in the future. As of yet, without it the whole history
            // of all clouds is kepts unless a big change happens...
            _networkPointCloudEncoder->deleteTree();
            _networkPointCloudEncoder->encodePointCloud(lCloud, compressedData);
    
            readBuffer.resize(1e6);
            unsigned long long* timestampPtr = (unsigned long long*)readBuffer.data();
            *timestampPtr = timestamp;

            compressedData.read(readBuffer.data() + sizeof(unsigned long long), readBuffer.size() - sizeof(unsigned long long));
            unsigned int totalSize = compressedData.gcount();
            while (!compressedData.eof())
            {
                int previousSize = readBuffer.size();
                readBuffer.resize(readBuffer.size() + 1e6);
                compressedData.read(readBuffer.data() + previousSize, 1e6);
                totalSize += compressedData.gcount();
            }
            readBuffer.resize(totalSize);
        }
        else
        {
            readBuffer = _blober->toBlob(lCloud, timestamp);
        }
    
        return readBuffer;
    }

    /*************/
    template <>
    std::vector<char> PointCloudSerializer<pcl::PointXYZRGBNormal>::serialize(const cloudPtr &cloud, unsigned long long timestamp) const
    {
        if (cloud.get() == NULL || cloud->size() == 0)
            return std::vector<char>();

        std::lock_guard<std::mutex> lock(_mutex);

        cloudPtr lCloud = const_cast<cloudPtr&>(cloud);
        std::vector<char> readBuffer;
        readBuffer = _blober->toBlob(lCloud, timestamp);
    
        return readBuffer;
    }

} // end of namespace
