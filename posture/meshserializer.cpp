#include "meshserializer.h"

#include <cstring>
#include <iostream>
#include <iterator>
#include <vector>
#include <thread>
#include <future>

#include <boost/make_shared.hpp>
#include <openctm.h>

using namespace std;
using namespace pcl;

/*************/
// utility function for passing to ctmReadCustom()
// writes aCount characters from aBuf into a passed stream
static CTMuint writeToStream(const void *aBuf, CTMuint aCount, void *aUserData)  
{
        const char * charBuf = (const char *)aBuf;
        std::ostream * my_stream = (std::ostream *)aUserData;
        my_stream->write(charBuf, aCount);
        return aCount;
}

/*************/
// utility function for passing to ctmLoadCustom()
// reads aCount characters into aBuf from a passed stream 
static CTMuint readFromStream(void *aBuf, CTMuint aCount, void *aUserData)  
{
        char * charBuf = (char *)aBuf;
        std::istream * my_stream = (std::istream *)aUserData;
        my_stream->read(charBuf, aCount);
        return aCount;
}


namespace posture
{
    /*************/
    int is_big_endian(void)
    {
        union
        {
            uint32_t i;
            char c[4];
        } v = {0x01020304};

        return v.c[0] == 1;
    }

    /*************/
    MeshSerializer::MeshSerializer()
    {
    }

    /*************/
    MeshSerializer::~MeshSerializer()
    {
    }

    /*************/
    void MeshSerializer::convertToTris(PolygonMesh::Ptr& mesh) const
    {
        // Check that the polymesh needs conversion (has anything other than triangles)
        bool doConvert = false;
        for (auto& poly : mesh->polygons)
            if (poly.vertices.size() > 3)
                doConvert = true;
        if (!doConvert)
            return;

        PolygonMesh::Ptr tris = boost::make_shared<PolygonMesh>();
        tris->cloud = mesh->cloud;
        for (auto& poly : mesh->polygons)
        {
            // Very basic conversion from n-gon to tri
            for (int i = 0; i < poly.vertices.size() - 2; ++i)
            {
                Vertices vs;
                vs.vertices.push_back(poly.vertices[0]);
                vs.vertices.push_back(poly.vertices[i + 1]);
                vs.vertices.push_back(poly.vertices[i + 2]);
                tris->polygons.push_back(vs);
            }
        }

        mesh = tris;
    }

    /*************/
    TextureMesh::Ptr MeshSerializer::deserializeAsTextured(const std::vector<unsigned char>& inputMesh, unsigned long long& timestamp) const
    {
        std::string serializationType(inputMesh.begin(), inputMesh.begin()+8);
        if (serializationType == "MCTM-tex")
        {
            vector<TextureMesh::Ptr> submeshList = unbundleAsTexturedMulti(inputMesh, timestamp);
            posture::MeshMerger mm(submeshList.size());
            mm.setApplyCalibration(false);
            int i=0;
            for (auto& m: submeshList)
                mm.setInputMesh(i++, m);

            // put end result into output pointer
            TextureMesh::Ptr outputMesh;
            mm.getMesh(outputMesh);
            return outputMesh;
        }
        else if (serializationType.find("OCTM") == 0)
        {
            return deserializeAsTexturedCTM(inputMesh, timestamp);
        }
        else
        {
            return boost::make_shared<TextureMesh>();
        }
    }


    /*************/
    TextureMesh::Ptr MeshSerializer::deserializeAsTexturedCTM(const std::vector<unsigned char>& inputMesh, unsigned long long& timestamp) const
    {
        // to hold the mesh
        TextureMesh::Ptr polyMesh = boost::make_shared<TextureMesh>();

        if (inputMesh.size() == 0)
            return polyMesh;

        polyMesh->tex_polygons.resize(1);

        // uncompress it
        CTMimporter ctmi;
        std::string serialization(inputMesh.begin(), inputMesh.end());
        std::istringstream loadingStream(serialization);

        try
        {
            ctmi.LoadCustom(&readFromStream, (void*)&loadingStream);
        }
        catch (const ctm_error e)
        {
            cerr << "MeshSerializer::" << __FUNCTION__ << " - Caught exception: " << e.what() << endl;
            return polyMesh;
        }

        auto nbrVertices = ctmi.GetInteger(CTM_VERTEX_COUNT);
        auto vertices = ctmi.GetFloatArray(CTM_VERTICES);    // should be freed upon importer destruction

        auto nbrPolys = ctmi.GetInteger(CTM_TRIANGLE_COUNT);
        auto polyIndices = ctmi.GetIntegerArray(CTM_INDICES);

        auto uvMapRef = ctmi.GetNamedUVMap("UV1");
        auto uvCoords = ctmi.GetFloatArray(uvMapRef);

        // set up the point cloud structure 
        int field_offset = 0;
        for (int i = 0; i < 6; ++i, field_offset += 4)
        {
            polyMesh->cloud.fields.push_back(pcl::PCLPointField());
            polyMesh->cloud.fields[i].offset = field_offset;
            polyMesh->cloud.fields[i].datatype = pcl::PCLPointField::FLOAT32;
            polyMesh->cloud.fields[i].count = 1;
        }
        polyMesh->cloud.fields[0].name = "x";
        polyMesh->cloud.fields[1].name = "y";
        polyMesh->cloud.fields[2].name = "z";
        polyMesh->cloud.fields[3].name = "normal_x";
        polyMesh->cloud.fields[4].name = "normal_y";
        polyMesh->cloud.fields[5].name = "normal_z";

        polyMesh->cloud.point_step = field_offset;
        polyMesh->cloud.width = nbrVertices;
        polyMesh->cloud.height = 1;
        polyMesh->cloud.row_step = field_offset * nbrVertices;
        polyMesh->cloud.is_dense = true;
        polyMesh->cloud.data.resize(nbrVertices * field_offset);

        polyMesh->tex_polygons[0].resize(nbrPolys);
        polyMesh->tex_coordinates.push_back(vector<Eigen::Vector2f, Eigen::aligned_allocator<Eigen::Vector2f>>(nbrPolys * 3));

        int pointSize = field_offset;      
        
        // write vertex information from vertices[] and polyIndices[] into point cloud data container
        for (int i = 0; i < nbrVertices; ++i)
        {
            int xyzn = 0;
            for (size_t d = 0; d < polyMesh->cloud.fields.size(); ++d)
            {
                if ((polyMesh->cloud.fields[d].datatype != pcl::PCLPointField::FLOAT32))
                    continue;

                if (polyMesh->cloud.fields[d].name == "x")
                    memcpy(&polyMesh->cloud.data[i * pointSize + polyMesh->cloud.fields[d].offset], &vertices[i * 3 + 0], sizeof(float));
                else if (polyMesh->cloud.fields[d].name == "y")
                    memcpy(&polyMesh->cloud.data[i * pointSize + polyMesh->cloud.fields[d].offset], &vertices[i * 3 + 1], sizeof(float));
                else if (polyMesh->cloud.fields[d].name == "z")
                    memcpy(&polyMesh->cloud.data[i * pointSize + polyMesh->cloud.fields[d].offset], &vertices[i * 3 + 2], sizeof(float));

                if (++xyzn == 3)
                    break;
            }
        }
        
        // write connectivity information and UV data into PolygonMesh
        for (int i = 0; i < nbrPolys; ++i)
            for (int j = 0; j < 3; ++j)
            {
                int thisVertexIndex = polyIndices[i * 3 + j];
                polyMesh->tex_polygons[0][i].vertices.push_back(thisVertexIndex);

                polyMesh->tex_coordinates[0][i * 3 + j][0] = uvCoords[2 * thisVertexIndex + 0];
                polyMesh->tex_coordinates[0][i * 3 + j][1] = uvCoords[2 * thisVertexIndex + 1];
            }
        return polyMesh;              
    }

    /*************/
    PolygonMesh::Ptr MeshSerializer::deserialize(const std::vector<unsigned char>& inputMesh, unsigned long long& timestamp) const
    {
        std::string serializationType(inputMesh.begin(), inputMesh.begin()+8);
        if (serializationType != "MCTM-pol")
        {
            return deserializeCTM(inputMesh, timestamp);
        }
        else
        {
            vector<PolygonMesh::Ptr> submeshList = unbundleMulti(inputMesh, timestamp);
            posture::MeshMerger mm(submeshList.size());
            mm.setApplyCalibration(false);
            int i=0;
            for (auto& m: submeshList)
                mm.setInputMesh(i++, m);

            // put end result into output pointer
            PolygonMesh::Ptr outputMesh;
            mm.getMesh(outputMesh);
            return outputMesh;
        }
    }

    /*************/
    PolygonMesh::Ptr MeshSerializer::deserializeCTM(const std::vector<unsigned char>& inputMesh, unsigned long long& timestamp) const
    {
        // to hold the mesh
        PolygonMesh::Ptr polyMesh = boost::make_shared<PolygonMesh>();
        if (inputMesh.size() == 0)
            return polyMesh;

        // uncompress it
        CTMimporter ctmi;
        std::string serialization(inputMesh.begin(), inputMesh.end());
        std::istringstream loadingStream(serialization);

        try
        {
            ctmi.LoadCustom(&readFromStream, (void*)&loadingStream);
        }
        catch (const ctm_error e)
        {
            cerr << "MeshSerializer::" << __FUNCTION__ << " - Caught exception: " << e.what() << endl;
            return polyMesh;
        }

        auto nbrVertices = ctmi.GetInteger(CTM_VERTEX_COUNT);
        auto vertices = ctmi.GetFloatArray(CTM_VERTICES);    // should be freed upon importer destruction

        auto nbrPolys = ctmi.GetInteger(CTM_TRIANGLE_COUNT);
        auto polyIndices = ctmi.GetIntegerArray(CTM_INDICES);

        auto uvMapRef = ctmi.GetNamedUVMap("UV1");
        auto uvCoords = ctmi.GetFloatArray(uvMapRef);

        // set up the point cloud structure (lifted from old deserialize() )
        int field_offset = 0;
        for (int i = 0; i < 6; ++i, field_offset += 4)
        {
            polyMesh->cloud.fields.push_back(pcl::PCLPointField());
            polyMesh->cloud.fields[i].offset = field_offset;
            polyMesh->cloud.fields[i].datatype = pcl::PCLPointField::FLOAT32;
            polyMesh->cloud.fields[i].count = 1;
        }
        polyMesh->cloud.fields[0].name = "x";
        polyMesh->cloud.fields[1].name = "y";
        polyMesh->cloud.fields[2].name = "z";
        polyMesh->cloud.fields[3].name = "normal_x";
        polyMesh->cloud.fields[4].name = "normal_y";
        polyMesh->cloud.fields[5].name = "normal_z";

        polyMesh->cloud.point_step = field_offset;
        polyMesh->cloud.width = nbrVertices;
        polyMesh->cloud.height = 1;
        polyMesh->cloud.row_step = field_offset * nbrVertices;
        polyMesh->cloud.is_dense = true;
        polyMesh->cloud.data.resize(nbrVertices * field_offset);

        polyMesh->polygons.resize(nbrPolys);

        int pointSize = field_offset;      
        
        // write vertex information from vertices[] and polyIndices[] into point cloud data container
        for (int i = 0; i < nbrVertices; ++i)
        {
            int xyzn = 0;
            for (size_t d = 0; d < polyMesh->cloud.fields.size(); ++d)
            {
                if ((polyMesh->cloud.fields[d].datatype != pcl::PCLPointField::FLOAT32))
                    continue;

                if (polyMesh->cloud.fields[d].name == "x")
                    memcpy(&polyMesh->cloud.data[i * pointSize + polyMesh->cloud.fields[d].offset], &vertices[i * 3 + 0], sizeof(float));
                else if (polyMesh->cloud.fields[d].name == "y")
                    memcpy(&polyMesh->cloud.data[i * pointSize + polyMesh->cloud.fields[d].offset], &vertices[i * 3 + 1], sizeof(float));
                else if (polyMesh->cloud.fields[d].name == "z")
                    memcpy(&polyMesh->cloud.data[i * pointSize + polyMesh->cloud.fields[d].offset], &vertices[i * 3 + 2], sizeof(float));

                if (++xyzn == 3)
                    break;
            }
        }

        // write connectivity information and UV data into PolygonMesh
        for (int i = 0; i < nbrPolys; ++i)
            for (int j = 0; j < 3; ++j)
            {
                int vertex = polyIndices[i * 3 + j];
                polyMesh->polygons[i].vertices.push_back(vertex);

                // Only works with Textured Mesh
                // polyMesh->tex_coordinates[0][i * 3 + j][0] = uvCoords[2 * i + 0];
                // polyMesh->tex_coordinates[0][i * 3 + j][1] = uvCoords[2 * i + 1];
            }

        return polyMesh;
    }

    /*************/
    // Non-textured mesh - puts blank UV data into serialization
    std::vector<unsigned char> MeshSerializer::serialize(const PolygonMesh::Ptr inputMesh, unsigned long long timestamp) const
    {
        PolygonMesh::Ptr mesh = inputMesh;

        // Yes, we can only handle triangles. Deal with it.
        convertToTris(mesh); 

        unsigned int nbrVertices = static_cast<unsigned int>(mesh->cloud.width * mesh->cloud.height);
        unsigned int nbrPolys = static_cast<unsigned int>(mesh->polygons.size());

        if (nbrVertices == 0)
            return std::vector<unsigned char>();

        CTMexporter ctmx;

        // create vertex positions list (float array) for ctm
        // x0 y0 z0 x1 y1 z1 ...
        auto aVertices = unique_ptr<CTMfloat[]>(new CTMfloat[3 * nbrVertices]); 
        unsigned int pointSize = static_cast<unsigned int>(mesh->cloud.data.size() / nbrVertices);

        for (int i = 0; i < nbrVertices; ++i)
        {
           int xyzn = 0;
           for (size_t d = 0; d < mesh->cloud.fields.size(); ++d)
           {
                if ((mesh->cloud.fields[d].datatype != pcl::PCLPointField::FLOAT32))
                    continue;

                if (mesh->cloud.fields[d].name == "x")
                    memcpy(&aVertices[i * 3 + 0], &mesh->cloud.data[i * pointSize + mesh->cloud.fields[d].offset], sizeof(float));
                else if (mesh->cloud.fields[d].name == "y")
                    memcpy(&aVertices[i * 3 + 1], &mesh->cloud.data[i * pointSize + mesh->cloud.fields[d].offset], sizeof(float));
                else if (mesh->cloud.fields[d].name == "z")
                    memcpy(&aVertices[i * 3 + 2], &mesh->cloud.data[i * pointSize + mesh->cloud.fields[d].offset], sizeof(float));

                if (++xyzn == 3)
                    break;
            }
        }

        // now, connectivity.
        auto aIndices = unique_ptr<CTMuint[]>(new CTMuint[3 * nbrPolys]);
        
        for (int i = 0; i < nbrPolys; ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                aIndices[i * 3 + j] = mesh->polygons[i].vertices[j];
            }
        }

        ctmx.DefineMesh(&aVertices[0], nbrVertices, &aIndices[0], nbrPolys, NULL);

        // UV coordinates all set to 0.0f.
        std::vector<float> uvData(2*nbrVertices);
        ctmx.AddUVMap(uvData.data(), "UV1", NULL);

        // set up compression parameters
        ctmx.CompressionMethod(_ctmCompressionMethod); // _RAW, _MG1 (Lossless), _MG2 (Lossy)
        ctmx.CompressionLevel(_ctmCompressionLevel);
        ctmx.VertexPrecision(_ctmVertexPrecision);

        // save into a string
        std::ostringstream saveStream;
        try
        {
           ctmx.SaveCustom( &writeToStream, (void*)&saveStream);
        }
        catch (const ctm_error e)
        {
            cerr << "MeshSerializer::" << __FUNCTION__ << " - Caught exception: " << e.what() << endl;
            return std::vector<unsigned char>();
        }
        std::string serialization = saveStream.str();

        std::vector<unsigned char> buffer(serialization.begin(), serialization.end());
        return buffer;
    }
 
    /*************/
    // TextureMesh serialization
    std::vector<unsigned char> MeshSerializer::serialize(const TextureMesh::Ptr mesh, unsigned long long timestamp) const
    {
        // NOTE: this function assumes the TextureMesh only has triangles.

        if (mesh->tex_polygons.size() == 0)
            return std::vector<unsigned char>();

        unsigned int nbrVertices = static_cast<unsigned int>(mesh->cloud.width * mesh->cloud.height);
        unsigned int nbrPolys = static_cast<unsigned int>(mesh->tex_polygons[0].size());

        if (nbrVertices == 0)
            return std::vector<unsigned char>();

        CTMexporter ctmx;

        // create vertex positions list (float array) for ctm
        // x0 y0 z0 x1 y1 z1 ...
        auto aVertices = unique_ptr<CTMfloat[]>(new CTMfloat[3 * nbrVertices]); 
        
        unsigned int pointSize = static_cast<unsigned int>(mesh->cloud.data.size() / nbrVertices);

        for (int i = 0; i < nbrVertices; ++i)
        {
           int xyzn = 0;
           for (size_t d = 0; d < mesh->cloud.fields.size(); ++d)
           {
                if ((mesh->cloud.fields[d].datatype != pcl::PCLPointField::FLOAT32))
                    continue;

                if (mesh->cloud.fields[d].name == "x")
                    memcpy(&aVertices[i * 3 + 0], &mesh->cloud.data[i * pointSize + mesh->cloud.fields[d].offset], sizeof(float));
                else if (mesh->cloud.fields[d].name == "y")
                    memcpy(&aVertices[i * 3 + 1], &mesh->cloud.data[i * pointSize + mesh->cloud.fields[d].offset], sizeof(float));
                else if (mesh->cloud.fields[d].name == "z")
                    memcpy(&aVertices[i * 3 + 2], &mesh->cloud.data[i * pointSize + mesh->cloud.fields[d].offset], sizeof(float));

                if (++xyzn == 3)
                    break;
            }
        }

        // now, connectivity and UV coordinates.
        auto aIndices = unique_ptr<CTMuint[]>(new CTMuint[3 * nbrPolys]);
        std::vector<float> uvData(2*nbrVertices);
        
        for (int i = 0; i < nbrPolys; ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                int thisVertexIndex = mesh->tex_polygons[0][i].vertices[j];  
                aIndices[i * 3 + j] = thisVertexIndex;

                // If there are no UVs, leave them to 0.0
                if (mesh->tex_coordinates.size() == 0 || mesh->tex_coordinates[0].size() < i * 3 + j)
                    continue;

                // We store only one UV per vertex, in the range [0,1]; the last one written is the one kept
                if (mesh->tex_coordinates[0][i * 3 + j][0] >= 0.f && mesh->tex_coordinates[0][i * 3 + j][0] <= 1.f)
                {                    
                    uvData[thisVertexIndex * 2 + 0] = mesh->tex_coordinates[0][i * 3 + j][0];
                    uvData[thisVertexIndex * 2 + 1] = mesh->tex_coordinates[0][i * 3 + j][1];
                }
            }
        }

        ctmx.DefineMesh((CTMfloat*)aVertices.get(), nbrVertices, (CTMuint*)aIndices.get(), nbrPolys, NULL);
        ctmx.AddUVMap(uvData.data(), "UV1", NULL);

        // set up compression parameters
        ctmx.CompressionMethod(_ctmCompressionMethod); // _RAW, _MG1 (Lossless), _MG2 (Lossy)
        ctmx.CompressionLevel(_ctmCompressionLevel);
        ctmx.VertexPrecision(_ctmVertexPrecision);

        // save into a string
        std::ostringstream saveStream;
        ctmx.SaveCustom( &writeToStream, (void*)&saveStream);
        std::string serialization = saveStream.str();
        std::vector<unsigned char> buffer(serialization.begin(), serialization.end());
    
        return buffer;
    }

    /*************/
    // Multi-Polygon Mesh serialization (uses concurrency, one task per submesh)
    vector<unsigned char> MeshSerializer::serialize(const vector<pcl::PolygonMesh::Ptr> submeshList, unsigned long long timestamp) const
    {
        // launch concurrent serialization of all the submeshes
        vector <std::future <vector<unsigned char> > > futureSerializations;
        
        for (auto submesh: submeshList) // serialize is not static: calling serializeOne 
        {
            futureSerializations.push_back(std::async(std::launch::async,
                                                      serializeOneP, 
                                                      submesh, 
                                                      timestamp, 
                                                      _ctmCompressionMethod, 
                                                      _ctmCompressionLevel, 
                                                      _ctmVertexPrecision));
        }

        // get the results
        vector<vector<unsigned char>> serializations;
        for (auto& f: futureSerializations)
            serializations.push_back(f.get());

        // now put it all together
        std::ostringstream resultStream;
        
        // write out a header specifying  
        // 1. that this is a MCTM (multi-CTM) buffer of PolygonMeshes (other version: "MCTM-texMesh ")
        resultStream << "MCTM-polyMesh ";
        // 2. the number of OCTM buffers included
        resultStream << submeshList.size();
        // 3. the lengths of each buffer
        for (auto& s: serializations)
        {
            resultStream << " " << s.size();
        }
        // 4. some padding to get to a fixed 512 bytes header
        int headerLen = resultStream.str().size();
        int padLen = 512 - headerLen;
        resultStream << string(padLen,' '); 

        // now write out the concatenation of serializations
        for (auto& s: serializations)
            std::copy(s.begin(), s.end(), ostream_iterator<char>(resultStream));

        std::string serialization = resultStream.str();

        return vector<unsigned char>(serialization.begin(), serialization.end());
    }

    /*************/
    // Multi-Textured Mesh serialization (uses concurrency, one task per submesh)
    // we just copied the Multi-Polygon Mesh code and replaced PolygonMesh with TextureMesh, MCTM-polyMesh with MCTM-texMesh 
    // TODO: Templated version
    vector<unsigned char> MeshSerializer::serialize(const vector<pcl::TextureMesh::Ptr> submeshList, unsigned long long timestamp) const
    {
        // launch concurrent serialization of all the submeshes
        vector<std::future<vector<unsigned char>>> futureSerializations;
        
        for (auto submesh: submeshList) // serialize is not static: calling serializeOne 
        {
            futureSerializations.push_back(std::async(std::launch::async,
            //  [&]()->vector<unsigned char>{ serialize(submesh,timestamp); } ) );  // beware the lambda calling a non-static method ;-)
                serializeOneT, submesh, timestamp, 
                          _ctmCompressionMethod, _ctmCompressionLevel, _ctmVertexPrecision) );
        }

        // get the results
        vector<vector<unsigned char> > serializations;
        for (auto& f: futureSerializations)
            serializations.push_back(f.get());

        // now put it all together
        std::ostringstream resultStream;
        
        // write out a header specifying  
        // 1. that this is a MCTM (multi-CTM) buffer of TextureMeshes
        resultStream << "MCTM-texMesh ";
        // 2. the number of OCTM buffers included
        resultStream << submeshList.size();
        // 3. the lengths of each buffer
        for (auto& s: serializations)
        {
            resultStream << " " << s.size();
        }
        // 4. some padding to get to a fixed 512 bytes header
        int headerLen = resultStream.str().size();
        int padLen = 512 - headerLen;
        resultStream << string(padLen,' '); 

        // now write out the concatenation of serializations
        for (auto& s: serializations)
            std::copy(s.begin(), s.end(), ostream_iterator<char>(resultStream));

        std::string serialization = resultStream.str();

        return vector<unsigned char>(serialization.begin(), serialization.end());
    }

    /************/
    // Multi-Polygon Mesh deserialization (uses concurrency, one task per submesh)
    vector<PolygonMesh::Ptr> MeshSerializer::unbundleMulti(const std::vector<unsigned char>& inputBuffer, unsigned long long& timestamp) const
    {
       // header is always 512 bytes long; remainder is the payload
       std::string header(inputBuffer.begin(), inputBuffer.begin() + 512);
       std::vector<unsigned char> payload(inputBuffer.begin() + 512, inputBuffer.end());
    
       // read header and split payload
       std::istringstream headerReader(header);
    
        std::string bufferType;
       headerReader >> bufferType;
       if (bufferType != "MCTM-polyMesh")
           throw(9999);
           
       int nPieces;
       headerReader >> nPieces;
       
       int pieceSize;
       std::vector<std::vector<unsigned char> > serializations;
       int pos = 0;
       for (int i = 0; i< nPieces; ++i)
       {
           headerReader >> pieceSize; 
           serializations.push_back(std::vector<unsigned char>
                                     (&payload[pos], &payload[pos + pieceSize]) );
           pos += pieceSize;
       }
       assert(pos == payload.size());
       
       // launch concurrent deserialization of all the serializations
       vector<std::future<PolygonMesh::Ptr>> futureMeshes;              
       for (auto thisPiece: serializations) // serialize is not static, hope this works
       {   
           futureMeshes.push_back(std::async(std::launch::async,
                                             deserializeOne,
                                             thisPiece,
                                             std::ref(timestamp)));
       }
       
       std::vector<PolygonMesh::Ptr> submeshList;
       for (auto& futureMesh: futureMeshes)
       {
           submeshList.push_back(futureMesh.get()); // join the threads. now submeshList is full & ready
       }
    
       return submeshList;
    }
           
    /************/
    // Multi-Textured Mesh deserialization (uses concurrency, one task per submesh)
    // we just copied unbundleMulti and replaced Polygon with Texture, and polyMesh with texMesh and deserializeOne with deserializeAsTextureOne
    // TODO: template version to reduce code duplication
    vector<TextureMesh::Ptr> MeshSerializer::unbundleAsTexturedMulti(const std::vector<unsigned char>& inputBuffer, unsigned long long& timestamp) const
    {
       // header is always 512 bytes long; remainder is the payload
       std::string header(inputBuffer.begin(), inputBuffer.begin() + 512);
       std::vector<unsigned char> payload(inputBuffer.begin() + 512, inputBuffer.end());
    
       // read header and split payload
       std::istringstream headerReader(header);
    
       std::string bufferType;
       headerReader >> bufferType;
       if (bufferType != "MCTM-texMesh")
           return {};
           
       int nPieces;
       headerReader >> nPieces;
       
       int pieceSize;
       std::vector<std::vector<unsigned char>> serializations;
       int pos = 0;
       for (int i = 0; i< nPieces; ++i)
       {
           headerReader >> pieceSize; 
           serializations.push_back(std::vector<unsigned char>(&payload[pos], &payload[pos + pieceSize]));
           pos += pieceSize;
       }
       assert(pos == payload.size());
       
       // launch concurrent deserialization of all the serializations
       vector <std::future <TextureMesh::Ptr> > futureMeshes;              
       for (auto thisPiece: serializations) // serialize is not static, hope this works
       {   
           futureMeshes.push_back(std::async(std::launch::async,
                                             deserializeAsTexturedOne,
                                             thisPiece,
                                             std::ref(timestamp)));
       }
       
       std::vector<TextureMesh::Ptr> submeshList;
       for (auto& futureMesh: futureMeshes)
       {
           submeshList.push_back(futureMesh.get()); // join the threads. now submeshList is full & ready
       }
    
       return submeshList;
    }

    // these 'Multi' utility functions can perhaps be made into lambdas in their callers:
    /*************/
    // static, instantiates a new deserializer for each concurrent execution
    PolygonMesh::Ptr MeshSerializer::deserializeOne(const vector<unsigned char>& serialization, unsigned long long& timestamp)
    {
        MeshSerializer ms;
        return ms.deserialize(serialization, timestamp);
    }
    
    /*************/
    // static, instantiates a new deserializer for each concurrent execution
    TextureMesh::Ptr MeshSerializer::deserializeAsTexturedOne(const vector<unsigned char>& serialization, unsigned long long& timestamp)
    {
        MeshSerializer ms;
        return ms.deserializeAsTexturedCTM(serialization, timestamp);
    }

    /*************/
    // static, instantiates a new serializer for each concurrent execution with the same parameters as its parent     
    std::vector<unsigned char> MeshSerializer::serializeOneP(const PolygonMesh::Ptr& submesh, unsigned long long timestamp, CTMenum cm, int cl, float cvp) 
    {
        MeshSerializer ms;
        ms.setCompressionMethod(cm, cl, cvp);
        return ms.serialize(submesh, timestamp);
    }

    /*************/
    // static, instantiates a new serializer for each concurrent execution with the same parameters as its parent     
    std::vector<unsigned char> MeshSerializer::serializeOneT(const TextureMesh::Ptr& submesh, unsigned long long timestamp, CTMenum cm, int cl, float cvp) 
    {
        MeshSerializer ms;
        ms.setCompressionMethod(cm, cl, cvp);
        return ms.serialize(submesh, timestamp);
    }

} // end of namespace
