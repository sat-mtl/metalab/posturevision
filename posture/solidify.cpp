#include "solidify.h"
#include "solidify-impl.h"

using namespace std;

namespace posture
{
    /*************/
    Solidify::Solidify()
    {
        _impl = make_shared<SolidifyImpl>();
    }
    
    /*************/
    Solidify::~Solidify()
    {
    }
    
    /*************/
    unsigned long long Solidify::getMesh(std::vector<unsigned char>& mesh)
    {
        if (!_impl)
            return 0;

        return _impl->getMesh(mesh);
    }

    /*************/
    unsigned long long Solidify::getMesh(pcl::PolygonMesh::Ptr mesh)
    {
        if (!_impl)
            return 0;

        return _impl->getMesh(mesh);
    }

    /*************/
    void Solidify::setGridResolution(int res)
    {
        if (!_impl)
            return;
    
        _impl->setGridResolution(res);
    }
    
    /*************/
    void Solidify::setInputCloud(vector<char> cloud, bool compressed)
    {
        if (!_impl)
            return;
    
        return _impl->setInputCloud(cloud, compressed);
    }

    /*************/
    void Solidify::setInputCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
    {
        if (!_impl)
            return;

        return _impl->setInputCloud(cloud);
    }

    /*************/
    void Solidify::setSaveMesh(bool active)
    {
        if (!_impl)
            return;
        
        _impl->setSaveMesh(active);
    }

} // end of namespace
