#include <boost/make_shared.hpp>
#include <pcl/conversions.h>

#include "directmesher.h"
#include "filter.h"

using namespace std;

namespace posture
{
    /*************/
    DirectMesher::DirectMesher(int pixelSize, float angleTolerance)
    {
        ofm.setTrianglePixelSize (pixelSize);
        //ofm.setTrianglePixelSizeRows(pixelSize);
        //ofm.setTrianglePixelSizeColumns(2*pixelSize);
        ofm.setAngleTolerance(pcl::deg2rad(angleTolerance));
        ofm.setTriangulationType (pcl::OrganizedFastMesh<pcl::PointXYZ>::TRIANGLE_ADAPTIVE_CUT);
        ofm.storeShadowedFaces(false);
    }


    /*************/
    DirectMesher::~DirectMesher()
    {
    }


    /*************/
    pcl::PointCloud<pcl::PointXYZ>::Ptr
    DirectMesher::convertToXYZPointCloud (const vector<unsigned short>& depth_image, int width, int height, int cameraNo) const
    {
        // adapted from pcl/openni2_grabber.cpp

        TRACE(cerr << "received depth image, size = " << depth_image.size() << " = " << width << "x" << height << endl;)
        TRACE(cerr << "coming from camera #" << cameraNo << endl;)

        // set up the cloud object
        auto cloud = boost::make_shared<pcl::PointCloud<pcl::PointXYZ> >();
        //cloud->header.seq = depth_image->getFrameID ();
        //cloud->header.stamp = depth_image->getTimestamp ();
        cloud->height = height;
        cloud->width = width;
        cloud->is_dense = false;
        cloud->points.resize (cloud->height * cloud->width);

        // set up camera focal length     
        float constant_x = 1.0f / _calibrationParams[cameraNo].rgb_focal; 
        float constant_y = 1.0f / _calibrationParams[cameraNo].rgb_focal;
        float centerX = ((float)cloud->width - 1.f) / 2.f;
        float centerY = ((float)cloud->height - 1.f) / 2.f;

        float bad_point = std::numeric_limits<float>::quiet_NaN ();

        int array_idx = 0;
        for (int v = 0; v < height; ++v)
        {
          for (int u = 0; u < width; ++u, ++array_idx)
          {
            pcl::PointXYZ& pt = cloud->points[array_idx];
            // Check for invalid measurements
            if (depth_image[array_idx] == 0) /* ||
                depth_image[array_idx] == depth_image->getNoSampleValue () ||
                depth_image[array_idx] == depth_image->getShadowValue ()) */
            {
              // not valid
              pt.x = pt.y = pt.z = bad_point;
              continue;
            }
            pt.z = depth_image[array_idx] * 0.001f;
            pt.x = (static_cast<float> (u) - centerX) * pt.z * constant_x;
            pt.y = (static_cast<float> (v) - centerY) * pt.z * constant_y;
          }
        }

        // finish up cloud setup
        cloud->sensor_origin_.setZero ();
        cloud->sensor_orientation_.w () = 1.0f;
        cloud->sensor_orientation_.x () = 0.0f;
        cloud->sensor_orientation_.y () = 0.0f;
        cloud->sensor_orientation_.z () = 0.0f;
        return cloud;
    }


    /*************/
    unsigned long long DirectMesher::getMesh(const pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud, pcl::PolygonMesh::Ptr& mesh) {

        // quick, make a copy of this cloud before it gets overwritten
        boost::shared_ptr<pcl::PointCloud<pcl::PointXYZ> > newCloud (new pcl::PointCloud<pcl::PointXYZ>);
        *newCloud = *cloud;

        // Store the connectivity reconstruction results in vertices vector
        ofm.setInputCloud(newCloud);
        boost::shared_ptr<std::vector<pcl::Vertices> > vertices (new std::vector<pcl::Vertices>);
        ofm.reconstruct (*vertices);
        
        // Now write it into the mesh 
        pcl::toPCLPointCloud2 (*cloud, mesh->cloud);
        mesh->polygons = *vertices;
        
        // Finally, filter out the NaNs
        TRACE(cerr << "DirectMesher::getMesh: filtering mesh" << endl;)
        posture::Filter f;
        f.filterNaN(mesh); 

        return 0;
    }

} // end of namespace
