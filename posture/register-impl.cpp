#include "register-impl.h"

#include <iostream>

using namespace std;
using namespace pcl;

namespace posture
{
    /*********/
    RegisterImpl::RegisterImpl()
    {
        _icp.setMaximumIterations(16);
        _icp.setMaxCorrespondenceDistance(0.05);
        _icp.setTransformationEpsilon(0.0001);
        _icp.setEuclideanFitnessEpsilon(0.01);
    }

    /*********/
    RegisterImpl::~RegisterImpl()
    {
    }

    /*********/
    vector<CalibrationParams> RegisterImpl::getCalibration()
    {
        unique_lock<mutex> lock(_mutex);

        if (!_pointClouds.size() || !_guessedCalibrationParams.size())
            return _calibrationParams;

        if (!_pointClouds[0] || !_guessedCalibrationParams[0].isValid)
            return _calibrationParams;

        // Point clouds and calibration points are copied
        // to allow for their update by another thread for the next call to getCalibration
        auto pointClouds = _pointClouds;
        auto guessedCalibrationParams = _guessedCalibrationParams;

        lock.unlock();

        // We try to match all clouds against the first one
        PointCloud<PointXYZRGBNormal>::Ptr transformedTargetCloud = boost::make_shared<PointCloud<PointXYZRGBNormal>>();
        transformPointCloudWithNormals(*pointClouds[0], *transformedTargetCloud, guessedCalibrationParams[0].getTransformation());
        _icp.setInputTarget(transformedTargetCloud);
        _calibrationParams[0] = guessedCalibrationParams[0];

        // Go through all other clouds
        for (uint32_t index = 1; index < std::min(guessedCalibrationParams.size(), pointClouds.size()); ++index)
        {
            _calibrationParams[index] = guessedCalibrationParams[index];

            if (!pointClouds[index]->size())
                continue;

            if (!guessedCalibrationParams[index].isValid)
                continue;

            PointCloud<PointXYZRGBNormal> finalCloud;
            _icp.setInputSource(pointClouds[index]);
            _icp.align(finalCloud, guessedCalibrationParams[index].getTransformation());

            _calibrationParams[index].overrideTransformation(_icp.getFinalTransformation());
        }

        return _calibrationParams;
    }

    /*********/
    void RegisterImpl::setInputCloud(const unsigned int index, const vector<char>& cloud, bool compressed)
    {
        unique_lock<mutex> lock(_mutex);

        if (_pointClouds.size() < index + 1)
        {
            _pointClouds.resize(index + 1);
            _timestamps.resize(index + 1);
        }

        _pointClouds[index] = _serializer.deserialize(cloud, compressed, _timestamps[index]);
    }

    /*********/
    void RegisterImpl::setInputCloud(const unsigned int index, const PointCloud<PointXYZRGBNormal>::Ptr cloud)
    {
        unique_lock<mutex> lock(_mutex);

        if (_pointClouds.size() < index + 1)
        {
            _pointClouds.resize(index + 1);
            _timestamps.resize(index + 1);
        }

        _pointClouds[index] = boost::make_shared<PointCloud<PointXYZRGBNormal>>();

        *_pointClouds[index] = *cloud;
    }

    /*********/
    void RegisterImpl::setGuessCalibration(const vector<CalibrationParams>& calibration)
    {
        unique_lock<mutex> lock(_mutex);
        _guessedCalibrationParams = calibration;
        _calibrationParams.resize(_guessedCalibrationParams.size());
    }

} // end of namespace
