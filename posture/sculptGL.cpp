#include "config.h"
#include "sculptGL.h"
#if HAVE_GLFW
    #include "sculptGL-impl.h"
#endif

using namespace std;

namespace posture
{

    /*********/
    SculptGL::SculptGL()
    {
#if HAVE_GLFW
        _impl = make_shared<SculptGLImpl>();
#endif
    }

    /*********/
    SculptGL::~SculptGL()
    {
    }

    /*********/
    unsigned long long SculptGL::getMesh(std::vector<unsigned char>& mesh)
    {
        if (!_impl)
            return 0;

#if HAVE_GLFW
        return _impl->getMesh(mesh);
#endif
    }

    /*********/
    unsigned long long SculptGL::getMesh(pcl::PolygonMesh::Ptr mesh)
    {
        if (!_impl)
            return 0;

#if HAVE_GLFW
        return _impl->getMesh(mesh);
#endif
    }

    /*********/
    void SculptGL::setCalibration(const vector<CalibrationParams>& calibrations) const
    {
        if (!_impl)
            return;

#if HAVE_GLFW
        return _impl->setCalibration(calibrations);
#endif
    }

    /*********/
    void SculptGL::setCompressMesh(bool active)
    {
        if (!_impl)
            return;

#if HAVE_GLFW
        _impl->setCompressMesh(active);
#endif
    }

    /*********/
    void SculptGL::setInputMesh(vector<unsigned char> mesh)
    {
        if (!_impl)
            return;

#if HAVE_GLFW
        _impl->setInputMesh(mesh);
#endif
    }

    /*************/
    void SculptGL::setInputMesh(pcl::PolygonMesh::Ptr mesh)
    {
        if (!_impl)
            return;

#if HAVE_GLFW
        _impl->setInputMesh(mesh);
#endif
    }

    /*********/
    void SculptGL::setInputCloud(vector<char> cloud)
    {
        if (!_impl)
            return;

#if HAVE_GLFW
        _impl->setInputCloud(cloud);
#endif
    }

    /*************/
    void SculptGL::setInputCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
    {
        if (!_impl)
            return;

#if HAVE_GLFW
        _impl->setInputCloud(cloud);
#endif
    }

    /*************/
    void SculptGL::setPointSeparation(float separation)
    {
        if (!_impl)
            return;

#if HAVE_GLFW
        _impl->setPointSeparation(separation);
#endif
    }

    /*************/
    void SculptGL::setMaxNeighbors(int maxNeighbors)
    {
        if (!_impl)
            return;

#if HAVE_GLFW
        _impl->setMaxNeighbors(maxNeighbors);
#endif
    }

}
