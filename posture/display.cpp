#include "display.h"
#include "display-impl.h"

using namespace std;

namespace posture
{
    /*********/
    Display::Display()
    {
        _impl = make_shared<DisplayImpl>();
    }

    /*********/
    Display::Display(string name)
    {
        _impl = make_shared<DisplayImpl>(name);
    }

    /*********/
    Display::~Display()
    {
    }

    /*********/
    void Display::setInputCloud(const vector<char>& cloud, const bool compressed)
    {
        if (!_impl)
            return;

        _impl->setInputCloud(cloud, compressed);
    }

    /*************/
    void Display::setInputCloud(const pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
    {
        if (!_impl)
            return;

        _impl->setInputCloud(cloud);
    }

    /*********/
    void Display::setPolygonMesh(const vector<unsigned char>& mesh)
    {
        if (!_impl)
            return;

        _impl->setPolygonMesh(mesh);
    }

    /*********/
    void Display::setPolygonMesh(const pcl::PolygonMesh::Ptr mesh)
    {
        if (!_impl)
            return;

        _impl->setPolygonMesh(mesh);
    }

    /*********/
    void Display::setTextureMesh(const pcl::TextureMesh::Ptr mesh)
    {
        if (!_impl)
            return;

        _impl->setTextureMesh(mesh);
    }

    /*********/
    bool Display::wasStopped()
    {
        if (!_impl)
            return false;

        return _impl->wasStopped();
    }

    /*********/
    void Display::setWireframe(bool enable)
    {
        if (!_impl)
            return;

        return _impl->setWireframe(enable);
    }
} // end of namespace
