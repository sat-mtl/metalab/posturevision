#include "pointcloudmerger-impl.h"

#include <iostream>

using namespace std;
using namespace pcl;
using namespace Eigen;

namespace posture
{
    /*************/
    PointCloudMergerImpl::PointCloudMergerImpl() :
        _noiseRes(10)
    {
    }
    
    /*************/
    PointCloudMergerImpl::~PointCloudMergerImpl()
    {
    }

    /*************/
    void PointCloudMergerImpl::setCompression(bool compress)
    {
        _serializer.setCompression(compress);
    }

    /*************/
    void PointCloudMergerImpl::setDownsampling(bool active, float resolution)
    {
        if (resolution < 0.01f)
            return;

        std::lock_guard<std::mutex> lock(_mutex);

        _downsample = active;
        _voxelGrid.setLeafSize(resolution, resolution, resolution);
    }
    
    /*************/
    void PointCloudMergerImpl::setInputCloud(const unsigned int index, const vector<char>& cloud, const bool compressed)
    {
        std::lock_guard<std::mutex> lock(_mutex);
        if (index < _nbrSources)
            _pointClouds[index] = _serializer.deserialize(cloud, compressed, _timestamps[index]);
    }
    
    /*************/
    void PointCloudMergerImpl::setInputCloud(const unsigned int index, vector<char>&& cloud, const bool compressed)
    {
        std::lock_guard<std::mutex> lock(_mutex);
        if (index < _nbrSources)
            _pointClouds[index] = _serializer.deserialize(std::move(cloud), compressed, _timestamps[index]);
    }

    /*************/
    void PointCloudMergerImpl::setInputCloud(const unsigned int index, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
    {
        std::lock_guard<std::mutex> lock(_mutex);
        if (index < _nbrSources)
        {
            _pointClouds[index] = cloud;
        }
    }
    
    /*************/
    void PointCloudMergerImpl::setLoadedPCD(std::string pcdPath)
    {
        _pcdPath = pcdPath;
        pcl::io::loadPCDFile(_pcdPath, _pointCloud);
        pcl::copyPointCloud(_pointCloud, _finalCloud);
    }

    /*************/
    void PointCloudMergerImpl::setSaveCloud(bool active)
    {
        std::lock_guard<std::mutex> lock(_mutex);

        _saveSeparately = active;
    }

    /*************/
    void PointCloudMergerImpl::setCloudNbr (unsigned int cloudNbr)
    {
        _nbrSources=cloudNbr;
    }
    
    /*************/
    unsigned long long PointCloudMergerImpl::getCloud(std::vector<char>& cloud)
    {
        pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr new_cloud = boost::make_shared<pcl::PointCloud<pcl::PointXYZRGBNormal>>();
        getCloud(new_cloud);
        cloud=_serializer.serialize(new_cloud,  _mergedTimestamp);

        return _mergedTimestamp;
    }

    /*************/
    unsigned long long PointCloudMergerImpl::getCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
    {
        std::lock_guard<std::mutex> lock(_mutex);

        if (!_pcdPath.empty())
        {
            for (size_t i = 0; i < _finalCloud.points.size(); ++i)
            {
                _finalCloud.points[i].r = 255.0f;
                _finalCloud.points[i].g = 255.0f;
                _finalCloud.points[i].b = 255.0f;
                _finalCloud.points[i].a = 255.0f;
            }
        }
        // If source nbr = 0, we send noise
        else if (_nbrSources == 0)
        {
            // Fill in the cloud data
            _finalCloud.width = _noiseRes;
            _finalCloud.height = _noiseRes;
            _finalCloud.is_dense = false;
            _finalCloud.points.resize(_finalCloud.width * _finalCloud.height);

            for (size_t i = 0; i < _finalCloud.points.size(); ++i)
            {
                _finalCloud.points[i].x = 1024 * rand() / (RAND_MAX + 1.0f);
                _finalCloud.points[i].y = 1024 * rand() / (RAND_MAX + 1.0f);
                _finalCloud.points[i].z = 1024 * rand() / (RAND_MAX + 1.0f);
                _finalCloud.points[i].r = (float)rand() / ((float)RAND_MAX/(255.0f));
                _finalCloud.points[i].g = (float)rand() / ((float)RAND_MAX/(255.0f));
                _finalCloud.points[i].b = (float)rand() / ((float)RAND_MAX/(255.0f));
                _finalCloud.points[i].a = 255.0f;
            }
        }
        else
        {
           _clippedClouds.clear();
           _finalCloud.clear();

           char buffer[64];
           if (_saveSeparately)
           {
               for (unsigned int i = 0; i < _nbrSources; ++i)
               {
                   _clippedClouds.push_back(_filter.applyClipping<PointXYZRGBNormal>(_pointClouds[i], _calibrationParams[i]));
                   transformPointCloudWithNormals(*_clippedClouds[i], *_clippedClouds[i], _matTransforms[i]);
                   _finalCloud += *_clippedClouds[i];
                   if (_timestamps[i] > 0)
                   {
                       sprintf(buffer, "/tmp/clouds/cam%i_%llu.ply", i, _timestamps[i]);
                       std::cout << buffer << " has been saved." << std::endl;
                       std::string filename(buffer);
                       pcl::io::savePLYFileBinary<pcl::PointXYZRGBNormal>(filename, *(_clippedClouds[i].get()));
                   }
               }

               _finalCloud = *(_filter.applyShapeClipping<PointXYZRGBNormal>(boost::make_shared<PointCloud<PointXYZRGBNormal>>(_finalCloud), _calibrationParams[0]));
           }
           else
           {
               for (unsigned int i = 0; i < _nbrSources; ++i)
               {
                   _clippedClouds.push_back(_filter.applyClipping<PointXYZRGBNormal>(_pointClouds[i], _calibrationParams[i]));
                   transformPointCloudWithNormals(*_clippedClouds[i], *_clippedClouds[i], _matTransforms[i]);
                   _finalCloud += *_clippedClouds[i];
               }

               _finalCloud = *(_filter.applyShapeClipping<PointXYZRGBNormal>(boost::make_shared<PointCloud<PointXYZRGBNormal>>(_finalCloud), _calibrationParams[0]));
            }
        }

        // Resample the cloud
        if (_downsample)
        {
            PointCloud<PointXYZRGBNormal> tmp;
            _voxelGrid.setInputCloud(_finalCloud.makeShared());
            _voxelGrid.filter(tmp);
            _finalCloud.swap(tmp);
        }

        *cloud = _finalCloud;
        return _mergedTimestamp;
    }

    /*************/
    void PointCloudMergerImpl::setSaveSeparately (const bool saveSeparately)
    {
        _saveSeparately = saveSeparately;
    }

    /*************/
    void PointCloudMergerImpl::start()
    {
        std::lock_guard<std::mutex> lock(_mutex);

        // If _nbrSources == 0, we output noise
        if (_nbrSources == 0)
            return;
    
        _emptyCloud.clear();
        const PointCloud<PointXYZRGBNormal>::Ptr emptyCloudPtr = boost::make_shared<PointCloud<PointXYZRGBNormal>>(_emptyCloud);
        _calibrationParams.clear();
        for (unsigned int i = 0; i < _nbrSources; ++i)
        {
            _pointClouds.push_back(emptyCloudPtr);
            _timestamps.push_back(0);
        }
    
        initializeTransformation();
    }

    /*************/
    void PointCloudMergerImpl::stop()
    {
    }
    
    /*************/
    void PointCloudMergerImpl::initializeTransformation()
    {
        for (unsigned int i = 0; i < _nbrSources; ++i)
        {
            if (i < _matTransforms.size())
                _matTransforms[i] = _calibrationParams[i].getTransformation();
            else
                _matTransforms.push_back(_calibrationParams[i].getTransformation());
        }
    }

} // end of namespace
