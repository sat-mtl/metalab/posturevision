#include "zcamera.h"
#include "zcamera-impl.h"

#include <chrono>

using namespace std;
using namespace pcl;
//using pcl::io::OpenNI2Grabber;

namespace posture
{
    /*************/
    ZCamera::ZCamera()
    {
        _impl = make_shared<ZCameraImpl>();
    }
    
    /*************/
    ZCamera::~ZCamera()
    {
    }

    /*************/
    float ZCamera::getRGBFocal()
    {
        if (!_impl)
            return 0.f;

        return _impl->getRGBFocal();
    }

    /*************/
    float ZCamera::getDepthFocal()
    {
        if (_impl == nullptr)
            return 0.f;

        return _impl->getDepthFocal();
    }
    
    /*************/
    unsigned long long ZCamera::getCloud(vector<char>& cloud) const
    {
        if (!_impl)
            return 0;
    
        return _impl->getCloud(cloud);
    }

    /*************/
    unsigned long long ZCamera::getCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud) const
    {
        if (!_impl)
            return 0;

        return _impl->getCloud(cloud);
    }

    /*************/
    unsigned int ZCamera::getDeviceCount()
    {
        return ZCameraImpl::getDeviceCount();
    }
    
    /*************/
    unsigned long long ZCamera::getRgbImage(std::vector<unsigned char> &rgb, unsigned int &width, unsigned int &height) const
    {
        if (!_impl)
            return 0;
    
        return _impl->getRgbImage(rgb, width, height);
    }
    
    /*************/
    unsigned long long ZCamera::getDepthImage(std::vector<unsigned char> &depth, unsigned int &width, unsigned int &height) const
    {
        if (!_impl)
            return 0;
    
        return _impl->getDepthImage(depth, width, height);
    }
    
    /*************/
    unsigned long long ZCamera::getIRImage(std::vector<unsigned char> &ir, unsigned int &width, unsigned int &height) const
    {
        if (!_impl)
            return 0;
    
        return _impl->getIRImage(ir, width, height);
    }

    /*************/
    ZCamera::CaptureFormat ZCamera::getCaptureFormat() const
    {
        if (!_impl)
            return ZCamera::CaptureFormat::RGB;

        return _impl->getCaptureFormat();
    }

    /*************/
    void ZCamera::getBilateralFiltering(int& kernelSize, float& sigmaPos, float& sigmaValue) const
    {
        if (!_impl)
            return;

        _impl->getBilateralFiltering(kernelSize, sigmaPos, sigmaValue);
    }
    
    /*************/
    bool ZCamera::isReady() const
    {
        if (!_impl)
            return false;
        
        return _impl->isReady();
    }
    
    /*************/
    bool ZCamera::isUpdated() const
    {
        if (!_impl)
            return false;
        
        return _impl->isUpdated();
    }
    
    /*************/
    bool ZCamera::isImageUpdated() const
    {
        if (!_impl)
            return false;
        
        return _impl->isImageUpdated();
    }
    
    /*************/
    bool ZCamera::isIRImageUpdated() const
    {
        if (!_impl)
            return false;
        
        return _impl->isIRImageUpdated();
    }
    
    /*************/
    void ZCamera::setCalibration(const CalibrationParams& calibration) const
    {
        if (!_impl)
            return;
        
        return _impl->setCalibration(calibration);
    }

    /*************/
    void ZCamera::setClippingDistance(int distance) const
    {
        if (!_impl)
            return;
        
        _impl->setClippingDistance(distance);
    }

    /*************/
    void ZCamera::setCompression(bool compress) const
    {
        if (!_impl)
            return;
        
        _impl->setCompression(compress);
    }

    /*************/
    void ZCamera::setDepthFocal(float focal) const
    {
        if (!_impl)
            return;
        
        _impl->setDepthFocal(focal);
    }
    
    /*************/
    bool ZCamera::setDeviceIndex(unsigned int index) const
    {
        if (!_impl)
            return false;
        
        return _impl->setDeviceIndex(index);
    }
    
    /*************/
    bool ZCamera::setCaptureMode(CaptureMode mode) const
    {
        if (!_impl)
            return false;
        
        return _impl->setCaptureMode((int)mode);
    }
    
    /*************/
    bool ZCamera::setCaptureIR(bool ir) const
    {
        if (!_impl)
            return false;
        
        return _impl->setCaptureIR(ir);
    }

    /*************/
    bool ZCamera::setRandomNoise(bool rn) const
    {
        if (!_impl)
            return false;

        return _impl->setRandomNoise(rn);
    }

    /*************/
    bool ZCamera::setModelPath(std::string path) const
    {
        if (!_impl)
            return false;

        return _impl->setModelPath(path);
    }

    /*************/
    void ZCamera::setCallbackCloud(std::function<void(void*, std::vector<char>&&)> cb, void* user_data)
    {
        if (!_impl)
            return;
        
        _impl->setCallbackCloud(cb, user_data);
    }

    /*************/
    void ZCamera::setCallbackCloud(std::function<void(void*, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr)> cb, void* user_data)
    {
        if (!_impl)
            return;

        _impl->setCallbackCloud(cb, user_data);
    }

    /*************/
    void ZCamera::setCallbackMesh(std::function<void(void*, std::vector<unsigned char>&&)> cb, void* user_data)
    {
        if (!_impl)
            return;

        _impl->setCallbackMesh(cb, user_data);
    }

    /*************/
    void ZCamera::setCallbackMesh(std::function<void(void*, pcl::TextureMesh::Ptr)> cb, void* user_data)
    {
        if (!_impl)
            return;

        _impl->setCallbackMesh(cb, user_data);
    }

    /*************/
    void ZCamera::setCallbackMesh(std::function<void(void*, pcl::PolygonMesh::Ptr)> cb, void* user_data)
    {
        if (!_impl)
            return;

        _impl->setCallbackMesh(cb, user_data);
    }

    /*************/
    void ZCamera::setCallbackDepth(std::function<void(void*, std::vector<unsigned char>&, int, int)> cb, void* user_data)
    {
        if (!_impl)
            return;
        
        _impl->setCallbackDepth(cb, user_data);
    }

    /*************/
    void ZCamera::setCallbackRgb(std::function<void(void*, std::vector<unsigned char>&, int, int)> cb, void* user_data)
    {
        if (!_impl)
            return;
        
        _impl->setCallbackRgb(cb, user_data);
    }

    /*************/
    void ZCamera::setCallbackIR(std::function<void(void*, std::vector<unsigned char>&, int, int)> cb, void* user_data)
    {
        if (!_impl)
            return;
        
        _impl->setCallbackIR(cb, user_data);
    }

    /*************/
    void ZCamera::setCallbackNoise(std::function<void(void*, std::vector<char>&&)> cb, void* user_data)
    {
        if (!_impl)
            return;

        _impl->setCallbackNoise(cb, user_data);
    }

    /*************/
    void ZCamera::setCallbackNoise(std::function<void(void*, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr)> cb, void* user_data)
    {
        if (!_impl)
            return;

        _impl->setCallbackNoise(cb, user_data);
    }

    /*************/
    void ZCamera::setCompressMesh(bool active)
    {
        if (!_impl)
            return;

        _impl->setCompressMesh(active);
    }

    /*************/
    void ZCamera::setBilateralFiltering(int kernelSize, float sigmaPos, float sigmaValue, int iterations)
    {
        if (!_impl)
            return;

        _impl->setBilateralFiltering(kernelSize, sigmaPos, sigmaValue, iterations);
    }

    /*************/
    void ZCamera::setHoleFiltering(int kernelSize, int iterations)
    {
        if (!_impl)
            return;

        _impl->setHoleFiltering(kernelSize, iterations);
    }

    /*************/
    void ZCamera::setOutlierFilterParameters(bool active, int meanK, double stddevMul)
    {
        if (!_impl)
            return;

        _impl->setOutlierFilterParameters(active, meanK, stddevMul);
    }

    /*************/
    void ZCamera::setDownsampling(bool active, float resolution)
    {
        if (!_impl)
            return;

        _impl->setDownsampling(active, resolution);
    }

    /*************/
    void ZCamera::setBuildMesh(bool active)
    {
        if (!_impl)
            return;

        _impl->setBuildMesh(active);
    }

    /*************/
    void ZCamera::setBuildEdgeLength(int length)
    {
        if (!_impl)
            return;

        _impl->setBuildEdgeLength(length);
    }
    
    /*************/
    void ZCamera::start()
    {
        if (!_impl)
            return;
    
        _impl->start();
    }
    
    /*************/
    void ZCamera::stop()
    {
        if (!_impl)
            return;
    
        _impl->stop();
    }

    /*************/
    void ZCamera::getNoise(float xMax, float yMax, float zMax, int nbrPoints, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
    {
        ZCameraImpl::getNoise(xMax, yMax, zMax, nbrPoints, cloud);
    }

    /*************/
    void ZCamera::getNoise(float xMax, float yMax, float zMax, int nbrPoints, std::vector<char>& cloud)
    {
        ZCameraImpl::getNoise(xMax, yMax, zMax, nbrPoints, cloud);
    }

    /*************/
    void ZCamera::getEmptyCubeNoise(float xMax, float yMax, float zMax, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
    {
        ZCameraImpl::getEmptyCubeNoise(xMax, yMax, zMax, cloud);
    }

    /*************/
    void ZCamera::loadFromPLY(std::string path, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
    {
        ZCameraImpl::loadFromPLY(path, cloud);
    }

    /*************/
    void ZCamera::getRandomMesh(float xMax, float yMax, float zMax, int nbrPolygons, pcl::PolygonMesh::Ptr mesh)
    {
        ZCameraImpl::getRandomMesh(xMax, yMax, zMax, nbrPolygons, mesh);
    }

    /*************/
    void ZCamera::getRandomMesh(float xMax, float yMax, float zMax, int nbrPolygons, std::vector<unsigned char>& mesh)
    {
        ZCameraImpl::getRandomMesh(xMax, yMax, zMax, nbrPolygons, mesh);
    }

} // end of namespace
