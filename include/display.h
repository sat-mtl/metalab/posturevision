/*
 * Copyright (C) 2014 Emmanuel Durand, Bruno Roy
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @display-impl.h
 * The Display class implementation
 */

#ifndef __POSTURE_DISPLAY_H__
#define __POSTURE_DISPLAY_H__

#include <functional>
#include <memory>
#include <string>
#include <vector>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/PolygonMesh.h>
#include <pcl/TextureMesh.h>

namespace posture
{
    class DisplayImpl;
    
    class Display
    {
        public:
            /**
             * \brief Constructor
             * \param name name to the display
             */
            Display();
            Display(std::string name);

            /**
             * \brief Destructor
             */
            ~Display();

            /**
             * \brief Set one of the input clouds
             * \param cloud Point cloud to display
             * \param compressed true if the cloud is compressed
             */
            void setInputCloud(const std::vector<char>& cloud, const bool compressed);
            void setInputCloud(const pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);

            /**
             * \brief Show a polygon mesh
             * \param mesh The polygon mesh to display
             */
            void setPolygonMesh(const std::vector<unsigned char>& mesh);
            void setPolygonMesh(const pcl::PolygonMesh::Ptr mesh);

            /**
             * \brief Show a texture mesh
             * \param mesh The texture mesh to display
             */
            //void setPolygonMesh(const std::vector<unsigned char>& mesh);
            void setTextureMesh(const pcl::TextureMesh::Ptr mesh);

            /**
             * \brief Asks if the viewer has been closed
             */
            bool wasStopped();

            void setWireframe(bool enable);

        private:
            std::shared_ptr<DisplayImpl> _impl;
    };
} // end of namespace

#endif // DISPLAY_H
