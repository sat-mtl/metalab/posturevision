/*
 * Copyright (C) 2014 Emmanuel Durand, Bruno Roy
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @display-impl.h
 * The DisplayImpl class implementation
 */

#ifndef DISPLAY_IMPL_H
#define DISPLAY_IMPL_H

#include <memory>
#include <mutex>
#include <string>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/TextureMesh.h>

#include "config.h"
#include "constants.h"
#include "pointcloudserializer.h"
#include "meshserializer.h"

namespace posture
{
    class DisplayImpl
    {
        public:
            /**
             * \brief Constructor
             * \param name name to the display
             */
            DisplayImpl();
            DisplayImpl(std::string name);

            /**
             * \brief Destructor
             */
            ~DisplayImpl();

            /**
             * \brief Show a point cloud
             * \param cloud Point cloud to display
             * \param timestamp Timestamp of the cloud.
             */
            void setInputCloud(const std::vector<char>& cloud, const bool compressed);
            void setInputCloud(const pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);

            /**
             * \brief Show a polygon mesh
             * \param mesh Polygon mesh to display
             */
            void setPolygonMesh(const std::vector<unsigned char>& mesh);
            void setPolygonMesh(const pcl::PolygonMesh::Ptr mesh);

            /**
             * \brief Show a texture mesh
             * \param mesh Texture mesh to display
             */
            //void setPolygonMesh(const std::vector<unsigned char>& mesh);
            void setTextureMesh(const pcl::TextureMesh::Ptr mesh);

            /**
             * \brief Asks if the viewer has been closed
             */
            bool wasStopped();

            void setWireframe(bool enable);

        private:
            std::mutex _mutex;
            bool _isWireframe {false};

            std::string _windowName {"Posture point cloud viewer"};
            std::shared_ptr<pcl::visualization::CloudViewer> _viewer {nullptr};
            PointCloudSerializer<pcl::PointXYZRGBNormal> _pclSerializer;
            MeshSerializer _meshSerializer;

            pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr _cloud {nullptr};
            pcl::PolygonMesh::Ptr _mesh {nullptr};
            pcl::TextureMesh::Ptr _textureMesh {nullptr};

            void init();
            void normal_callback(pcl::visualization::PCLVisualizer& viz);
            void mesh_callback(pcl::visualization::PCLVisualizer& viz);
            void textureMesh_callback(pcl::visualization::PCLVisualizer& viz);
    };
} // end of namespace

#endif // DISPLAY_IMPL_H
