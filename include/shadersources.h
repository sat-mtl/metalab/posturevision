/*
 * Copyright (C) 2016 Emmanuel Durand
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @filterShaders.h
 * Contains the GLSL sources for all shaders used in the Filter class
 */

#ifndef __FILTERSHADERS_H__
#define __FILTERSHADERS_H__

namespace posture
{

struct ShaderSources
{
    const std::string BILATERAL_FILTER {R"(
        #version 430 core

        #extension GL_NV_gpu_shader5 : enable
        #extension GL_ARB_compute_shader : enable
        #extension GL_ARB_shader_storage_buffer_object : enable

        #define PI 3.14159265359

        layout(local_size_x = 32, local_size_y = 32) in;

        layout(std430, binding = 0) buffer depthMapBuffer
        {
            unsigned short depthMap[];
        };

        uniform vec2 _size;
        uniform int _kernelShift;
        uniform float _sigmaPos;
        uniform float _sigmaValue;

        float gauss1D(float x, float sigma)
        {
            return exp(-pow(x / sigma, 2.0) / 2.0);
        }

        float gauss2D(vec2 pos, vec2 sigma)
        {
            return gauss1D(pos.x, sigma.x) * gauss1D(pos.y, sigma.y);
        }

        void main(void)
        {
            vec2 pixCoords = gl_WorkGroupID.xy * vec2(32.0) + gl_LocalInvocationID.xy;
            if (pixCoords.x > _size.x || pixCoords.y > _size.y)
                return;

            ivec2 pos = ivec2(int(pixCoords.x), int(pixCoords.y));
            int index = pos.x + pos.y * int(_size.x);

            if (depthMap[index] == uint16_t(0))
                return;

            float filtered = 0.0;
            float samples = 0.0;
            for (int y = pos.y - _kernelShift; y <= pos.y + _kernelShift; ++y)
            {
                if (y < 0 || y >= int(_size.y))
                    continue;

                for (int x = pos.x - _kernelShift; x <= pos.x + _kernelShift; ++x)
                {
                    if (x < 0 || x >= int(_size.x))
                        continue;

                    int neighbourIndex = x + y * int(_size.x);
                    if (depthMap[neighbourIndex] == uint16_t(0))
                        continue;

                    float posCoeff = gauss2D(vec2(x - pos.x, y - pos.y), vec2(_sigmaPos));
                    float valueCoeff = gauss1D(abs(float(depthMap[index]) - float(depthMap[neighbourIndex])), _sigmaValue);
                    float coeff = posCoeff * valueCoeff;
                    filtered += depthMap[neighbourIndex] * coeff;
                    samples += coeff;
                }
            }

            depthMap[index] = uint16_t(filtered / samples);
        }
    )"};

    const std::string DILATE_FILTER {R"(
        #version 430 core

        #extension GL_NV_gpu_shader5 : enable
        #extension GL_ARB_compute_shader : enable
        #extension GL_ARB_shader_storage_buffer_object : enable

        #define PI 3.14159265359

        layout(local_size_x = 32, local_size_y = 32) in;

        layout(std430, binding = 0) buffer depthMapBuffer
        {
            unsigned short depthMap[];
        };

        uniform vec2 _size;
        uniform int _kernelSize;

        void main(void)
        {
            vec2 pixCoords = gl_WorkGroupID.xy * vec2(32.0) + gl_LocalInvocationID.xy;
            if (pixCoords.x > _size.x || pixCoords.y > _size.y)
                return;
            ivec2 pos = ivec2(int(pixCoords.x), int(pixCoords.y));
            
            unsigned short sum = uint16_t(0);
            unsigned short number = uint16_t(0);

            for (int y = pos.y - _kernelSize; y <= pos.y + _kernelSize; ++y)
            {
                if (y < 0 || y >= int(_size.y))
                    continue;

                for (int x = pos.x - _kernelSize; x <= pos.x + _kernelSize; ++x)
                {
                    if (x < 0 || x >= int(_size.x))
                        continue;

                    int index = x + y * int(_size.x);
                    if (depthMap[index] > uint16_t(0))
                    {
                        sum += depthMap[index];
                        number++;
                    }
                }
            }

            barrier();
            if (number > uint16_t(0))
            {
                int index = pos.x + pos.y * int(_size.x);
                depthMap[index] = sum / number;
            }
        }
    )"};

    const std::string ERODE_FILTER {R"(
        #version 430 core

        #extension GL_NV_gpu_shader5 : enable
        #extension GL_ARB_compute_shader : enable
        #extension GL_ARB_shader_storage_buffer_object : enable

        #define PI 3.14159265359

        layout(local_size_x = 32, local_size_y = 32) in;

        layout(std430, binding = 0) buffer depthMapBuffer
        {
            unsigned short depthMap[];
        };

        uniform vec2 _size;
        uniform int _kernelSize;

        void main(void)
        {
            vec2 pixCoords = gl_WorkGroupID.xy * vec2(32.0) + gl_LocalInvocationID.xy;
            if (pixCoords.x > _size.x || pixCoords.y > _size.y)
                return;
            ivec2 pos = ivec2(int(pixCoords.x), int(pixCoords.y));
            
            bool isFilled = true;

            for (int y = pos.y - _kernelSize; y <= pos.y + _kernelSize; ++y)
            {
                if (y < 0 || y >= int(_size.y))
                    continue;

                for (int x = pos.x - _kernelSize; x <= pos.x + _kernelSize; ++x)
                {
                    if (x < 0 || x >= int(_size.x))
                        continue;

                    int index = x + y * int(_size.x);
                    if (depthMap[index] == uint16_t(0))
                        isFilled = false;
                }
            }

            barrier();
            if (!isFilled)
            {
                int index = pos.x + pos.y * int(_size.x);
                depthMap[index] = uint16_t(0);
            }
        }
    )"};
} ShaderSources;

} // end of namespace

#endif
