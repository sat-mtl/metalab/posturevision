#ifndef __POSTURE_SOLIDIFYGPU_IMPL_H__
#define __POSTURE_SOLIDIFYGPU_IMPL_H__

#include <mutex>
#include <vector>
#include <vector>
#include <Eigen/Core>

#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/PolygonMesh.h>
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/gpu/surface/tsdf_volume.h>
#include <pcl/gpu/surface/marching_cubes.h>
#include <pcl/gpu/surface/volumetric_diffusion.h>
#include <pcl/gpu/containers/device_array.h>

#include <glm/glm.hpp>

#include "constants.h"
#include "meshserializer.h"
#include "calibrationreader.h"
#include "filter.h"
#include "glEngine.h"

namespace posture
{
    class SolidifyGPUImpl
    {
        public:
            /**
             * \brief Constructor
             */
            SolidifyGPUImpl();

            /**
             * \brief Destructor
             */
            ~SolidifyGPUImpl();

            /**
             * \brief Get the resulting mesh
             * \param mesh Reference to the object where the mesh will be written
             * \return Timestamp Reference for the grab (in ms, from epoch)
             */
            unsigned long long getMesh(std::vector<unsigned char>& mesh, bool threads = true);
            unsigned long long getMesh(pcl::PolygonMesh::Ptr& mesh);
            unsigned long long getMesh(std::vector<pcl::PolygonMesh::Ptr>& mesh);

            /**
             * \brief Set the grid resolution for the reconstruction
             * \param res Grid resolution
             */
            void setGridResolution(float res);

            /**
             * \brief Set the grid size for the reconstruction
             * \param size Grid size
             */
            void setGridSize(float size);

            /**
             * \brief Set the grid size of the X coordinates for the reconstruction
             * \param size Grid X size
             */
            void setGridSizeX(float size);

            /**
             * \brief Set the grid size of the y coordinates for the reconstruction
             * \param size Grid Y size
             */
            void setGridSizeY(float size);

            /**
             * \brief Set the grid size of the Z coordinates for the reconstruction
             * \param size Grid Z size
             */
            void setGridSizeZ(float size);

            /**
             * \brief Set the number of depth maps using for TSDF calculation
             * \param dpmNbr Number of depth maps
             */
            void setDepthMapNbr (unsigned int dpmNbr);

            /**
             * \brief Set an input depth map
             * \param index Which map to set
             * \param depth Depth image data
             * \param width Image width 
             * \param height Image height 
             */
            void setInputDepthMap(int index, const std::vector<unsigned char>& depth, int width, int height);
            void setInputDepthMap(int index, const std::vector<unsigned short>& depth, int width, int height);

            /**
             * \brief Activate the compression of the output mesh
             *        parameters are specified directly in getMesh(std::vector<unsigned char>&)             * \param active Compression state
             */
            void setCompressMesh(bool active)
            {
                _meshSerializer.setCompress(active);
                _compressMesh = active;
            }

            /**
             * \brief Activate the saving of created mesh
             * \param active Saving state
             */
            void setSaveMesh(bool active);

            /**
             * \brief Set the path where to find the calibration file
             * \param calibration Calibration parameters for the camera
             */
            void setCalibration(const std::vector<CalibrationParams>& calibrations)
            {
                std::unique_lock<std::mutex> lock(_calibrationMutex);
                _calibrationParams = calibrations;
            }

            /**
             * \brief Set the depth map bilateral filtering parameters
             * \param size Kernel size
             * \param sigmaPos Spatial sigma
             * \param sigmaValue Depth value sigma
             */
            void setDepthFiltering(int size, float sigmaPos, float sigmaValue)
            {
                _kernelSize = std::max(1, size);
                _sigmaPos = std::max(0.f, sigmaPos);
                _sigmaValue = std::max(0.f, sigmaValue);
            }

            /**
             * \brief Set the number of iterations for the hole filling algorithm
             * \param iter Number of iterations
             */
            void setHoleFillingIterations(int iter);

        private:
            std::mutex _mutex;
            mutable std::mutex _calibrationMutex;

            MeshSerializer _meshSerializer;
            bool _compressMesh {false};
            std::vector<CalibrationParams> _calibrationParams;

            std::shared_ptr<GLEngine> _glEngine {};
            std::shared_ptr<Shader> _shaderBilateralFilter {};
            int _kernelSize {3};
            float _sigmaPos {5.0};
            float _sigmaValue {1000.0};

            pcl::gpu::MarchingCubes _marchingCubes;
            pcl::gpu::VolumetricDiffusion _volumetricDiffusion;
            bool _updatedGridParameters {false};
            unsigned int _gridMaxResolution {16};
            float _gridMaxSize {1.0f};
            float _gridMaxSizeX {1.0f};
            float _gridMaxSizeY {1.0f};
            float _gridMaxSizeZ {1.0f};
            float _scaledResolution {0.05};
            Eigen::Vector3i _resolution;
            Eigen::Vector3f _size;

            Filter _filter;

            unsigned long long _timestamp;
            pcl::gpu::TsdfVolume::Ptr _grid;
            pcl::gpu::DeviceArray<pcl::PointXYZ> _triangleBuffer;
            pcl::PolygonMesh::Ptr _mesh {nullptr};

            std::vector<std::vector<unsigned short>> _depthMaps;
            std::vector<pcl::gpu::DeviceArray2D<unsigned short>> _depthMapsGPU;
            std::vector<int> _widths;
            std::vector<int> _heights;

            bool _saveMesh {false};

            int _nThreads {1};

            int getCPUNbr();

            pcl::gpu::DeviceArray<pcl::PointXYZ> runMarchingCubes();

            void deviceArrayToPolygonMesh(const pcl::gpu::DeviceArray<pcl::PointXYZ>& array, pcl::PolygonMesh::Ptr& mesh);
            std::vector<pcl::PolygonMesh::Ptr> deviceArrayToPolygonMesh(const pcl::gpu::DeviceArray<pcl::PointXYZ>& array, int nSubmeshes); 
            static pcl::PolygonMesh::Ptr vertexListToPolygonMesh(const std::vector<pcl::PointXYZ>& vertexList, int begin, int nbrVertices);


            void updateCalibration();
    };

} // end of namespace

#endif // __POSTURE_SOLIDIFYGPU_IMPL_H__
