#ifndef TRIGGER_GRABBER_H
#define	TRIGGER_GRABBER_H

#include <pcl/io/grabber.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include "pcl/pcl_macros.h"

#include <thread>
#include <memory>

namespace posture
{

    class PCL_EXPORTS TriggerGrabber : public pcl::Grabber
    {
        public:
            TriggerGrabber(float xMax, float yMax, float zMax, std::string modelPath = "");
            virtual ~TriggerGrabber ();

            virtual void start();
            virtual void stop();
            void update();
            virtual std::string getName() const;
            virtual bool isRunning() const;
            virtual float getFramesPerSecond() const;

            boost::shared_ptr<const pcl::PointCloud<pcl::PointXYZRGBA>> generateNoise() const;

        protected:
            virtual void signalsChanged();

        private:
            float _xMax, _yMax, _zMax;
            bool _running;
            std::string _modelPath;
            std::shared_ptr<std::thread> _thread;

            boost::signals2::signal<void (const boost::shared_ptr<const pcl::PointCloud<pcl::PointXYZRGBA>>&)>* _noiseSignal;
            boost::shared_ptr<const pcl::PointCloud<pcl::PointXYZRGBA>> loadFromPLY(std::string path) const;
    };

}

#endif // TRIGGER_GRABBER_H

