/*
 * Copyright (C) 2012 Bruno Roy, Emmanuel Durand
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @calibrationreader.h
 * The CalibrationReader class
 */

#ifndef CALIBRATIONREADER_H
#define CALIBRATIONREADER_H

#include <iostream>
#include <vector>
#include <memory>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <libxml/xmlreader.h>

#include "constants.h"

namespace posture
{
    /*************/
    struct ClippingShape
    {
        std::string name;
        bool include;
        std::vector<float> params;
    };
    
    /*************/
    struct CalibrationParams
    {
        // Parameters
        bool isValid {false};

        float rgb_focal {1.f};
        float rgbd_distance {0.2f};
        float scale {1.f};

        float offset_x {0.f}, offset_y {0.f}, offset_z {0.f};
        float rot_x {0.f}, rot_y {0.f}, rot_z {0.f};
        
        float min_clip_x {-1000.f}, min_clip_y {-1000.f}, min_clip_z {-1000.f};
        float max_clip_x {1000.f}, max_clip_y {1000.f}, max_clip_z {1000.f};
        std::vector<ClippingShape> shapes {};

        bool isOverriden {false};
        Eigen::Matrix4f overrideMatrix {};

        // Methods
        inline Eigen::Matrix4f getTransformation()
        {
            Eigen::Matrix4f transformationMatrix;

            if (isOverriden)
            {
                transformationMatrix = overrideMatrix;
            }
            else
            {
                Eigen::Matrix4f matrix = Eigen::Matrix4f::Zero();
                matrix.topLeftCorner(3, 3) = getRotation();

                auto translation = getTranslation();
                matrix.col(3) = Eigen::Vector4f(translation[0], translation[1], translation[2], 1.f);

                transformationMatrix = matrix;
            }

            return transformationMatrix;
        }

        inline Eigen::Matrix3f getRotation()
        {
            Eigen::Matrix3f rotationMatrix;

            if (isOverriden)
            {
                rotationMatrix = overrideMatrix.topLeftCorner(3, 3);
            }
            else
            {
                Eigen::Affine3f rx = Eigen::Affine3f(Eigen::AngleAxisf(rot_x * M_PI / 180.f, Eigen::Vector3f(1.f, 0.f, 0.f)));
                Eigen::Affine3f ry = Eigen::Affine3f(Eigen::AngleAxisf(rot_y * M_PI / 180.f, Eigen::Vector3f(0.f, 1.f, 0.f)));
                Eigen::Affine3f rz = Eigen::Affine3f(Eigen::AngleAxisf(rot_z * M_PI / 180.f, Eigen::Vector3f(0.f, 0.f, 1.f)));
                rotationMatrix = (rz * ry * rx).matrix().topLeftCorner(3, 3);
            }

            return rotationMatrix;
        }

        inline Eigen::Vector3f getTranslation()
        {
            Eigen::Vector3f translation;

            if (isOverriden)
            {
                translation = overrideMatrix.col(3).topRows(3);
            }
            else
            {
                translation = Eigen::Vector3f(offset_x, offset_y, offset_z);
            }

            return translation;
        }

        inline void overrideTransformation(const Eigen::Matrix4f& transformation)
        {
            isOverriden = true;
            overrideMatrix = transformation;
        }
    };
    
    /*************/
    struct Device
    {
        std::string manufacturer;
        std::string product;
    };
    
    /*************/
    //! CalibrationReader class, used to read the calibration file
    class CalibrationReader
    {
        public:
            /** 
             * \brief Constructor
             * \param calibrationPath Path to the calibration file
             * \param devicesPath Path to the known devices types
             */
            CalibrationReader(std::string calibrationPath, std::string devicesPath = DEVICES_PATH);
    
            /**
             * \brief Destructor
             */
            ~CalibrationReader();

            /**
             * Boolean operator
             */
            explicit operator bool() const {return _loadedSuccessfully;}
    
            /**
             * \brief Loads the specified calibration file
             * \param path Path to the calibration file
             */
            void loadCalibration(std::string path);
    
            /**
             * \brief Loads the specified devices file
             * \param path Path to the devices file
             */
            void loadDevices(std::string path);
    
            /**
             * \brief Gets the calibration for the given camera index
             * \param index Index of the camera
             * \return The calibration parameters for the chosen camera
             */
            CalibrationParams getCalibrationParams(const int index);
            std::vector<CalibrationParams> getCalibrationParams() {return _calibrationParams;}

            /**
             * \brief Get the number of calibrated devices
             * \return number of calibrated devices
             */
            unsigned int getSourceNumber();

            /**
             * \brief Reload the calibration file
             */
            void reload();
        
        private:
            std::string _calibrationFilePath {};

            std::vector<Device> _knownDevices;
            std::vector<CalibrationParams> _calibrationParams;
            bool _loadedSuccessfully {false};
    
            void readCalibrationAttributes(xmlNode * rootNode, int& calibrationIndex);
            void readDeviceAttributes(xmlNode * rootNode);
    };

} // end of namespace

#endif // CALIBRATIONREADER_H
