/*
 * Copyright (C) 2013 Emmanuel Durand, Bruno Roy
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @pointcloudmerger.h
 * The MeshMerger class
 */

#ifndef __POSTURE_MESHMERGER_H__
#define __POSTURE_MESHMERGER_H__

#include <functional>
#include <iostream>
#include <string>
#include <memory>
#include <mutex>
#include <vector>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/PolygonMesh.h>
#include <pcl/TextureMesh.h>

#include "calibrationreader.h"

namespace posture
{
    class MeshMergerImpl;

    /*************/
    //! MeshMerger class, merges meshes according to a calibration file
    class MeshMerger
    {
        public:
            /**
             * \brief Constructor
             * \param meshNbr Number of input meshes to be merged
             */
            MeshMerger(const unsigned int meshNbr = 1);
    
            /**
             * \brief Destructor
             */
            ~MeshMerger();

            /**
             * \brief Set whether to apply the calibration or not
             * \param apply true for apply calibration
             */
            void setApplyCalibration(bool apply) const;
    
            /**
             * \brief Set the path where to find the calibration file
             * \param calibration Calibration parameters for the camera
             */
            void setCalibration(const std::vector<CalibrationParams>& calibrations) const;
    
            /**
             * \brief Set one of the input meshes
             * \param index Index of the mesh to be set
             * \param mesh Pointer on the point mesh
             */
            void setInputMesh(const unsigned int index, const std::vector<unsigned char>& mesh) const;
            void setInputMesh(const unsigned int index, std::vector<unsigned char>&& mesh) const;
            void setInputMesh(const unsigned int index, const pcl::PolygonMesh::Ptr mesh) const;
            void setInputMesh(const unsigned int index, const pcl::TextureMesh::Ptr mesh) const;
            void setInputMesh(const unsigned int index, pcl::PolygonMesh::Ptr&& mesh) const;
    
            /**
             * \brief Get the merged mesh
             * \param mesh Pointer on the structure where to write the output point mesh
             * \return Timestamp for the grab (in ms, from epoch)
             */
            unsigned long long getMesh(std::vector<unsigned char>& mesh) const;
            unsigned long long getMesh(pcl::TextureMesh::Ptr mesh) const;
            unsigned long long getMesh(pcl::PolygonMesh::Ptr) const;
    
            /**
             * \brief Starts the merger
             */
            void start() const;
    
            /**
             * \brief Stops the merger
             */
            void stop() const;
    
        private:
            std::shared_ptr<MeshMergerImpl> _impl {nullptr};
    };

} // end of namespace

#endif // MESHMERGER_H
