/*
 * Copyright (C) 2013 Emmanuel Durand, Bruno Roy
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @zcamera-impl.h
 * The ZCamera class implementation
 */

#ifndef __ZCAMERA_IMPL_H__
#define __ZCAMERA_IMPL_H__

#include <fstream>
#include <functional>
#include <memory>
#include <mutex> 
#include <string>

#include "config.h"

#if WITH_REALSENSE
#include <librealsense/rs.hpp>
#elif WITH_OPENNI2
#include <pcl/io/openni2_grabber.h>
#include <pcl/io/openni2/openni2_device.h>
#elif WITH_OPENNI // This file is compiled if at least OpenNI is present
#include <pcl/io/openni_grabber.h>
#endif

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/common/transforms.h>
#include <pcl/TextureMesh.h>
#include <pcl/PolygonMesh.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/filters/fast_bilateral_omp.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/surface/organized_fast_mesh.h>

//#include <pcl/gpu/kinfu/tsdf_volume.h>
#include <pcl/gpu/surface/tsdf_volume.h>
#include <pcl/gpu/containers/device_array.h>

#if WITH_FREENECT2
#include <libfreenect2/libfreenect2.hpp>
#include <pcl/stereo/disparity_map_converter.h>
#endif

#include "constants.h"
#include "calibrationreader.h"
#include "filter.h"
#include "glEngine.h"
#include "pointcloudserializer.h"
#include "meshserializer.h"
#include "shader.h"
#include "triggergrabber.h"
#include "zcamera.h"

namespace posture
{
    /*************/
    //! ZCameraImpl class, used to grab frames and point clouds from OpenNI devices
    class ZCameraImpl
    {
#if WITH_REALSENSE
        typedef rs::device* PclGrabber;
#elif WITH_FREENECT2
        typedef libfreenect2::Freenect2Device* PclGrabber;
#elif WITH_OPENNI2
        typedef pcl::io::OpenNI2Grabber PclGrabber;
        typedef pcl::io::Image Image;
        typedef pcl::io::IRImage IRImage;
        typedef pcl::io::DepthImage DepthImage;
#elif WITH_OPENNI
        typedef pcl::OpenNIGrabber PclGrabber;
        typedef openni_wrapper::Image Image;
        typedef openni_wrapper::IRImage IRImage;
        typedef openni_wrapper::DepthImage DepthImage;
#endif

        public:
            /**
             * \brief Constructor
             * \param index Camera index to look for in the calibration file
             * \param mode Grab mode for the OpenNI grabber
             * \param grabVideo Flag to specify if video and depth map should be grabbed too
             * \param devicesPath Path to the list of known devices types
             */
#if WITH_REALSENSE
            ZCameraImpl(const int index = 0, const ZCamera::CaptureMode mode = ZCamera::CaptureMode::Default_Mode, const bool grabVideo = false);
#elif WITH_FREENECT2
            ZCameraImpl(const int index = 0, const ZCamera::CaptureMode mode = ZCamera::CaptureMode::Default_Mode, const bool grabVideo = false);
#elif WITH_OPENNI or WITH_OPENNI2
            ZCameraImpl(const int index = 0, const PclGrabber::Mode mode = PclGrabber::OpenNI_VGA_30Hz, const bool grabVideo = false);
#endif

            /**
             * \brief Destructor
             */
            ~ZCameraImpl();
    
            /**
             * \brief Method to check the state of the grabber
             * \return True if the camera is ready to grab
             */
            bool isReady() {return _ready;}

            /**
             * \brief Get the RGB camera focal length
             */
            float getRGBFocal();

            /**
             * \brief Get the depth camera focal length
             */
            float getDepthFocal();
    
            /**
             * \brief Method to get the last cloud grabbed
             * \param cloud Reference to the object where the cloud will be written
             * \return Timestamp for the grab (in ms, from epoch)
             */
            unsigned long long getCloud(std::vector<char>& cloud) const;
            unsigned long long getCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud) const;

            /**
             * \brief Get the number of supported devices connected to the computer
             * \return The number of devices
             */
            static unsigned int getDeviceCount();
    
            /**
             * \brief Method to get the last rgb image grabbed
             * \param rgb Reference to the object where the rgb image will be written
             * \param width Reference Width will be written here
             * \param height Reference Height will be written here
             * \return Timestamp for the grab (in ms, from epoch)
             */
            unsigned long long getRgbImage(std::vector<unsigned char> &rgb, unsigned int &width, unsigned int &height) const;
    
            /**
             * \brief Method to get the last depth image grabbed
             * \param depth Reference to the object where the depth image will be written
             * \param width Reference Width will be written here
             * \param height Reference Height will be written there
             * \return Timestamp Reference for the grab (in ms, from epoch)
             */
            unsigned long long getDepthImage(std::vector<unsigned char> &depth, unsigned int &width, unsigned int &height) const;
    
            /**
             * \brief Method to get the last depth image grabbed
             * \param ir Reference to the object where the ir image will be written
             * \param width Reference Width will be written here
             * \param height Reference Height will be written there
             * \return Timestamp for the grab (in ms, from epoch)
             */
            unsigned long long getIRImage(std::vector<unsigned char> &ir, unsigned int &width, unsigned int &height) const;

            /**
             * \brief Get the color capture format
             * \return The capture format
             */
            ZCamera::CaptureFormat getCaptureFormat() const;

            /**
             * \brief Get the parameters for the bilateral filtering on the depth map. Currently only applied to RS cameras
             * \param kernelSize Size of the convolution kernel
             * \param sigmaPos Spatial sigma
             * \param sigmaValue Depth sigma
             */
            void getBilateralFiltering(int& kernelSize, float& sigmaPos, float& sigmaValue)
            {
                kernelSize = _bilateralFilterKernelSize;
                sigmaPos = _bilateralFilterSigmaPos;
                sigmaValue = _bilateralFilterSigmaValue;
            }
    
            /**
             * \brief Method to check if a new cloud is available
             * \return True if a new cloud is available
             */
            bool isUpdated() const {return _newCloud;}
    
            /**
             * \brief Method to check if new rgb and depth images are available
             * \return True if new images are available
             */
            bool isImageUpdated() const {return _newImage;}
    
            /**
             * \brief Method to check if new ir image are available
             * \return True if a new IR image is available
             */
            bool isIRImageUpdated() const {return _newIRImage;}

            /**
             * \brief Set the path where to find the calibration file
             * \param calibration Calibration parameters for the camera
             */
            void setCalibration(const CalibrationParams& calibration)
            {
                std::unique_lock<std::mutex> lock(_calibrationMutex);
                _calibrationParams = calibration;
            }

            /**
             * \brief Set the capture mode for the device
             * \param mode
             * \return true at the end
             */
#if WITH_REALSENSE
            bool setCaptureMode(ZCamera::CaptureMode mode);
#elif WITH_FREENECT2
            bool setCaptureMode(ZCamera::CaptureMode mode);
#elif WITH_OPENNI or WITH_OPENNI2
            bool setCaptureMode(PclGrabber::Mode mode);
#endif
            bool setCaptureMode(int mode);

            /**
             * \brief Set whether to grab video + cloud or IR only
             * \param ir true if cloud or IR only
             * \return true at the end
             */
            bool setCaptureIR(bool ir);

            bool setRandomNoise(bool rn);

            bool setModelPath(std::string path);

            /**
             * \brief Set whether to compress the output cloud or not
             * \param compress true if the cloud is to compress
             */
            void setCompression(bool compress);

            /**
             * \brief Override the default depth focal length
             * \param focal focal of the camera
             */
            void setDepthFocal(float focal);

            /**
             * \brief Set the index of the device to connect to
             * \param index index of the camera
             * \return true if index is lower than number of cameras
             */
            bool setDeviceIndex(unsigned int index);

            /**
             * \brief Activate the creation of a mesh from the organized cloud
             * \param active Build Mesh is active if true
             */
            void setBuildMesh(bool active);

            /**
             * \brief Set the edge length (in pixels) of the build mesh
             * \param length edge length (in pixels) of the build mesh
             */
            void setBuildEdgeLength(int length);

            /**
             * \brief Set various callbacks
             * \param cb callback function
             * \param user_data pointer to a structure with all necessary data
             */
            void setCallbackCloud(std::function<void(void*, std::vector<char>&&)>, void* user_data);
            void setCallbackCloud(std::function<void(void*, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr)> cb, void* user_data);
            void setCallbackMesh(std::function<void(void*, std::vector<unsigned char>&&)>, void* user_data);
            void setCallbackMesh(std::function<void(void*, pcl::TextureMesh::Ptr)>, void* user_data);
            void setCallbackMesh(std::function<void(void*, pcl::PolygonMesh::Ptr)>, void* user_data);
            void setCallbackDepth(std::function<void(void*, std::vector<unsigned char>&, int, int)>, void* user_data);
            void setCallbackRgb(std::function<void(void*, std::vector<unsigned char>&, int, int)>, void* user_data);
            void setCallbackIR(std::function<void(void*, std::vector<unsigned char>&, int, int)>, void* user_data);
            void setCallbackNoise(std::function<void(void*, std::vector<char>&&)>, void* user_data);
            void setCallbackNoise(std::function<void(void*, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr)> cb, void* user_data);

            /**
             * \brief Set the clipping distance for mesh and cloud creation
             * \param distance Clipping distance in mm
             */
            void setClippingDistance(int distance) {_clippingDistance = distance;}

            /**
             * \brief Set output mesh compression
             * \param active Compress is active if true
             */
            void setCompressMesh(bool active) {_meshSerializer.setCompress(active);}

            /**
             * \brief Set the downsampling
             * \param active Downsampling is active if true
             * \param resolution Target resolution
             */
            void setDownsampling(bool active, float resolution);

            /**
             * \brief Set the parameters for the bilateral filtering on the depth map. Currently only applied to RS cameras
             * \param kernelSize Size of the convolution kernel
             * \param sigmaPos Spatial sigma
             * \param iterations Iteration count
             */
            void setBilateralFiltering(int kernelSize, float sigmaPos, float sigmaValue, int iterations)
            {
                _bilateralFilterKernelSize = std::max(0, kernelSize);
                _bilateralFilterSigmaPos = std::max(0.f, sigmaPos);
                _bilateralFilterSigmaValue = std::max(0.f, sigmaValue);
                _bilateralFilterIterations = std::max(0, iterations);
            }

            /**
             * \brief Set the hole filling parameters
             * \param kernelSize Size of the kernel used
             * \param iterations Number of pass for the filter
             */
            void setHoleFiltering(int kernelSize, int iterations)
            {
                _holeFillingKernelSize = kernelSize;
                _holeFillingIterations = iterations;
            }

            /**
             * \brief Set the outlier filter parameters (see http://docs.pointclouds.org/trunk/classpcl_1_1_statistical_outlier_removal.html#details)
             * \param active Filter is active if true
             * \param meanK Number of neighbours to consider
             * \param stddevMul Multiplicator to apply to the std deviation
             */
            void setOutlierFilterParameters(bool active, int meanK, double stddevMul);
    
            /**
             * \brief Starts the grabber
             */
            void start();
    
            /**
             * \brief Stops the grabber
             */
            void stop();

            /**
             * \brief get a cube of noise
             * \param xMax maximum value for x coordinate of each point
             * \param yMax maximum value for y coordinate of each point
             * \param zMax maximum value for z coordinate of each point
             * \param nbrPoints number of points on the mesh
             * \param cloud pointer on the object where to write the cloud
             */
            static void getNoise(float xMax, float yMax, float zMax, int nbrPoints, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);
            static void getNoise(float xMax, float yMax, float zMax, int nbrPoints, std::vector<char>& cloud);
            static void getEmptyCubeNoise(float xMax, float yMax, float zMax, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);
            static void loadFromPLY(std::string path, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);

            /**
             * \brief get a random mesh
             * \param xMax maximum value for x coordinate of each point
             * \param yMax maximum value for y coordinate of each point
             * \param zMax maximum value for z coordinate of each point
             * \param nbrPoolygons number of polygons on the mesh
             * \param mesh pointer on the object where to write the cloud
             */
            static void getRandomMesh(float xMax, float yMax, float zMax, int nbrPolygons, pcl::PolygonMesh::Ptr mesh);
            static void getRandomMesh(float xMax, float yMax, float zMax, int nbrPolygons, std::vector<unsigned char>& mesh);

        private:
            bool _ready {false};
            mutable bool _newCloud {false};
            mutable bool _newImage {false}, _newIRImage {false};
            unsigned long long _timestamp;

            CalibrationParams _calibrationParams;
            int _deviceIndex {0};
#if WITH_REALSENSE
            ZCamera::CaptureMode _grabMode;
#elif WITH_FREENECT2
            ZCamera::CaptureMode _grabMode;
#elif WITH_OPENNI or WITH_OPENNI2
            PclGrabber::Mode _grabMode;
#endif
            bool _grabIR {false};
            bool _buildFastMesh {false};
            bool _randomNoise {false};
            std::string _modelPath;

            Filter _filter;

#if WITH_REALSENSE
            static std::unique_ptr<rs::context> _rsContext;
            rs::device* _grabber {nullptr};
#elif WITH_FREENECT2
            static std::unique_ptr<libfreenect2::Freenect2> _freenectContext;
            libfreenect2::Freenect2Device* _grabber {nullptr};
#elif WITH_OPENNI2 or WITH_OPENNI
            std::shared_ptr<PclGrabber> _grabber;
            boost::shared_ptr<std::vector<std::string>> _deviceURIs;
#endif
            std::shared_ptr<TriggerGrabber> _triggerGrabber;

            pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr _pointCloud;
            pcl::TextureMesh::Ptr _mesh;
            pcl::PolygonMesh::Ptr _polyMesh;
            std::vector<uint8_t> _rgbImage, _depthImage, _irImage;
    
            unsigned int _rgbWidth {1}, _rgbHeight {1};
            unsigned int _depthWidth {1}, _depthHeight {1};
            unsigned int _irWidth {1}, _irHeight {1};
    
            mutable std::mutex _depthMutex;
            mutable std::mutex _imageMutex;
            mutable std::mutex _depthImageMutex;
            mutable std::mutex _irImageMutex;
            mutable std::mutex _calibrationMutex;
            //std::thread _noiseThread;

            // Normal estimation
            pcl::FastBilateralFilterOMP<pcl::PointXYZRGB>::Ptr _depthFilter {nullptr};
            pcl::IntegralImageNormalEstimation<pcl::PointXYZRGB, pcl::Normal>::Ptr _normalEstimator {nullptr};

            // Depth map filtering
            std::shared_ptr<GLEngine> _glEngine {nullptr};
            std::shared_ptr<Shader> _shaderBilateralFilter {nullptr};
            std::shared_ptr<Shader> _shaderDilateFilter {nullptr};
            std::shared_ptr<Shader> _shaderErodeFilter {nullptr};
            int _bilateralFilterKernelSize {5};
            float _bilateralFilterSigmaPos {5.f};
            float _bilateralFilterSigmaValue {1000.f};
            int _bilateralFilterIterations {1};
            int _holeFillingKernelSize {1};
            int _holeFillingIterations {1};
            uint16_t _clippingDistance {2500};

            // Downsampling
            bool _downsample {false};
            pcl::VoxelGrid<pcl::PointXYZRGBNormal> _voxelGrid;
    
            // Outlier filtering
            bool _filterOutliers {false};
            int _filterMeanK {8};
            double _filterStddevMulThresh {1.0};
            pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB>::Ptr _outlierRemoval {nullptr};

            // Mesh reconstruction
            pcl::OrganizedFastMesh<pcl::PointXYZRGB>::Ptr _fastMesh {nullptr};

            // Serializers
            PointCloudSerializer<pcl::PointXYZRGBNormal> _serializer;
            MeshSerializer _meshSerializer;

            std::function<void(void*, std::vector<char>&&)> _cbCloud {nullptr};
            std::function<void(void*, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr)> _cbCloudUnserialized {nullptr};
            std::function<void(void*, std::vector<unsigned char>&&)> _cbMesh {nullptr};
            std::function<void(void*, pcl::TextureMesh::Ptr)> _cbMeshTexture {nullptr};
            std::function<void(void*, pcl::PolygonMesh::Ptr)> _cbMeshUnserialized {nullptr};
            std::function<void(void*, std::vector<unsigned char>&, int, int)> _cbDepth {nullptr};
            std::function<void(void*, std::vector<unsigned char>&, int, int)> _cbRGB {nullptr};
            std::function<void(void*, std::vector<unsigned char>&, int, int)> _cbIr {nullptr};
            std::function<void(void*, std::vector<char>&&)> _cbNoise {nullptr};
            std::function<void(void*, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr)> _cbNoiseUnserialized {nullptr};

            void* _cbCloudUserData {nullptr};
            void* _cbMeshUserData {nullptr};
            void* _cbDepthUserData {nullptr};
            void* _cbRGBUserData {nullptr};
            void* _cbIrUserData {nullptr};
            void* _cbNoiseUserData {nullptr};

#if WITH_REALSENSE
            // Intel Realsense update loop
            std::thread _rsUpdateThread;
            bool _rsStopLoop {false};
            double _depthClamp {4000.0};

            void rsUpdateLoop();

            void setRsOption(rs::option option, double value);
#endif

#if WITH_FREENECT2
            std::thread _freenect2UpdateThread;
            void freenectUpdateLoop();
            bool _freenect2StopLoop {false};
            pcl::DisparityMapConverter<pcl::PointXYZRGBNormal> _disparityToCloud;
#endif
    
#if not WITH_REALSENSE and (WITH_OPENNI or WITH_OPENNI2)
            void onCameraCloud(const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr cloud);
            void onCameraImage(const boost::shared_ptr<Image>& rgb);
            void onCameraImageDepth(const boost::shared_ptr<Image>& rgb, const boost::shared_ptr<DepthImage>& depth, float constant);
            void onCameraIRImage(const boost::shared_ptr<IRImage>& ir);
#endif
            void onCameraNoise(const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr cloud);
    };

} // end of namespace

#endif // ZCAMERA_H
