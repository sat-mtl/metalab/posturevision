/*
 * Copyright (C) 2013 Emmanuel Durand, Bruno Roy
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @zcamera.h
 * The ZCamera class
 */

#ifndef __POSTURE_ZCAMERA_H__
#define __POSTURE_ZCAMERA_H__

#include <functional>
#include <memory>
#include <string>
#include <vector>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/TextureMesh.h>
#include <pcl/PolygonMesh.h>

//#include <pcl/gpu/kinfu/tsdf_volume.h>
#include <pcl/gpu/surface/tsdf_volume.h>
#include <pcl/gpu/containers/device_array.h>

#include "calibrationreader.h"

namespace posture
{
    class ZCameraImpl;
    
    /*************/
    //! ZCamera class, used to grab frames and point clouds from OpenNI devices
    class ZCamera
    {
        public:
            enum class CaptureMode : int32_t
            {
                Default_Mode = 0,
                SXGA_15Hz,
                VGA_30Hz,
                VGA_25Hz,
                QVGA_25Hz,
                QVGA_30Hz,
                QVGA_60Hz,
                QQVGA_25Hz,
                QQVGA_30Hz,
                QQVGA_60Hz
            };

            enum class CaptureFormat : int32_t
            {
                RGB = 0,
                BGR
            };

            /**
             * \brief Constructor
             */
            ZCamera();

            /**
             * \brief Destructor
             */
            ~ZCamera();
    
            /**
             * \brief Method to check the state of the grabber
             * \return True if the camera is ready to grab
             */
            bool isReady() const;

            /**
             * \brief Get the RGB camera focal length
             */
            float getRGBFocal();

            /**
             * \brief Get the depth camera focal length
             */
            float getDepthFocal();
    
            /**
             * \brief Method to get the last cloud grabbed
             * \param cloud Reference to the object where the cloud will be written
             * \return Timestamp for the grab (in ms, from epoch)
             */
            unsigned long long getCloud(std::vector<char>& cloud) const;
            unsigned long long getCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud) const;

            /**
             * \brief Get the number of supported devices connected to the computer
             * \return The number of devices
             */
            static unsigned int getDeviceCount();
    
            /**
             * \brief Method to get the last rgb image grabbed
             * \param rgb Reference to the object where the rgb image will be written
             * \param width Reference Width will be written here
             * \param height Reference Height will be written here
             * \return Timestamp for the grab (in ms, from epoch)
             */
            unsigned long long getRgbImage(std::vector<unsigned char> &rgb, unsigned int &width, unsigned int &height) const;
    
            /**
             * \brief Method to get the last depth image grabbed
             * \param depth Reference to the object where the depth image will be written
             * \param width Reference Width will be written here
             * \param height Reference Height will be written there
             * \return Timestamp Reference for the grab (in ms, from epoch)
             */
            unsigned long long getDepthImage(std::vector<unsigned char> &depth, unsigned int &width, unsigned int &height) const;
    
            /**
             * \brief Method to get the last depth image grabbed
             * \param ir Reference to the object where the ir image will be written
             * \param width Reference Width will be written here
             * \param height Reference Height will be written there
             * \return Timestamp for the grab (in ms, from epoch)
             */
            unsigned long long getIRImage(std::vector<unsigned char> &ir, unsigned int &width, unsigned int &height) const;

            /**
             * \brief Get the color capture format
             * \return The capture format
             */
            CaptureFormat getCaptureFormat() const;

            /**
             * \brief Get the parameters for the bilateral filtering on the depth map. Currently only applied to RS cameras
             * \param kernelSize Size of the convolution kernel
             * \param sigmaPos Spatial sigma
             * \param sigmaValue Depth sigma
             */
            void getBilateralFiltering(int& kernelSize, float& sigmaPos, float& sigmaValue) const;
    
            /**
             * \brief Method to check if a new cloud is available
             * \return True if a new cloud is available
             */
            bool isUpdated() const;
    
            /**
             * \brief Method to check if new rgb and depth images are available
             * \return True if new images are available
             */
            bool isImageUpdated() const;
    
            /**
             * \brief Method to check if new IR image are available
             * \return True if a new IR image is available
             */
            bool isIRImageUpdated() const;

            /**
             * \brief Set the path where to find the calibration file
             * \param calibration Calibration parameters for the camera
             */
            void setCalibration(const CalibrationParams& calibration) const;
            
            /**
             * \brief Set whether to compress the output cloud or not
             * \param compress true if the cloud is compressed
             */
            void setCompression(bool compress) const;

            /**
             * \brief Override the default depth focal length
             * \param focal focal of the camera
             */
            void setDepthFocal(float focal) const;

            /**
             * \brief Set the index of the device to connect to
             * \param index index of the camera
             * \return true if index is lower than number of cameras
             */
            bool setDeviceIndex(unsigned int index) const;

            /**
             * \brief Set the capture mode for the device
             * \param mode capture mode for the device
             * \return true at the end
             */
            bool setCaptureMode(CaptureMode mode) const;

            /**
             * \brief Set whether to grab video + cloud or IR only
             * \param ir true if cloud or IR only
             * \return true at the end
             */
            bool setCaptureIR(bool ir) const;

            bool setRandomNoise(bool rn) const;

            bool setModelPath(std::string path) const;

            /**
             * \brief Set various callbacks
             * \param cb callback function
             * \param user_data pointer to a structure with all necessary data
             */
            void setCallbackCloud(std::function<void(void*, std::vector<char>&&)> cb, void* user_data);
            void setCallbackCloud(std::function<void(void*, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr)> cb, void* user_data);
            void setCallbackMesh(std::function<void(void*, std::vector<unsigned char>&&)> cb, void* user_data);
            void setCallbackMesh(std::function<void(void*, pcl::TextureMesh::Ptr)> cb, void* user_data);
            void setCallbackMesh(std::function<void(void*, pcl::PolygonMesh::Ptr)> cb, void* user_data);
            void setCallbackDepth(std::function<void(void*, std::vector<unsigned char>&, int, int)> cb, void* user_data);
            void setCallbackRgb(std::function<void(void*, std::vector<unsigned char>&, int, int)> cb, void* user_data);
            void setCallbackIR(std::function<void(void*, std::vector<unsigned char>&, int, int)> cb, void* user_data);
            void setCallbackNoise(std::function<void(void*, std::vector<char>&&)> cb, void* user_data);
            void setCallbackNoise(std::function<void(void*, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr)> cb, void* user_data);

            /**
             * \brief Set the clipping distance for mesh and cloud creation
             * \param distance Clipping distance in mm
             */
            void setClippingDistance(int distance) const;

            /**
             * \brief Set output mesh compression
             * \param active Compress is active if true
             */
            void setCompressMesh(bool active);

            /**
             * \brief Set the downsampling
             * \param active Downsampling is active if true
             * \param resolution Target resolution
             */
            void setDownsampling(bool active, float resolution);

            /**
             * \brief Activate the creation of a mesh from the organized cloud
             * \param active Build Mesh is active if true
             */
            void setBuildMesh(bool active);

            /**
             * \brief Set the edge length (in pixels) of the build mesh
             * \param length edge length (in pixels) of the build mesh
             */
            void setBuildEdgeLength(int length);

            /**
             * \brief Set the parameters for the bilateral filtering on the depth map. Currently only applied to RS cameras
             * \param kernelSize Size of the convolution kernel
             * \param sigmaPos Spatial sigma
             * \param sigmaValue Depth sigma
             * \param iterations Iteration count
             */
            void setBilateralFiltering(int kernelSize, float sigmaPos, float sigmaValue, int iterations);

            /**
             * \brief Set the hole filling parameters
             * \param kernelSize Size of the kernel used
             * \param iterations Number of pass for the filter
             */
            void setHoleFiltering(int kernelSize, int iterations);

            /**
             * \brief Set the outlier filter parameters (see http://docs.pointclouds.org/trunk/classpcl_1_1_statistical_outlier_removal.html#details)
             * \param active Filter is active if true
             * \param meanK Number of neighbours to consider
             * \param stddevMul Multiplicator to apply to the std deviation
             */
            void setOutlierFilterParameters(bool active, int meanK, double stddevMul);
    
            /**
             * \brief Starts the grabber
             */
            void start();
    
            /**
             * \brief Stops the grabber
             */
            void stop();

            /**
             * \brief get a cube of noise
             * \param xMax maximum value for x coordinate of each point
             * \param yMax maximum value for y coordinate of each point
             * \param zMax maximum value for z coordinate of each point
             * \param nbrPoints number of points on the mesh
             * \param cloud pointer on the object where to write the cloud
             */
            static void getNoise(float xMax, float yMax, float zMax, int nbrPoints, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);
            static void getNoise(float xMax, float yMax, float zMax, int nbrPoints, std::vector<char>& cloud);
            static void getEmptyCubeNoise(float xMax, float yMax, float zMax, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);
            static void loadFromPLY(std::string path, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);

            /**
             * \brief get a noised mesh
             * \param xMax maximum value for x coordinate of each point
             * \param yMax maximum value for y coordinate of each point
             * \param zMax maximum value for z coordinate of each point
             * \param nbrPoolygons number of polygons on the mesh
             * \param mesh pointer on the object where to write the cloud
             */
            static void getRandomMesh(float xMax, float yMax, float zMax, int nbrPolygons, pcl::PolygonMesh::Ptr mesh);
            static void getRandomMesh(float xMax, float yMax, float zMax, int nbrPolygons, std::vector<unsigned char>& mesh);

        private:
            std::shared_ptr<ZCameraImpl> _impl {nullptr};
    };

} // end of namespace

#endif // ZCAMERA_H
