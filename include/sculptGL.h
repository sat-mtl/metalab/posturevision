/*
 * Copyright (C) 2015 Bruno Roy
 *
 * This file is part of posturevision.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @sculptGL.h
 * The SculptGL class, which tessellate and displace meshes according to input point clouds
 */

#ifndef __POSTURE_SCULPTGL
#define __POSTURE_SCULPTGL

#include <vector>
#include <memory>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/PolygonMesh.h>

#include "calibrationreader.h"

namespace posture
{
    class SculptGLImpl;

    /*************/
    //! SculptGL class, which tessellate and displace meshes according to input point clouds
    class SculptGL
    {
    public:
        /**
         * \brief Constructor
         */
        SculptGL();

        /**
         * \brief Destructor
         */
        ~SculptGL();

        /**
         * \brief Get the serialized mesh
         * \return serialized mesh
         */
        unsigned long long getMesh(std::vector<unsigned char>& mesh);
        unsigned long long getMesh(pcl::PolygonMesh::Ptr mesh);

        /**
         * \brief Set the path where to find the calibration file
         * \param calibration Calibration parameters for the camera
         */
        void setCalibration(const std::vector<CalibrationParams>& calibrations) const;

        /**
         * \brief Set output mesh compression
         * \param active true if the mesh is compressed
         */
        void setCompressMesh(bool active);

        /**
         * \brief Set the input mesh
         * \param mesh Serialized input mesh
         */
        void setInputMesh(std::vector<unsigned char> mesh);
        void setInputMesh(pcl::PolygonMesh::Ptr mesh);

        /**
         * \brief Set the input cloud
         * \param cloud Serialized input cloud
         */
        void setInputCloud(const std::vector<char> cloud);
        void setInputCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);

        /**
         * @brief Set the point separation of the cloud
         * @param separation Distance in float (usally between 0 and 1)
         */
        void setPointSeparation(float separation);

        /**
         * @brief Set the maximum number of neighbors nearby a polygon
         * @param maxNeighbors Maximum number of neighbors in search radius
         */
        void setMaxNeighbors(int maxNeighbors);

    private:
        std::shared_ptr<SculptGLImpl> _impl {nullptr};
    };

}

#endif // __POSTURE_SCULPTGL
