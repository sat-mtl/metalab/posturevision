/*
 * Copyright (C) 2015 Emmanuel Durand
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @glEngine.h
 * The GLEngine class, which processes vertex buffers according to a Shader object
 */

#ifndef __GLENGINE__
#define __GLENGINE__

#define GL_GLEXT_PROTOTYPES
#define GLX_GLXEXT_PROTOTYPES
#include <GLFW/glfw3.h>

#include <cmath>
#include <iostream>
#include <memory>
#include <vector>

#include <glm/glm.hpp>

#include "shader.h"

namespace posture
{
    enum LogPriority
    {
        DEBUGGING = 0,
        MESSAGE,
        WARNING,
        ERROR,
        NONE
    };

    /*************/
    //! GLEngine class
    class GLEngine
    {
        public:
            typedef std::shared_ptr<GLEngine> ptr;

            GLEngine();
            ~GLEngine();

            explicit operator bool() const
            {
                return (_glWindow != nullptr);
            }

            template<typename T> std::vector<T> getFeedbackBuffer(unsigned int index);
            template<typename T> std::vector<T> getColorBuffer(unsigned int index);

            void lockContext();
            void unlockContext();

            void run();

            bool setShader(const std::shared_ptr<Shader>& shader);
            void setColorBuffers(std::vector<GLint> formats);
            void setBufferSize(int width, int height);

            void resetBuffers();
            void setBufferNumber(unsigned int number);
            bool setBuffer(const void* data, size_t size, size_t typeSize, unsigned int index, int elemSize, GLenum elemType, GLenum type = GL_ARRAY_BUFFER);
            template<typename T> bool setBuffer(const std::vector<T>& data, unsigned int index, int elemSize, GLenum type = GL_ARRAY_BUFFER);

            void setTessLevel(uint level)
            {
                _tessLevel = level;
            }

        private:
            GLFWwindow* _glWindow {nullptr};
            bool _isContextLocked {false};
            bool _isManuallyLocked {false};
            std::vector<int> _glVersion {0, 0};
            int _glWidth {1024};
            int _glHeight {1024};
            GLuint _fbo {0};
            GLuint _vertexArray {0};
            GLuint _glDepthTex {0};
            std::vector<GLint> _glColorFormats {GL_RGBA};
            std::vector<GLuint> _glColorTex {1};

            std::shared_ptr<Shader> _shader;

            struct VertexBufferParams
            {
                int elemNbr;
                int elemSize;
                GLenum elemType;
                GLenum type;
            };
            std::vector<GLuint> _bufferObjects;
            std::vector<VertexBufferParams> _bufferObjectParams;
            unsigned int _vertexNumber {0};

            uint _tessLevel {2};

            void cleanFbo();
            void cleanBuffers();

            void bindColorBuffers();
            void unbindColorBuffers();

            void drawToWindow();
            std::vector<int> findGLVersion();

            bool initializeFBO();
            void initializeTexture(GLuint tex, GLint format);

            static void glfwErrorCallback(int code, const char* msg);
            static void glMsgCallback(GLenum, GLenum, GLuint, GLenum, GLsizei, const GLchar*, const void*);

            void runGraphics();
            void runCompute();
    };

    /*********/
    template<typename T>
    std::vector<T> GLEngine::getFeedbackBuffer(unsigned int index)
    {
        if (index >= _bufferObjects.size())
            return {};

        if (!_isManuallyLocked)
        {
            _isContextLocked = true;
            glfwMakeContextCurrent(_glWindow);
        }

        int size;
        if (_bufferObjectParams[index].type == GL_TRANSFORM_FEEDBACK_BUFFER)
        {
            glBindBufferBase(_bufferObjectParams[index].type, 0, _bufferObjects[index]);
            size = _shader->getQueryValue() * 3 * 4 * sizeof(T); // # of primitives * 3 vertices per primitive * 4 components per vertex
            //glGetBufferParameteriv(_bufferObjectParams[index].type, GL_TRANSFORM_FEEDBACK_BUFFER_SIZE, &size);
        }
        else
        {
            glBindBuffer(_bufferObjectParams[index].type, _bufferObjects[index]);
            glGetBufferParameteriv(_bufferObjectParams[index].type, GL_BUFFER_SIZE, &size);
        }

        //GLsize
        auto buffer = std::vector<T>(std::ceil((float)size / (float)sizeof(T)));

        if (_bufferObjectParams[index].type == GL_TRANSFORM_FEEDBACK_BUFFER)
            glGetBufferSubData(_bufferObjectParams[index].type, 0, size, buffer.data());
        else
            glGetBufferSubData(_bufferObjectParams[index].type, 0, size, buffer.data());

        glBindBuffer(_bufferObjectParams[index].type, 0);

        if (!_isManuallyLocked)
        {
            _isContextLocked = false;
            glfwMakeContextCurrent(nullptr);
        }

        return buffer;
    }

    /*********/
    template<typename T>
    std::vector<T> GLEngine::getColorBuffer(unsigned int index)
    {
        if (index >= _glColorTex.size())
            return {};

        if (!_isManuallyLocked)
        {
            _isContextLocked = true;
            glfwMakeContextCurrent(_glWindow);
        }
        glBindTexture(GL_TEXTURE_2D, _glColorTex[index]);
        std::vector<T> buffer;

        switch (_glColorFormats[index])
        {
        case GL_RGBA:
            buffer.resize(_glWidth * _glHeight * 4 * sizeof(uint8_t));
            glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer.data());
            break;
        case GL_RGB:
            buffer.resize(_glWidth * _glHeight * 3 * sizeof(uint8_t));
            glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_UNSIGNED_BYTE, buffer.data());
            break;
        case GL_R32UI:
            buffer.resize(_glWidth * _glHeight * sizeof(uint32_t));
            glGetTexImage(GL_TEXTURE_2D, 0, GL_RED, GL_UNSIGNED_INT, buffer.data());
            break;
        case GL_R32I:
            buffer.resize(_glWidth * _glHeight * sizeof(int32_t));
            glGetTexImage(GL_TEXTURE_2D, 0, GL_RED, GL_INT, buffer.data());
            break;
        case GL_R32F:
            buffer.resize(_glWidth * _glHeight * sizeof(float));
            glGetTexImage(GL_TEXTURE_2D, 0, GL_RED, GL_FLOAT, buffer.data());
            break;
        case GL_RGBA32F:
            buffer.resize(_glWidth * _glHeight * 4 * sizeof(float));
            glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, buffer.data());
            break;
        }

        glBindTexture(GL_TEXTURE_2D, 0);

        if (!_isManuallyLocked)
        {
            _isContextLocked = false;
            glfwMakeContextCurrent(nullptr);
        }

        return buffer;
    }
    
} // end of namespace

#endif
