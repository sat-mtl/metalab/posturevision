/*
 * Copyright (C) 2015 Bruno Roy
 *
 * This file is part of posturevision.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @sculptGL-impl.h
 * The SculptGLImpl implementation class, and utility classes
 */

#include <mutex>
#include <vector>

#include <glm/glm.hpp>

#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/PolygonMesh.h>
#include <pcl/gpu/octree/octree.hpp>

#include "shader.h"
#include "glEngine.h"
#include "meshserializer.h"
#include "calibrationreader.h"
#include "pointcloudserializer.h"

#ifndef __POSTURE_SCULPTGL_IMPL
#define __POSTURE_SCULPTGL_IMPL

namespace posture
{

    typedef pcl::Vertices Polygon;

    class SculptGLImpl
    {
    public:
        /**
         * \brief Constructor
         */
        SculptGLImpl();

        /**
         * \brief Destructor
         */
        ~SculptGLImpl();

        explicit operator bool() const
        {
            return _ready;
        }

        /**
         * \brief Get the serialized mesh
         * \return serialized mesh
         */
        unsigned long long getMesh(std::vector<unsigned char>& mesh);
        unsigned long long getMesh(pcl::PolygonMesh::Ptr mesh);

        /**
         * \brief Set the path where to find the calibration file
         * \param calibration Calibration parameters for the camera
         */
        void setCalibration(const std::vector<CalibrationParams>& calibration)
        {
            std::unique_lock<std::mutex> lock(_mutex);
            _calibrationParams = calibration;
        }

        /**
         * \brief Set output mesh compression
         * \param active true if the mesh is compressed
         */
        void setCompressMesh(bool active) {_meshSerializer.setCompress(active);}

        /**
         * \brief Set the input mesh
         * \param mesh Serialized input mesh
         */
        void setInputMesh(const std::vector<unsigned char>& mesh);
        void setInputMesh(pcl::PolygonMesh::Ptr mesh);

        void setInputCloud(const std::vector<char>& cloud);
        void setInputCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);

        /**
         * @brief Set the point separation of the cloud
         * @param separation Distance in float (usally between 0 and 1)
         */
        void setPointSeparation(float separation);

        /**
         * @brief Set the maximum number of neighbors nearby a polygon
         * @param maxNeighbors Maximum number of neighbors in search radius
         */
        void setMaxNeighbors(int maxNeighbors);

    private:
        std::mutex _mutex;
        bool _ready {false};
        float _pointSeparation {0.1f};
        int _maxNeighbors {5};

        std::vector<CalibrationParams> _calibrationParams;

        //GL
        std::shared_ptr<GLEngine> _glEngine;
        std::shared_ptr<Shader> _shaderComputeProjectPP;
        std::shared_ptr<Shader> _shaderTessellate;

        //PCL
        MeshSerializer _meshSerializer;
        PointCloudSerializer<pcl::PointXYZRGBNormal> _cloudSerializer;
        pcl::gpu::Octree::Ptr _octree;

        unsigned long long _timestamp {0};
        pcl::PolygonMesh::Ptr _mesh {nullptr};
        pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr _cloud {nullptr};

        std::chrono::time_point<std::chrono::system_clock> _start, _end;

        glm::mat4 getTransformationForCamera(unsigned int index);

        glm::vec3 getPolygonCentroid(pcl::PointXYZ v1, pcl::PointXYZ v2, pcl::PointXYZ v3);
    };

}

#endif // __POSTURE_SCULPTGL_IMPL
