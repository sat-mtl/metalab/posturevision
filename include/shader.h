/*
 * Copyright (C) 2015 Emmanuel Durand
 *
 * This file is part of posturevision.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/*
 * @shader.h
 * The Shader class
 */

#ifndef SHADER_H
#define SHADER_H

#define GL_GLEXT_PROTOTYPES
#define GLX_GLXEXT_PROTOTYPES
#include <GLFW/glfw3.h>

#include <atomic>
#include <map>
#include <memory>
#include <string>
#include <vector>
#include <unordered_map>

#include <glm/glm.hpp>

#include "config.h"

#include "coretypes.h"

namespace posture {

class GLEngine;

class Shader
{
    public:
        typedef std::shared_ptr<Shader> ptr;

        enum ProgramType
        {
            prgGraphic = 0,
            prgCompute,
            prgFeedback
        };

        enum ShaderType
        {
            vertex = 0,
            tess_ctrl,
            tess_eval,
            geometry,
            fragment,
            compute
        };

        enum Sideness
        {
            doubleSided = 0,
            singleSided,
            inverted
        };

        enum Fill
        {
            texture = 0,
            texture_rect,
            color,
            primitiveId,
            uv,
            wireframe,
            window
        };

        /**
         * Constructor
         */
        Shader(const std::weak_ptr<GLEngine>& engine, ProgramType type = prgGraphic);

        /**
         * Destructor
         */
        ~Shader();

        /**
         * No copy constructor, but a move one
         */
        Shader(const Shader&) = delete;
        Shader& operator=(const Shader&) = delete;

        /**
         * Activate this shader
         */
        void activate();

        /**
         * Deactivate this shader
         */
        void deactivate();

        /**
         * Launch the compute shader, if present
         */
        void doCompute();

        /**
         * Return the program type
         */
        ProgramType getProgramType() {return _programType;}

        /**
         * Set the sideness of the object
         */
        void setSideness(const Sideness side) {_sideness = side;}
        Sideness getSideness() const {return _sideness;}

        /**
         * Set the size for a computation shader
         */
        void setComputeSize(int x, int y)
        {
            _computeSize = {x, y};
        }

        /**
         * Set a shader source
         */
        void setSource(std::string src, const ShaderType type);

        /**
         * Set a shader source from file
         */
        void setSourceFromFile(const std::string filename, const ShaderType type);

        /**
         * Set the model view and projection matrices
         */
        void setModelViewProjectionMatrix(const glm::dmat4& mv, const glm::dmat4& mp);

        /**
         * Set a uniform
         */
        void setUniform(const std::string& name, const Values& values);

        /**
         * Set the currently queued uniforms updates
         */
        void updateUniforms();

        /**
         * Compile the shader program
         */
        void compileProgram();

        /**
         * Add a feedback varyings attribute
         */
        void addFeedbackAttribute(const std::string attribute)
        {
            _feedbackVaryings.push_back(attribute);
        }

        /**
         * Return value captured from a query object
         */
        GLuint getQueryValue() {return _queryValue;}

    private:
        std::weak_ptr<GLEngine> _engine;
        mutable std::mutex _mutex;
        std::atomic_bool _activated {false};
        ProgramType _programType {prgGraphic};

        std::unordered_map<int, GLuint> _shaders;
        std::unordered_map<int, std::string> _shadersSource;
        GLuint _program {0};
        bool _isLinked = {false};

        struct Uniform
        {
            std::string type {""};
            Values values {};
            GLint glIndex {-1};
            GLuint glBuffer {0};
            bool glBufferReady {false};
        };
        std::map<std::string, Uniform> _uniforms;
        std::vector<std::string> _uniformsToUpdate;

        // A map of previously compiled programs
        std::string _currentProgramName {};
        std::map<std::string, GLuint> _compiledPrograms;
        std::map<std::string, std::string> _compiledProgramsOptions;
        std::map<std::string, bool> _programsLinkStatus;

        // Rendering parameters
        Fill _fill {texture};
        std::string _shaderOptions {""};
        Sideness _sideness {doubleSided};
        std::vector<int> _computeSize {32, 32};

        // Transform feedback
        GLuint _feedbackQuery;
        GLuint _queryValue;
        std::vector<std::string> _feedbackVaryings {"outVertex"};

        /**
         * Link the shader program
         */
        bool linkProgram();

        /**
         * Parses the shader to find uniforms
         */
        void parseUniforms(const std::string& src);

        /**
         * Get a string expression of the shader type, used for logging
         */
        std::string stringFromShaderType(int type);

        /**
         * Replace a shader with an empty one
         */
        void resetShader(ShaderType type);

        /**
         * Register feedback varyings attributes
         */
        void registerFeedbackAttributes();
};

typedef std::shared_ptr<Shader> ShaderPtr;

} // end of namespace

#endif // SHADER_H
