/*
 * Copyright (C) 2013 Emmanuel Durand, Bruno Roy
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @pointcloudmerger.h
 * The PointCloudMergerImpl class
 */

#ifndef __POINTCLOUDMERGER_IMPL_H__
#define __POINTCLOUDMERGER_IMPL_H__

#include <functional>
#include <iostream>
#include <string>
#include <memory>
#include <mutex>
#include <vector>

#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/registration/transforms.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/point_cloud.h>

#include "calibrationreader.h"
#include "config.h"
#include "constants.h"
#include "calibrationreader.h"
#include "filter.h"
#include "pointcloudserializer.h"

namespace posture
{
    /*************/
    //! PointCloudMergerImpl class, merges clouds according to a calibration file
    class PointCloudMergerImpl
    {
        public:
            /**
             * \brief Constructor
             */
            PointCloudMergerImpl();
    
            /**
             * \brief Destructor
             */
            ~PointCloudMergerImpl();

            /**
             * \brief Set the path where to find the calibration file
             * \param calibration Calibration parameters for the camera
             */
            void setCalibration(const std::vector<CalibrationParams>& calibrations)
            {
                std::unique_lock<std::mutex> lock(_mutex);
                _calibrationParams = calibrations;
                initializeTransformation();
            }
            
            /**
             * \brief Set the number of clouds to merge
             * \param cloudNbr Number of clouds
             */
            void setCloudNbr (unsigned int cloudNbr);

            /**
             * \brief Set whether to compress the output cloud or not
             * \param compress true for compress the output cloud
             */
            void setCompression(bool compress);

            /**
             * \brief Set the downsampling
             * \param active Downsampling is active if true
             * \param resolution Target resolution
             */
            void setDownsampling(bool active, float resolution);
    
            /**
             * \brief Sets the size of the random noise (used if no input cloud is set).
             * \param size Size of the cloud. The effective value used is the square of this value
             */
            void setRandomNoiseSize(const unsigned int size) {_noiseRes = size; printf("Random noise size set to %i\n", size * size);}
    
            /**
             * \brief Set one of the input clouds
             * \param index Index of the cloud to be set
             * \param cloud Added point cloud
             * \param compressed true if the input cloud is compressed
             */
            void setInputCloud(const unsigned int index, const std::vector<char>& cloud, const bool compressed);
            void setInputCloud(const unsigned int index, std::vector<char>&& cloud, const bool compressed);
            void setInputCloud(const unsigned int index, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);

            /**
             * \brief Activate the saving of current cloud
             * \param active Saving state
             */
            void setSaveCloud(bool active);
    
            /**
             * \brief Get the merged cloud
             * \param cloud pointer on the object where to write the cloud
             * \return Timestamp for the grab (in ms, from epoch)
             */
            unsigned long long getCloud(std::vector<char>& cloud);
            unsigned long long getCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);
    
            /**
             * \brief Sets if a cloud is loaded from file.
             * \param pcdPath Path to PCD file.
             */
            void setLoadedPCD(std::string pcdPath);

            /**
             * \brief Set whether to save the clouds separately or not
             * \param saveSeparately true to save the inputs in separate files
             */
            void setSaveSeparately (const bool saveSeparately);
    
            /**
             * \brief Starts the merger
             */
            void start();
    
            /**
             * \brief Stops the merger
             */
            void stop();
    
        private:
            Filter _filter;
            PointCloudSerializer<pcl::PointXYZRGBNormal> _serializer;
    
            pcl::PointCloud<pcl::PointXYZ> _pointCloud;
            std::vector<pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr> _pointClouds, _clippedClouds;
            pcl::PointCloud<pcl::PointXYZRGBNormal> _emptyCloud, _finalCloud;
            pcl::VoxelGrid<pcl::PointXYZRGBNormal> _voxelFilter;
    
            std::vector<CalibrationParams> _calibrationParams;
            std::vector<Eigen::Matrix4f> _matTransforms;

            bool _downsample {false};
            pcl::VoxelGrid<pcl::PointXYZRGBNormal> _voxelGrid;
    
            std::vector<unsigned long long> _timestamps;
            unsigned long long _mergedTimestamp {0};
    
            unsigned int _nbrSources {1};
            unsigned int _noiseRes;
            std::string _pcdPath;
            bool _saveSeparately {false};
    
            mutable std::mutex _mutex;
    
            // Methods
            void initializeTransformation();
    };

} // end of namespace

#endif // POINTCLOUDMERGER_H
