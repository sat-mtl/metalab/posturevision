/*
 * Copyright (C) 2014 Emmanuel Durand
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @shapes.h
 * Definitions of all detectable shapes
 */

#ifndef __POSTURE_SHAPES_H__
#define __POSTURE_SHAPES_H__

#include <vector>

namespace posture
{
    namespace Shapes
    {
        enum Type {
            PLANE = 0,
            SPHERE = 1
        };
    }

    struct Shape
    {
        Shapes::Type type;
        std::vector<double> coeff;
    };
} // end of namespace

#endif // __POSTURE_SHAPES_H__
