/*
 * Copyright (C) 2016 Emmanuel Durand
 *
 * This file is part of posturevision.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @register.h
 * The Register class, which does registration on point clouds
 */

#ifndef __POSTURE_REGISTER_H__
#define __POSTURE_REGISTER_H__

#include <memory>
#include <utility>
#include <vector>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include "calibrationreader.h"

namespace posture
{
    class RegisterImpl;

    /*************/
    //! Register class, which converts point clouds to meshes
    class Register
    {
        public:
            /**
             * \brief Constructor
             */
            Register();

            /**
             * \brief Destructor
             */
            ~Register();

            /**
             * \brief Get the updated calibrations
             */
            std::vector<CalibrationParams> getCalibration() const;
    
            /**
             * \brief Set one of the input clouds
             * \param index Index of the cloud to be set
             * \param cloud added point cloud
             * \param compressed true if the input cloud is compressed
             */
            void setInputCloud(const unsigned int index, const std::vector<char>& cloud, bool compressed) const;
            void setInputCloud(const unsigned int index, const pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud) const;

            /**
             * \brief Set the path where to find the calibration file
             * \param calibration Guess calibration parameters for the cameras
             */
            void setGuessCalibration(const std::vector<CalibrationParams>& calibration) const;

        private:
            std::shared_ptr<RegisterImpl> _impl {nullptr};
    };

} // end of namespace

#endif // __POSTURE_REGISTER_H__
