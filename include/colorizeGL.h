/*
 * Copyright (C) 2015 Emmanuel Durand
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @colorizeGL.h
 * The ColorizeGLGL class, which projects RGB textures onto mesh
 */

#ifndef __POSTURE_COLORIZEGL__
#define __POSTURE_COLORIZEGL__

#include <memory>
#include <utility>
#include <vector>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/PolygonMesh.h>
#include <pcl/TextureMesh.h>

#include "calibrationreader.h"

namespace posture
{
    class ColorizeGLImpl;

    /*************/
    //! ColorizeGL class, which converts point clouds to meshes
    class ColorizeGL
    {
        public:
            /**
             * \brief Constructor
             */
            ColorizeGL();

            /**
             * \brief Destructor
             */
            ~ColorizeGL();

            /**
             * \brief Get the objects texture
             * \param width reference width of the texture
             * \param height reference height of the texture
             * \return the texture
             */
            std::vector<unsigned char> getTexture(unsigned int& width, unsigned int& height) const;

            /**
             * \brief Get the serialized textured mesh
             * \return serialized textured mesh
             */
            unsigned long long getTexturedMesh(std::vector<unsigned char>& mesh, bool threaded = true) const;
            unsigned long long getTexturedMesh(pcl::TextureMesh::Ptr mesh) const;

            /**
             * \brief Set the path where to find the calibration file
             * \param calibrations Calibration parameters for the cameras
             */
            void setCalibration(const std::vector<CalibrationParams>& calibrations) const;

            /**
             * \brief Set output mesh compression
             * \param active true if the mesh is compressed
             */
            void setCompressMesh(bool active) const;

            /**
             * \brief Set the input mesh and the rgb images
             * \param mesh Serialized input mesh
             * \param images Vector of rgb (unsigned char) images
             * \param dims vector of dimensions ( int ) of the images
             */
            void setInput(std::vector<unsigned char> mesh, std::vector<std::vector<unsigned char>> images, std::vector<std::vector<unsigned int>> dims) const;
            void setInput(pcl::TextureMesh::Ptr mesh, std::vector<std::vector<unsigned char>> images, std::vector<std::vector<unsigned int>> dims) const;
            void setInput(pcl::PolygonMesh::Ptr mesh, std::vector<std::vector<unsigned char>> images, std::vector<std::vector<unsigned int>> dims) const;
            void setInput(std::vector<pcl::PolygonMesh::Ptr> multimesh, std::vector<std::vector<unsigned char>>& images, std::vector<std::vector<unsigned int>>& dims) const;

        private:
            std::shared_ptr<ColorizeGLImpl> _impl {nullptr};
    };

} // end of namespace

#endif // __POSTURE_COLORIZEGL__
