/*
 * Copyright (C) 2014 Emmanuel Durand and Sebastien Paquet
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @meshserializer.h
 * The MeshSerializer class
 */

#ifndef __POSTURE_MESHSERIALIZER_H__
#define __POSTURE_MESHSERIALIZER_H__

#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <future>

#include <pcl/PolygonMesh.h>
#include <pcl/TextureMesh.h>

#include <openctm.h>
#include "meshmerger.h"

#include "constants.h"

namespace posture
{
    /*************/
    //! MeshSerializer class
    class MeshSerializer
    {
        public:
            /**
             * \brief Constructor
             */
            MeshSerializer();

            /**
             * \brief Destructor
             */
            ~MeshSerializer();

            /**
             * \brief Converts the given mesh to triangles
             * \param mesh reference to the mesh to convert to triangles
             */
            void convertToTris(pcl::PolygonMesh::Ptr& mesh) const;

            /**
             * \brief Set whether to compress or not
             * \param compress true for compressed return mesh
             */
            void setCompress(bool compress)
            {
                if (compress)
                    setCompressionMethod(CTM_METHOD_MG2);
                else
                    setCompressionMethod(CTM_METHOD_RAW);
            }
            
            /**
             * \brief Specify whether and how to compress the mesh
             * \param aMethod = CTM_METHOD_RAW: Just store the raw data. / CTM_METHOD_MG1: Lossless compression (floating point). / CTM_METHOD_MG2: Lossless compression (fixed point).
             * \param aLevel  = LZMA compression level, 0-9 (9 = most compressed, most memory)
             * \param aPrecision  = rounding gridstep for fixed point compression (MG2 only)
             */
            void setCompressionMethod(CTMenum aMethod, int aLevel=1, float aPrecision=0.01) 
            { 
                _ctmCompressionMethod = aMethod; 
                _ctmCompressionLevel  = aLevel; 
                _ctmVertexPrecision   = aPrecision; 
            }

            /**
             * \brief Deserialize the given mesh
             * \param mesh reference mesh to deserialize
             * \param timestamp reference
             * \return deserialized mesh
             */
            pcl::PolygonMesh::Ptr deserialize(const std::vector<unsigned char>& mesh, unsigned long long& timestamp) const;
            pcl::PolygonMesh::Ptr deserializeCTM(const std::vector<unsigned char>& mesh, unsigned long long& timestamp) const;
            std::vector<pcl::PolygonMesh::Ptr> unbundleMulti(const std::vector<unsigned char>& mesh, unsigned long long& timestamp) const;

            // we support two types of serialization as input: CTM and MCTM
            pcl::TextureMesh::Ptr deserializeAsTextured(const std::vector<unsigned char>& mesh, unsigned long long& timestamp) const;
            pcl::TextureMesh::Ptr deserializeAsTexturedCTM(const std::vector<unsigned char>& mesh, unsigned long long& timestamp) const;
            std::vector<pcl::TextureMesh::Ptr> unbundleAsTexturedMulti(const std::vector<unsigned char>& mesh, unsigned long long& timestamp) const;

            /**
             * \brief Serialize the given mesh
             * \param inputMesh reference mesh to serialize
             * \param timestamp
             * \return serialized mesh
             */
            std::vector<unsigned char> serialize(const pcl::PolygonMesh::Ptr inputMesh, unsigned long long timestamp = 0) const;
            std::vector<unsigned char> serialize(const pcl::TextureMesh::Ptr inputMesh, unsigned long long timestamp = 0) const;
            std::vector<unsigned char> serialize(const std::vector<pcl::PolygonMesh::Ptr> submeshList, unsigned long long timestamp = 0) const;
            std::vector<unsigned char> serialize(const std::vector<pcl::TextureMesh::Ptr> submeshList, unsigned long long timestamp = 0) const;

        private:
            CTMenum _ctmCompressionMethod {CTM_METHOD_RAW};
            int _ctmCompressionLevel {1};
            float _ctmVertexPrecision {0.01};

            static std::vector<unsigned char> serializeOneP(const pcl::PolygonMesh::Ptr& mesh, unsigned long long timestamp, CTMenum cm, int cl, float cvp);
            static std::vector<unsigned char> serializeOneT(const pcl::TextureMesh::Ptr& mesh, unsigned long long timestamp, CTMenum cm, int cl, float cvp);
            static pcl::PolygonMesh::Ptr deserializeOne(const std::vector<unsigned char>& mesh, unsigned long long& timestamp);
            static pcl::TextureMesh::Ptr deserializeAsTexturedOne(const std::vector<unsigned char>& mesh, unsigned long long& timestamp);
            
            template<typename MeshPtr>
            static MeshPtr deserializeMultiTemplated(const std::vector<unsigned char>& inputBuffer, unsigned long long& timestamp, MeshPtr& outputMesh)
            {
                /* ... work-in-progress ...
                // simply calls regular deserialize and returns result

                outputMesh = deserializeMulti(inputBuffer, )
                return outputMesh */
            }
    };
}

#endif // __POSTURE_MESHSERIALIZER_H__
