/*
 * Copyright (C) 2014 Emmanuel Durand, Bruno Roy
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @colorize-impl.h
 * The Colorize implementation class, and utility classes
 */

#ifndef __POSTURE_COLORIZE_IMPL__
#define __POSTURE_COLORIZE_IMPL__

#include <mutex>
#include <vector>

#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/PolygonMesh.h>
#include <pcl/TextureMesh.h>
#include <pcl/surface/texture_mapping.h>

#include "calibrationreader.h"
#include "config.h"
#include "constants.h"
#include "pointcloudserializer.h"
#include "meshserializer.h"
#include "calibrationreader.h"

namespace posture
{
    /*************/
    //! ColorizeImpl class, which converts point clouds to meshes
    class ColorizeImpl
    {
        public:
            /**
             * \brief Constructor
             */
            ColorizeImpl();

            /**
             * \brief Destructor
             */
            ~ColorizeImpl();

            /**
             * \brief Get the objects texture
             * \param width reference width of the texture
             * \param height reference height of the texture
             * \return the texture
             */
            std::vector<unsigned char> getTexture(unsigned int& width, unsigned int& height);

            /**
             * \brief Get the serialized textured mesh
             * \return serialized textured mesh
             */
            unsigned long long getTexturedMesh(std::vector<unsigned char>& mesh);
            unsigned long long getTexturedMesh(pcl::TextureMesh::Ptr mesh);

            /**
             * \brief Set the path where to find the calibration file
             * \param calibrations Calibration parameters for the cameras
             */
            void setCalibration(const std::vector<CalibrationParams>& calibrations)
            {
                std::unique_lock<std::mutex> lock(_mutex);
                _calibrationParams = calibrations;
            }

            /**
             * \brief Set output mesh compression
             * \param active true if the mesh is compressed
             */
            void setCompressMesh(bool active) {_meshSerializer.setCompress(active);}

            /**
             * \brief Set whether to compute the UVs or not
             * \param active true to compute the UVs
             */
            void setComputeTexCoords(bool active);

            /**
             * \brief Set the input mesh and the rgb images
             * \param mesh Serialized input mesh
             * \param images Vector of rgb (unsigned char) images
             * \param dims Vector of dimensions (width, height, channels) of the input images
             */
            void setInput(std::vector<unsigned char>& mesh, std::vector<std::vector<unsigned char>>& images, std::vector<std::vector<unsigned int>>& dims);
            void setInput(pcl::TextureMesh::Ptr mesh, std::vector<std::vector<unsigned char>> images, std::vector<std::vector<unsigned int>> dims);
            void setInput(pcl::PolygonMesh::Ptr mesh, std::vector<std::vector<unsigned char>> images, std::vector<std::vector<unsigned int>> dims);

        private:
            std::mutex _mutex;
            
            std::vector<CalibrationParams> _calibrationParams;
            bool _computeTexCoords {false};

            MeshSerializer _meshSerializer;
            pcl::TextureMapping<pcl::PointNormal> _textureMapping;

            unsigned long long _timestamp {0};
            std::vector<std::vector<unsigned char>> _images {};
            std::vector<std::vector<unsigned int>> _dims {};
            pcl::TextureMesh::Ptr _mesh {nullptr};
            pcl::TextureMesh::Ptr _textureMesh {nullptr};

            std::vector<unsigned char> _mergedImage {};
            unsigned int _mergedWidth, _mergedHeight;

            Eigen::Affine3f getTransformationForCamera(unsigned int index);
    };

} // end of namespace

#endif // __POSTURE_COLORIZE_IMPL__
