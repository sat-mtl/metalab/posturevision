/*
 * Copyright (C) 2013 Emmanuel Durand
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @pointcloudserializer.h
 * The PointCloudSerializer class, and utility classes
 */

#ifndef __POINTCLOUDSERIALIZER_H__
#define __POINTCLOUDSERIALIZER_H__

#include <chrono>
#include <functional>
#include <iostream>
#include <memory>
#include <mutex>
#include <sstream>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/compression/octree_pointcloud_compression.h>

#include "config.h"

namespace pio = pcl::io;
namespace poct = pcl::octree;

namespace posture
{
    /*************/
    //! NetworkPointCloudCompression class, derived from pcl::io::OctreePointCloudCompression with optimizations for network
    template<typename PointT, typename LeafT = poct::OctreeContainerPointIndices,
        typename BranchT = poct::OctreeContainerEmpty,
        typename OctreeT = poct::Octree2BufBase<LeafT, BranchT> >
    class NetworkPointCloudCompression : public pio::OctreePointCloudCompression<PointT, LeafT, BranchT, OctreeT>
    {
        typedef typename pio::OctreePointCloudCompression<PointT, LeafT, BranchT, OctreeT>::PointCloudPtr PointCloudPtr;
    
        public:
            /**
             * \brief Constructor
             * \param compressionProfile_arg Specify here a known compression profile
             * \param showStatistics_arg Output compression statistics
             * \param pointResolution_arg Precision of point coordinates
             * \param octreeResolution_arg Octree resolution at lowest octree level
             * \param doVoxelGridDownDownSampling_arg Voxel grid filtering
             * \param iFrameRate_arg I-frame encoding rate
             * \param doColorEncoding_arg Enable/disable color coding
             * \param colorBitResolution_arg Color bit depth
             */
            NetworkPointCloudCompression(const pio::compression_Profiles_e compressionProfile_arg = pio::MED_RES_ONLINE_COMPRESSION_WITH_COLOR,
                                         const bool showStatistics_arg = false,
                                         const double pointResolution_arg = 0.001,
                                         const double octreeResolution_arg = 0.01,
                                         const bool doVoxelGridDownDownSampling_arg = false,
                                         const unsigned int iFrameRate_arg = 0,
                                         const bool doColorEncoding_arg = true,
                                         const unsigned char colorBitResolution_arg = 6) :
                pio::OctreePointCloudCompression<PointT, LeafT, BranchT, OctreeT> (compressionProfile_arg, showStatistics_arg, pointResolution_arg,
                    octreeResolution_arg, doVoxelGridDownDownSampling_arg, iFrameRate_arg, doColorEncoding_arg, colorBitResolution_arg),
                validFrame_(PointCloudPtr()),
                prevValidFrame_(0)
            {
            }
    
            /**
             * \brief Destructor
             */
            ~NetworkPointCloudCompression()
            {
            }
    
            /**
             * \brief Decode the point cloud contained in the given istream to the given PointCloudPtr
             * \param compressedTreeDataIn_arg Binary input stream containing compressed data
             * \param cloud_arg Reference to the decoded point cloud
             */
            void decodeNetworkPointCloud(std::istream &compressedTreeDataIn_arg, PointCloudPtr &cloud_arg);
    
        private:
            PointCloudPtr validFrame_;
            bool firstFrame_;
    
            unsigned int prevValidFrame_;
    };
    
    /*************/
    template<typename PointT, typename LeafT, typename BranchT, typename OctreeT>
    void NetworkPointCloudCompression<PointT, LeafT, BranchT, OctreeT>::decodeNetworkPointCloud(std::istream &compressedTreeDataIn_arg,
        PointCloudPtr &cloud_arg)
    {
        typename pcl::PointCloud<PointT>::Ptr tempCloud (new pcl::PointCloud<PointT>());
    
        try
        {
            this->decodePointCloud(compressedTreeDataIn_arg, tempCloud);
            if (this->frame_ID_ == prevValidFrame_+1 || this->i_frame_ == true)
            {
                validFrame_ = tempCloud;
                prevValidFrame_ = this->frame_ID_;
                firstFrame_ = false;
            }
        }
        catch (...)
        {
            std::cout << "Exception caught." << std::endl;
        }
    
        if (firstFrame_ == false)
        {
            cloud_arg = validFrame_;
        }
    }
    
    /*************/
    //! PointCloudBlob class, designed to carry uncompressed cloud over the network
    template <typename T>
    class PointCloudBlob
    {
        public:
            /**
             * \brief Constructor
             */
            PointCloudBlob();
    
            /**
             * \brief Destructor
             */
            ~PointCloudBlob();
    
            /**
             * \brief Encodes the specified PointCloud to a binary blob
             * \param cloud Reference to the cloud
             * \param size Reference to the size of the blob after being set
             * \param timestamp Timestamp for the cloud.
             * \return A pointer to the resulting binary blob of the cloud
             */
            std::vector<char> toBlob(typename pcl::PointCloud<T>::Ptr cloud, const unsigned long long timestamp = 0);
    
            /**
             * \brief Decodes the specified binary blob to a PointCloud
             * \param blob Pointer to the binary blob to decode
             * \param size Size of the blob
             * \param timestamp Timestamp will be stored in this pointer
             * \return A shared pointer to a pcl::PointCloud
             */
            typename pcl::PointCloud<T>::Ptr toCloud(const std::vector<char>& blob, unsigned long long& timestamp = 0);
    };
    
    /*************/
    template <typename T>
    PointCloudBlob<T>::PointCloudBlob()
    {
    }

    
    /*************/
    template <typename T>
    PointCloudBlob<T>::~PointCloudBlob()
    {
    }
     
    /*************/
    //! PointCloudSerializer class, to write point clouds to shmdata
    template <typename T>
    class PointCloudSerializer
    {
        public:
            typedef typename pcl::PointCloud<T>::Ptr cloudPtr;
    
            /**
             * \brief Constructor
             */
            PointCloudSerializer();
    
            /**
             * \brief Set the compression to apply to the input point clouds. Only meaningful if the PointCloudSerializer is set as a reader.
             * \param compressionProfile_arg Specify here a known compression profile
             * \param showStatistics_arg Output compression statistics
             * \param pointResolution_arg Precision of point coordinates
             * \param octreeResolution_arg Octree resolution at lowest octree level
             * \param doVoxelGridDownDownSampling_arg Voxel grid filtering
             * \param iFrameRate_arg I-frame encoding rate
             * \param doColorEncoding_arg Enable/disable color coding
             * \param colorBitResolution_arg Color bit depth
             */
            void setCompression(const bool compress,
                                const pio::compression_Profiles_e compressionProfile = pio::MED_RES_ONLINE_COMPRESSION_WITH_COLOR,
                                const bool showStatistics = false,
                                const double pointResolution = 0.001,
                                const double octreeResolution = 0.01,
                                const bool doVoxelGridDownDownSampling = false,
                                const unsigned int iFrameRate = 0,
                                const bool doColorEncoding = true,
                                const unsigned char colorBitResolution = 6);
    
            /**
             * Deserialize the given cloud
             */
            cloudPtr deserialize(const std::vector<char>& sCloud, bool compressed, unsigned long long& timestamp) const;
    
            /**
             * Serialize the given cloud
             */
            std::vector<char> serialize(const cloudPtr& cloud, unsigned long long timestamp = 0) const;
    
        private:
            mutable std::mutex _mutex {};
            bool _compress {false};
    
            std::shared_ptr<NetworkPointCloudCompression<T>> _networkPointCloudEncoder;
            std::shared_ptr<PointCloudBlob<T>> _blober;
    };

} // end of namespace

#endif // POINTCLOUDSERIALIZER_H
