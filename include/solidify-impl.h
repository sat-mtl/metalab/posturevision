/*
 * Copyright (C) 2014 Emmanuel Durand, Bruno Roy
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @solidify-impl.h
 * The Solidify implementation class, and utility classes
 */

#ifndef __POSTURE_SOLIDIFY_IMPL__
#define __POSTURE_SOLIDIFY_IMPL__

#include <mutex>
#include <vector>

#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/PolygonMesh.h>
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/surface/marching_cubes_hoppe.h>
#include <pcl/surface/marching_cubes_rbf.h>

#include <glm/glm.hpp>

#include "pointcloudserializer.h"
#include "meshserializer.h"

namespace posture
{
    /*************/
    //! SolidifyImpl class, which converts point clouds to meshes
    class SolidifyImpl
    {
        public:
            /**
             * \brief Constructor
             */
            SolidifyImpl();

            /**
             * \brief Destructor
             */
            ~SolidifyImpl();

            /**
             * \brief Get the resulting mesh
             * \param mesh Reference to the object where the mesh will be written
             * \return Timestamp Reference for the grab (in ms, from epoch)
             */
            unsigned long long getMesh(std::vector<unsigned char>& mesh);
            unsigned long long getMesh(pcl::PolygonMesh::Ptr mesh);

            /**
             * \brief Set the grid resolution for the reconstruction
             * \param res Grid resolution
             */
            void setGridResolution(int res);

            /**
             * \brief Set the input cloud / camera images to convert
             * \param cloud The input cloud
             * \param compressed True if the cloud is compressed
             */
            void setInputCloud(std::vector<char> cloud, bool compressed);
            void setInputCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);

            /**
             * \brief Activate the saving of created mesh
             * \param active Saving state
             */
            void setSaveMesh(bool active);

        private:
            std::mutex _mutex;

            PointCloudSerializer<pcl::PointXYZRGBNormal> _pclSerializer;
            MeshSerializer _meshSerializer;

            pcl::MarchingCubes<pcl::PointXYZRGBNormal>::Ptr _marchingCubes {nullptr};
            pcl::search::KdTree<pcl::PointXYZRGBNormal>::Ptr _searchTree {nullptr};
            unsigned int _gridMaxResolution {16};

            unsigned long long _timestamp;
            pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr _cloud {nullptr};
            pcl::PolygonMesh::Ptr _mesh {nullptr};

            bool _saveMesh {false};

            void filterNaNFromMesh(pcl::PolygonMesh::Ptr mesh);
    };

} // end of namespace

#endif // __POSTURE_SOLIDIFY_IMPL__
