/*
 * Copyright (C) 2014 Emmanuel Durand
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @detect.h
 * The Detect class
 */

#ifndef __POSTURE_DETECT_H__
#define __POSTURE_DETECT_H__

#include <functional>
#include <iostream>
#include <string>
#include <memory>
#include <mutex>
#include <vector>

#include "shapes.h"

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/PolygonMesh.h>

namespace posture
{
    class DetectImpl;

    /*************/
    //! Detect class
    class Detect
    {
        public:
            /**
             * \brief Constructor
             */
            Detect();
    
            /**
             * \brief Destructor
             */
            ~Detect();

            /**
             * \brief Do the detection process
             * \return True if something was detected
             */
            bool detect();

            /**
             * \brief Get the input cloud, with the detected shapes colored
             * \param cloud Pointer on the structure where to write the output point cloud
             * \return Timestamp for the grab (in ms, from epoch)
             */
            unsigned long long getProcessedCloud(std::vector<char>& cloud);
            unsigned long long getProcessedCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);

            /**
             * \brief Get a convex hull poly for the detected cloud specified by the index
             * \param index index of the cloud
             * \param mesh Pointer on the structure where to write the output mesh
             * \return Timestamp for the grab (in ms, from epoch)
             */
            unsigned long long getConvexHull (std::vector<unsigned char> mesh, unsigned int index);
            unsigned long long getConvexHull (pcl::PolygonMesh::Ptr mesh, unsigned int index);

            /**
             * \brief Get a vector containing the detected shapes
             * \return the shape vector
             */
            std::vector<Shape> getDetectedShapes();
    
            /**
             * \brief Set one of the input clouds
             * \param cloud added point cloud
             * \param compressed true if the cloud is compressed
             */
            void setInputCloud(const std::vector<char>& cloud, const bool compressed);
            void setInputCloud(const pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr& cloud);
    
        private:
            std::shared_ptr<DetectImpl> _impl {nullptr};
    };

} // end of namespace

#endif // DETECT_H
