/*
 * Copyright (C) 2014 Emmanuel Durand, Bruno Roy
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @posturevision.h
 * The PostureVision main include file
 */

#ifndef __POSTURE_VISION_H__
#define __POSTURE_VISION_H__

#include <posture/calibrationreader.h>
#include <posture/colorize.h>
#include <posture/colorizeGL.h>
#include <posture/detect.h>
#include <posture/display.h>
#include <posture/meshmerger.h>
#include <posture/meshserializer.h>
#include <posture/pointcloudmerger.h>
#include <posture/register.h>
#include <posture/solidify.h>
#include <posture/zcamera.h>
#include <posture/solidifyGPU.h>
#include <posture/sculptGL.h>

#endif // POSTUREVISION_H
