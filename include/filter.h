/*
 * Copyright (C) 2016 Emmanuel Durand
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @filter.h
 * A class dedicating to filtering the clouds and meshes
 * according to the calibration
 */

#ifndef __FILTER_H__
#define __FILTER_H__

#include <fstream>
#include <functional>
#include <map>
#include <memory>
#include <mutex> 
#include <string>

#include <boost/make_shared.hpp>

#include "config.h"

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/TextureMesh.h>
#include <pcl/PolygonMesh.h>
#include <pcl/conversions.h>
#include <pcl/filters/passthrough.h>

#include "constants.h"
#include "calibrationreader.h"

namespace posture
{
    /*************/
    //! Filter class
    class Filter
    {
    public:
        /**
         * \brief Constructor
         */
        Filter();

        /**
         * \brief Apply clipping from the given calibration to the input
         * \param input Input cloud, mesh, ...
         * \param calibration Calibration
         * \return The clipped cloud, mesh, ...
         */
        template <typename T>
        typename pcl::PointCloud<T>::Ptr applyClipping(typename pcl::PointCloud<T>::Ptr input, const CalibrationParams& calibration);

        /**
         * \brief Apply clipping from geometric shapes. The PolygonMesh and TextureMesh versions modify the input mesh.
         * \param input Input cloud, mesh, ...
         * \param calibration Calibration
         */
        template <typename T>  // TODO: make the cloud version void like the mesh versions?
        typename pcl::PointCloud<T>::Ptr applyShapeClipping(typename pcl::PointCloud<T>::Ptr input, const CalibrationParams& calibration);
        void applyShapeClipping(pcl::PolygonMesh::Ptr& input, const CalibrationParams& calibration);
        void applyShapeClipping(pcl::TextureMesh::Ptr& input, const CalibrationParams& calibration);

        /**
         * \brief remove NaN vertices 
         * \param input Input cloud, mesh, ...
         */
        void filterNaN(pcl::PolygonMesh::Ptr& mesh);
        void filterNaN(pcl::TextureMesh::Ptr& mesh);

    private:
        std::map<std::string, std::function<float(const std::vector<float>&, const std::vector<float>&)>> _distanceFunctions;

        template <typename T>
        bool isInShapes(const T& point, const CalibrationParams& calibration);
    };

    /*********/
    template <typename T>
    typename pcl::PointCloud<T>::Ptr Filter::applyClipping(typename pcl::PointCloud<T>::Ptr cloud, const CalibrationParams& calibration)
    {
        using PointCloudT = typename pcl::PointCloud<T>;
        using PointCloudTPtr = typename pcl::PointCloud<T>::Ptr;

        PointCloudTPtr clipped;
        clipped = boost::make_shared<PointCloudT>(*(cloud.get()));
    
        typename pcl::PassThrough<T> filterClipping;
        filterClipping.setKeepOrganized(true);
    
        if (calibration.isValid)
        {
            filterClipping.setInputCloud(clipped);
            filterClipping.setFilterFieldName("x");
            filterClipping.setFilterLimits(calibration.min_clip_x, calibration.max_clip_x);
            filterClipping.filter(*clipped);
            filterClipping.setFilterFieldName("y");
            filterClipping.setFilterLimits(calibration.min_clip_y, calibration.max_clip_y);
            filterClipping.filter(*clipped);
            filterClipping.setFilterFieldName("z");
            filterClipping.setFilterLimits(calibration.min_clip_z, calibration.max_clip_z);
            filterClipping.filter(*clipped);
        }
        else
        {
            filterClipping.setInputCloud(clipped);
            filterClipping.filter(*clipped);
        }
    
        return clipped;
    }

    /*********/
    template <typename T>
    typename pcl::PointCloud<T>::Ptr Filter::applyShapeClipping(typename pcl::PointCloud<T>::Ptr cloud, const CalibrationParams& calibration)
    {
        using PointCloudT = typename pcl::PointCloud<T>;
        using PointCloudTPtr = typename pcl::PointCloud<T>::Ptr;

        PointCloudTPtr clipped;
        clipped = boost::make_shared<PointCloudT>(*(cloud.get()));
    
        PointCloudTPtr newCloud = boost::make_shared<pcl::PointCloud<T>>();
        T p;
        int count = 0;
        for (unsigned int i = 0; i < clipped->size(); ++i)
        {
            bool keep = false;
            p = clipped->points[i];
            
            for_each (calibration.shapes.begin(), calibration.shapes.end(), [&](ClippingShape shape) {
                if (!shape.include || keep == true)
                    return;
    
                if (_distanceFunctions.find(shape.name) == _distanceFunctions.end())
                    return;
    
                if (_distanceFunctions[shape.name]({p.x, p.y, p.z}, shape.params) < 0.f)
                    keep = true;
            });
            
            if (keep)
            {
                for_each (calibration.shapes.begin(), calibration.shapes.end(), [&](ClippingShape shape) {
                    if (shape.include)
                        return;
    
                    if (_distanceFunctions.find(shape.name) == _distanceFunctions.end())
                        return;
    
                    if (_distanceFunctions[shape.name]({p.x, p.y, p.z}, shape.params) < 0.f)
                        keep = false;
                });
            }
    
            if (!keep)
            {
                count++;
                p.x = std::numeric_limits<float>::quiet_NaN();
                p.y = std::numeric_limits<float>::quiet_NaN();
                p.z = std::numeric_limits<float>::quiet_NaN();
            }

            newCloud->push_back(p);
        }
    
        if (calibration.shapes.size() > 0)
            clipped = newCloud;
    
        return clipped;
    }

    /*********/
    template <typename T>
    bool Filter::isInShapes(const T& point, const CalibrationParams& calibration)
    {
        if (calibration.shapes.size() == 0)
            return true;

        bool keep = false;
        
        for (auto& shape : calibration.shapes)
        {
            if (!shape.include || keep == true)
                continue;
        
            if (_distanceFunctions.find(shape.name) == _distanceFunctions.end())
                continue;
        
            if (_distanceFunctions[shape.name]({point.x, point.y, point.z}, shape.params) <= 0.f)
                keep = true;
        }
        
        if (keep)
        {
            for (auto& shape : calibration.shapes)
            {
                if (shape.include)
                    continue;
        
                if (_distanceFunctions.find(shape.name) == _distanceFunctions.end())
                    continue;
        
                if (_distanceFunctions[shape.name]({point.x, point.y, point.z}, shape.params) <= 0.f)
                    keep = false;
            }
        }

        return keep;
    }
}
#endif // __FILTER_H__
