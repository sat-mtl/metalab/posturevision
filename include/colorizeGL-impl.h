/*
 * Copyright (C) 2015 Emmanuel Durand
 *
 * This file is part of posturevision. 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */

/*
 * @colorizeGL-impl.h
 * The ColorizeGL implementation class, and utility classes
 */

#ifndef __POSTURE_COLORIZEGL_IMPL__
#define __POSTURE_COLORIZEGL_IMPL__

#include <mutex>
#include <vector>

#include <glm/glm.hpp>

#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/PolygonMesh.h>
#include <pcl/TextureMesh.h>
#include <pcl/surface/texture_mapping.h>

#include "config.h"
#include "constants.h"
#include "glEngine.h"
#include "pointcloudserializer.h"
#include "meshserializer.h"
#include "calibrationreader.h"
#include "shader.h"

namespace posture
{
    /*************/
    //! ColorizeGLImpl class, which computes texture coordinates for meshes and prepares a composite texture map given camera information 
    class ColorizeGLImpl
    {
        public:
            /**
             * \brief Constructor
             */
            ColorizeGLImpl();

            /**
             * \brief Destructor
             */
            ~ColorizeGLImpl();

            explicit operator bool() const
            {
                return _ready;
            }

            /**
             * \brief Get the objects texture
             * \param width reference width of the texture
             * \param height reference height of the texture
             * \return the texture
             */
            std::vector<unsigned char> getTexture(unsigned int& width, unsigned int& height);

            /**
             * \brief Get the serialized textured mesh
             * \return serialized textured mesh
             */
            unsigned long long getTexturedMesh(std::vector<unsigned char>& mesh, bool threaded = true);
            unsigned long long getTexturedMesh(pcl::TextureMesh::Ptr mesh);

            /**
             * \brief Set the path where to find the calibration file
             * \param calibrations Calibration parameters for the cameras
             */
            void setCalibration(const std::vector<CalibrationParams>& calibrations)
            {
                std::unique_lock<std::mutex> lock(_mutex);
                _calibrationParams = calibrations;
            }

            /**
             * \brief Set output mesh compression
             * \param active true if the mesh is compressed
             */
            void setCompressMesh(bool active) {_meshSerializer.setCompress(active);}

            /**
             * \brief Set the input mesh and the rgb images
             * \param mesh Serialized input mesh
             * \param images Vector of rgb (unsigned char) images
             * \param dims Vector of dimensions (width, height, channels) of the input images
             */
            void setInput(const std::vector<unsigned char>& mesh, const std::vector<std::vector<unsigned char>>& images, const std::vector<std::vector<unsigned int>>& dims);
            void setInput(pcl::TextureMesh::Ptr mesh, const std::vector<std::vector<unsigned char>>& images, const std::vector<std::vector<unsigned int>>& dims);
            void setInput(pcl::PolygonMesh::Ptr mesh, const std::vector<std::vector<unsigned char>>& images, const std::vector<std::vector<unsigned int>>& dims);
            void setInput(std::vector<pcl::PolygonMesh::Ptr> multimesh, const std::vector<std::vector<unsigned char>>& images, const std::vector<std::vector<unsigned int>>& dims);

        private:
            std::mutex _mutex;
            bool _ready {false};
            
            std::vector<CalibrationParams> _calibrationParams;
            bool _computeTexCoords {false};
            bool _threaded {false}; // This is updated according to input datatype, if input is not serialized

            // GL stuff
            std::shared_ptr<GLEngine> _glEngine;
            std::shared_ptr<Shader> _shaderVisibility;
            std::shared_ptr<Shader> _shaderComputeTexCoords;
            vector<int> _glResolution {1024, 1024};

            // PCL stuff
            MeshSerializer _meshSerializer;

            unsigned long long _timestamp {0};
            std::vector<std::vector<unsigned char>> _images {};
            std::vector<std::vector<unsigned int>> _dims {};
            pcl::TextureMesh::Ptr _mesh {nullptr};
            // pcl::TextureMesh::Ptr _textureMesh {nullptr}; unused?
            vector<pcl::TextureMesh::Ptr> _multiBlankMesh {};

            std::vector<unsigned char> _mergedImage {};
            unsigned int _mergedWidth, _mergedHeight;

            unsigned long long computeTextureCoordinates(pcl::TextureMesh::Ptr& inputMesh, pcl::TextureMesh::Ptr& outputMesh);
            void mergeImages();

            glm::mat4 getTransformationForCamera(unsigned int index);
    };

} // end of namespace

#endif // __POSTURE_COLORIZEGL_IMPL__
