#ifndef __POSTURE_CONSTANTS_H__
#define __POSTURE_CONSTANTS_H__

#ifdef DEBUG
#define TRACE(x) x
#else
#define TRACE(x)
#endif

#define DEVICES_PATH "devices.xml"
#define CALIBRATION_FILE "default.kvc"

#define XML_CAMERA_TAG "Camera"
#define XML_CALIBRATION_TAG "Calibration"
#define XML_POINTCLOUD_TAG "PointCloud"
#define XML_TRANSLATION_TAG "Translation"
#define XML_ROTATION_TAG "Rotation"
#define XML_CLIPPING_TAG "Clipping"
#define XML_SHAPE_TAG "Shape"

#define XML_MANUFACTURER "Manufacturer"
#define XML_PRODUCT "Product"

#define MSG_TIMEOUT 3000

#endif // __POSTURE_CONSTANTS_H__

