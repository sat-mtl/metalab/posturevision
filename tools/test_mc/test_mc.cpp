#include <test_mc.h>

using namespace std;
using namespace pcl;
using namespace posture;
using namespace Eigen;

void DeviceArrayToPolygonMesh (const gpu::DeviceArray<PointXYZ>& Array, PolygonMesh::Ptr Mesh)
{
    vector<PointXYZ> interm1;
    PointCloud<PointXYZ> interm2;

    interm1.resize(Array.size()); // sinon interm1.data() = 0
    Array.download(interm1.data());
    for (int i=0; i<interm1.size(); i++)
    {
        interm2.push_back(interm1[i]);
    }

    Mesh->polygons.clear();
    toPCLPointCloud2 (interm2, Mesh->cloud);
    Mesh->polygons.resize(interm2.size()/3);
    for (int i=0; i<interm2.size()/3; i++)
    {
        Mesh->polygons[i].vertices.push_back(3*i);
        Mesh->polygons[i].vertices.push_back(3*i+1);
        Mesh->polygons[i].vertices.push_back(3*i+2);
    }
}

MC_GPU::MC_GPU()
{
    _resolution << 32, 32, 32;

    _calibrationReader = unique_ptr<posture::CalibrationReader>(new posture::CalibrationReader("default.kvc"));
    auto calibrations = _calibrationReader->getCalibrationParams();

    _cam1.setCaptureMode(ZCamera::CaptureMode::QQVGA_30Hz); // for Primesense camera
//    _cam1.setCaptureMode(ZCamera::CaptureMode::VGA_30Hz); // for Asus Camera
    _cam1.setCalibration(calibrations[0]);
    _cam1.setDeviceIndex(0);

    _cam2.setCaptureMode(ZCamera::CaptureMode::QQVGA_30Hz); // for Primesense camera
//    _cam2.setCaptureMode(ZCamera::CaptureMode::VGA_30Hz); // for Asus Camera
    _cam1.setCalibration(calibrations[1]);
    _cam1.setDeviceIndex(1);

    _cam3.setCaptureMode(ZCamera::CaptureMode::QQVGA_30Hz); // for Primesense camera
//    _cam3.setCaptureMode(ZCamera::CaptureMode::VGA_30Hz); // for Asus Camera
    _cam1.setCalibration(calibrations[2]);
    _cam1.setDeviceIndex(2);

    _mesh_creator.setGridResolution(10);
    _mesh_creator.setCalibration(calibrations);
    _mesh_creator.setGridSize(0.3);
    _mesh_creator.setDepthMapNbr(3);

    _cloud = boost::make_shared<PointCloud<PointXYZRGBNormal>>();
    _merge.setCalibration(calibrations);
    _merge.setCloudNbr (3);

    _triangle_buffer.create(3*3*_resolution(0)*_resolution(1)*_resolution(2));
}

MC_GPU::~MC_GPU()
{
}

void MC_GPU::run ()
{
    _merge.start();
    _cam1.setCallbackCloud([&] (void*, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud){
        CloudCallback1 (cloud);
    }, nullptr);

    _cam1.setCallbackDepth([&] (void*, std::vector<unsigned char>& depth, int width, int height) {
        DepthCallback1 (depth, width, height);
    }, nullptr);

    _cam2.setCallbackCloud([&] (void*, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud){
        CloudCallback2 (cloud);
    }, nullptr);

    _cam2.setCallbackDepth([&] (void*, std::vector<unsigned char>& depth, int width, int height) {
        DepthCallback2 (depth, width, height);
    }, nullptr);

    _cam3.setCallbackCloud([&] (void*, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud){
        CloudCallback3 (cloud);
    }, nullptr);

    _cam3.setCallbackDepth([&] (void*, std::vector<unsigned char>& depth, int width, int height) {
        DepthCallback3 (depth, width, height);
    }, nullptr);

    _cam1.start();
    _cam2.start();
    _cam3.start();
    while (!_disp.wasStopped())
    {}
    _merge.stop();
    _cam1.stop();
    _cam2.stop();
    _cam3.stop();

    cout << "out" << endl;
}

void MC_GPU::CloudCallback1 (pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
{
//    if (!_cloudMutex.try_lock())
//       return;

    _calibrationReader->loadCalibration("default.kvc");
    auto calibrations = _calibrationReader->getCalibrationParams();
    _cam1.setCalibration(calibrations[0]);

    _merge.setCalibration(calibrations);
    _merge.setInputCloud(0, cloud);
    _merge.getCloud(_cloud);
    _disp.setInputCloud(_cloud);
}

void MC_GPU::DepthCallback1 (std::vector<unsigned char>& depth, int width, int height)
{
    if (!_depthMutex.try_lock())
        return;

//#if WITH_OPENNI2
//        memcpy(&depth[0], depthAsInt->getData(), depth.size());
//#else
//        memcpy(&depth[0], reinterpret_cast<const unsigned char*>(&depthAsInt[0]), depth.size());
//#endif

    _mesh_creator.setInputDepthMap(0, depth, width, height);

    PolygonMesh::Ptr Mesh;
    Mesh = boost::make_shared<PolygonMesh> ();

    _mesh_creator.getMesh(Mesh);
    _disp.setPolygonMesh(Mesh);

    _depthMutex.unlock();
}

void MC_GPU::CloudCallback2 (pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
{
//    if (!_cloudMutex.try_lock())
//       return;

    _calibrationReader->loadCalibration("default.kvc");
    auto calibrations = _calibrationReader->getCalibrationParams();
    _cam2.setCalibration(calibrations[1]);

    _merge.setCalibration(calibrations);
    _merge.setInputCloud (1, cloud);
    _merge.getCloud(_cloud);
    _disp.setInputCloud(_cloud);
}

void MC_GPU::DepthCallback2 (std::vector<unsigned char>& depth, int width, int height)
{
    if (!_depthMutex.try_lock())
        return;

//#if WITH_OPENNI2
//        memcpy(&depth[0], depthAsInt->getData(), depth.size());
//#else
//        memcpy(&depth[0], reinterpret_cast<const unsigned char*>(&depthAsInt[0]), depth.size());
//#endif

    _mesh_creator.setInputDepthMap(1, depth, width, height);

    PolygonMesh::Ptr Mesh;
    Mesh = boost::make_shared<PolygonMesh> ();

    _mesh_creator.getMesh(Mesh);
    _disp.setPolygonMesh(Mesh);

    _depthMutex.unlock();
}

void MC_GPU::CloudCallback3 (pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
{
//    if (!_cloudMutex.try_lock())
//       return;
    _calibrationReader->loadCalibration("default.kvc");
    auto calibrations = _calibrationReader->getCalibrationParams();
    _cam3.setCalibration(calibrations[2]);

    _merge.setCalibration(calibrations);
    _merge.setInputCloud (2, cloud);
    _merge.getCloud(_cloud);
    _disp.setInputCloud(_cloud);
}

void MC_GPU::DepthCallback3 (std::vector<unsigned char>& depth, int width, int height)
{
    if (!_depthMutex.try_lock())
        return;

//#if WITH_OPENNI2
//        memcpy(&depth[0], depthAsInt->getData(), depth.size());
//#else
//        memcpy(&depth[0], reinterpret_cast<const unsigned char*>(&depthAsInt[0]), depth.size());
//#endif

    _mesh_creator.setInputDepthMap(2, depth, width, height);

    PolygonMesh::Ptr Mesh;
    Mesh = boost::make_shared<PolygonMesh> ();

    _mesh_creator.getMesh(Mesh);
    _disp.setPolygonMesh(Mesh);

    _depthMutex.unlock();
}

int main(int argc, char** argv)
{
    MC_GPU test;
    test.run ();
}
