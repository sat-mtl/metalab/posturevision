#ifndef TEST_POSTUREGL
#define TEST_POSTUREGL

#include <atomic>
#include <condition_variable>
#include <iostream>
#include <memory>
#include <mutex>
#include <thread>
#include <vector>

#include <boost/make_shared.hpp>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/PolygonMesh.h>

#include <calibrationreader.h>
#include <colorizeGL.h>
#include <detect.h>
#include <display.h>
#include <meshmerger.h>
#include <pointcloudmerger.h>
#include <register.h>
#include <solidify.h>
#include <zcamera.h>
#include <solidifyGPU.h>
#include <sculptGL.h>
#include <pointcloudserializer.h>
#include <filter.h>

#include <shmdata/writer.hpp>
#include <shmdata/console-logger.hpp>

#define CAMERA_NBR 1

/*************/
class App
{
    public:
        App();
        ~App();

        void run();
        void stop();

    private:
        bool _continue {true};

        std::unique_ptr<posture::CalibrationReader> _calibrationReader;
        std::unique_ptr<posture::PointCloudMerger> _merger;
        //std::unique_ptr<posture::CalibrationReader> _calibrationCamera;
        //std::unique_ptr<posture::CalibrationReader> _calibrationMesh;

        std::unique_ptr<posture::Register> _register;
        std::vector<std::unique_ptr<posture::ZCamera>> _cameras;
        posture::ColorizeGL _colorizer;
        posture::SolidifyGPU _mesher;
        std::shared_ptr<posture::SculptGL> _sculpter;
        posture::PointCloudSerializer<pcl::PointXYZRGBNormal> _cloudSerializer;

        std::shared_ptr<shmdata::Writer> _meshWriter {nullptr};
        std::shared_ptr<shmdata::Writer> _imgWriter {nullptr};
        std::shared_ptr<shmdata::Writer> _cloudWriter {nullptr};

        std::mutex _cameraMutex;
        std::vector<bool> _camerasUpdated {};
        std::vector<std::vector<uint8_t>> _images;
        std::vector<std::vector<uint32_t>> _dims;

        std::condition_variable _updateCondition;

        std::thread _registeringThread;
        std::atomic_bool _isRegistering {false};

        posture::Filter _filter;
        pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr _cloud;
        std::vector<char> _serializedCloud;

        std::chrono::time_point<std::chrono::system_clock> _start, _end;

        bool all(const std::vector<bool>& status);
        void zero(std::vector<bool>& status);

        void callbackDepth(uint32_t camId, std::vector<unsigned char>& depth, int width, int height);
        void callbackRGB(uint32_t camId, std::vector<unsigned char>& depth, int width, int height);
        void callbackCloud(uint32_t camId, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);
};

#endif // TEST_POSTUREGL
