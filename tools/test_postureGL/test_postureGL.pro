QMAKE_CXXFLAGS += -std=gnu++11
TARGET = test_postureGL
TEMPLATE = app

ROOT_DIR = ../..

HEADERS += test_postureGL.h
SOURCES += test_postureGL.cpp

INCLUDEPATH += $$ROOT_DIR/include
INCLUDEPATH += /usr/local/include/pcl-1.8
INCLUDEPATH += /usr/include/eigen3
INCLUDEPATH += /usr/include/libxml2
INCLUDEPATH += /usr/local/include/shmdata-1.0

LIBS += -lposture-0.4
LIBS += -lX11 -lXi -lXrandr -lXxf86vm -lXinerama -lXcursor
LIBS += -lboost_system -lpcl_io -lpcl_gpu_octree
LIBS += -L/usr/local/lib -lglfw
LIBS += -L/usr/local/lib -lshmdata-1.0

DESTDIR = .
OBJECTS_DIR = build
