#ifndef TEST_SCULPTGL_H
#define TEST_SCULPTGL_H

#include <memory>
#include <iostream>
#include <mutex>
#include <vector>
#include <Eigen/Core>

#include <boost/make_shared.hpp>

#include <detect.h>
#include <display.h>
#include <zcamera.h>
#include <solidify.h>
#include <sculptGL.h>

class Test_SculptGL
{
public:
    Test_SculptGL();
    ~Test_SculptGL();
    void run();

    void setWireframe(bool wireframe)
    {
        _wireframe = wireframe;
        _display.setWireframe(_wireframe);
    }
    void showPoints(bool show) {_showPoints = show;}
    void refineMesh(bool refine) {_refineMesh = refine;}

private:
    void cloudCB(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);
    void depthCB(std::vector<unsigned char>& depth, int width, int height);

    void getGeometry();

    bool _wireframe {false};
    bool _showPoints {false};
    bool _refineMesh {false};

    std::mutex _cloudMutex;
    std::mutex _depthMutex;

    posture::ZCamera _camera;
    posture::Display _display;

    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr _cloud;
    pcl::PolygonMesh::Ptr _mesh;

    std::shared_ptr<posture::Solidify> _meshCreator;
    std::shared_ptr<posture::SculptGL> _sculptGL;
};

#endif // TEST_SCULPTGL_H
