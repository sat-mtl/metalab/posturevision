#include <test_sculptGL.h>

using namespace std;
using namespace pcl;
using namespace posture;
using namespace Eigen;

#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/exceptions.h>

Test_SculptGL::Test_SculptGL()
{
    _sculptGL = make_shared<posture::SculptGL>();
    _sculptGL->setPointSeparation(0.1f);
    _sculptGL->setMaxNeighbors(1);
}

Test_SculptGL::~Test_SculptGL()
{
}

void Test_SculptGL::run ()
{
    _cloud = boost::make_shared<PointCloud<PointXYZRGBNormal>>();
    _mesh = boost::make_shared<PolygonMesh>();
    getGeometry();
    std::clog << "polygons (before): " << _mesh->polygons.size() << std::endl;
    _sculptGL->setInputCloud(_cloud);
    _sculptGL->setInputMesh(_mesh);
    _sculptGL->getMesh(_mesh);
    std::clog << "polygons (after): " << _mesh->polygons.size() << std::endl;

    _display.setInputCloud(_cloud);
    _display.setPolygonMesh(_mesh);

    while (!_display.wasStopped()) {}
}

// Testing the whole pipeline with simple geometries (cloud and mesh)
void Test_SculptGL::getGeometry()
{
    uint pointCount = 250;
    _cloud->width = ceil(sqrt(static_cast<double>(pointCount)));
    _cloud->height = _cloud->width;
    _cloud->is_dense = false;
    _cloud->reserve(pointCount);

    srand(time(NULL));
    for (uint i = 0; i < pointCount; i++)
    {
        pcl::PointXYZRGBNormal p;
        p.x = ((float) rand() / (RAND_MAX)) - 0.5f;
        p.y = ((float) rand() / (RAND_MAX)) - 0.5f;
        p.z = ((float) rand() / (RAND_MAX)) * 0.1 - 0.05f;
        p.r = 255.0f;
        p.g = 255.0f;
        p.b = 255.0f;
        p.normal_x = 0.0f;
        p.normal_y = 0.0f;
        p.normal_z = 1.0f;
        _cloud->points.push_back(p);
    }

    uint verticesCount = 4;
    pcl::PointCloud<pcl::PointXYZ> cloud = pcl::PointCloud<pcl::PointXYZ>();
    cloud.width = verticesCount;
    cloud.height = 1;
    cloud.is_dense = false;
    cloud.reserve(verticesCount);
    //Vertices for the first and the second triangle
    cloud.points.push_back(pcl::PointXYZ(-0.5f, -0.5f, 0.0f));
    cloud.points.push_back(pcl::PointXYZ(0.5f, -0.5f, 0.0f));
    cloud.points.push_back(pcl::PointXYZ(-0.5f, 0.5f, 0.0f));
    cloud.points.push_back(pcl::PointXYZ(0.5f, 0.5f, 0.0f));

    _mesh->polygons.clear();

    pcl::Vertices triangle;
    triangle.vertices.push_back(0);
    triangle.vertices.push_back(1);
    triangle.vertices.push_back(3);
    _mesh->polygons.push_back(triangle);

    triangle.vertices.clear();
    triangle.vertices.push_back(0);
    triangle.vertices.push_back(2);
    triangle.vertices.push_back(3);
    _mesh->polygons.push_back(triangle);
    pcl::toPCLPointCloud2(cloud, _mesh->cloud);
}

void Test_SculptGL::cloudCB(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud)
{
    lock_guard<mutex> lock(_cloudMutex);
}

void Test_SculptGL::depthCB(vector<unsigned char>& depth, int width, int height)
{
    lock_guard<mutex> lock(_depthMutex);
}

int main(int argc, char** argv)
{
    if (argc == 1 || argc > 4)
        std::cout << "Usage: " << argv[0] << " [wireframe {0,1}] [showPoints {0,1}] [refineMesh {0,1}]" << std::endl;
    else
    {
        Test_SculptGL testSculptGL;

        testSculptGL.setWireframe(std::stoi(argv[1]) == 1);
        if (argc > 2)
            testSculptGL.showPoints(std::stoi(argv[2]) == 1);
        if (argc > 3)
            testSculptGL.refineMesh(std::stoi(argv[3]) == 1);

        testSculptGL.run();
    }
}
