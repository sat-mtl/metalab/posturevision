QMAKE_CXXFLAGS += -std=gnu++11
TARGET = test_sculptGL
TEMPLATE = app

ROOT_DIR = ../..

HEADERS += test_sculptGL.h
SOURCES += test_sculptGL.cpp

INCLUDEPATH += $$ROOT_DIR/include /usr/local/include/pcl-1.8 /usr/include/eigen3 /usr/include/libxml2
LIBS += -lposture-0.4
LIBS += -lX11 -lXi -lXrandr -lXxf86vm -lXinerama -lXcursor
LIBS += -lboost_system -lpcl_io -lpcl_gpu_octree
LIBS += -L/usr/local/lib -lglfw

DESTDIR = .
OBJECTS_DIR = build
