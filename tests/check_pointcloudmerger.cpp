#include <cassert>
#include <chrono>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>

#include <boost/make_shared.hpp>

#include "calibrationreader.h"
#include "zcamera.h"
#include "pointcloudmerger.h"

using namespace std;

/*************/
int main(int argc, char** argv)
{
    auto cloud1 = boost::make_shared<pcl::PointCloud<pcl::PointXYZRGBNormal>>();
    auto cloud2 = boost::make_shared<pcl::PointCloud<pcl::PointXYZRGBNormal>>();
    auto mergedCloud = boost::make_shared<pcl::PointCloud<pcl::PointXYZRGBNormal>>();

    auto cloud1Serialised = vector<char>();
    auto cloud2Serialized = vector<char>();
    auto mergedCloudSerialized = vector<char>();;

    auto calibrationReader = posture::CalibrationReader("default.kvc");
    auto merger = posture::PointCloudMerger();
    merger.setCloudNbr(2);
    merger.setCalibration(calibrationReader.getCalibrationParams());
    merger.start();

    posture::ZCamera::getNoise (1, 1, 1, 512, cloud1);
    posture::ZCamera::getNoise (1, 1, 1, 1024, cloud2);

    merger.setInputCloud(0, cloud1);
    merger.setInputCloud(1, cloud2);

    merger.getCloud (mergedCloud);

    assert(mergedCloud->size() > 0);
    assert(mergedCloud->size() == cloud1->size() + cloud2 ->size());

    // serialised test
    posture::ZCamera::getNoise (1, 1, 1, 512, cloud1Serialised);
    posture::ZCamera::getNoise (1, 1, 1, 1024, cloud2Serialized);

    merger.setInputCloud(0, cloud1Serialised, false);
    merger.setInputCloud(1, cloud2Serialized, false);

    merger.getCloud (mergedCloudSerialized);

    assert(mergedCloudSerialized.size() > 0);
    assert(mergedCloudSerialized.size() == cloud1Serialised.size() + cloud2Serialized.size() - 8);

    return 0;
}
