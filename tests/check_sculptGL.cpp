#include <cassert>
#include <chrono>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>

#include <boost/make_shared.hpp>

#include <pcl/io/obj_io.h>

#include "zcamera.h"
#include "sculptGL.h"

using namespace std;

/*************/
int main(int argc, char** argv)
{
    std::clog << __FUNCTION__ << "(" << argc << ", " << argv << ")" << std::endl;

    auto mesh = boost::make_shared<pcl::PolygonMesh>();
    posture::ZCamera::getRandomMesh(1, 1, 1, 1, mesh);
    auto sculptGL = unique_ptr<posture::SculptGL>(new posture::SculptGL());
    sculptGL->setInput(mesh);
    sculptGL->getMesh(mesh);
    if (mesh->polygons.size() > 0)
        pcl::io::saveOBJFile("mesh.obj", *mesh.get());

    assert(mesh->polygons.size() > 0);

    // serialised mesh
    /*auto meshSerialized = vector<unsigned char>();
    posture::ZCamera::getRandomMesh(1, 1, 1, 1, meshSerialized);
    sculptGL->setInput(meshSerialized);
    sculptGL->getMesh(meshSerialized);

    assert(meshSerialized.size() > 0);*/

    return 0;
}
