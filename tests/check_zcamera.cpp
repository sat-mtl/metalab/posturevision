#include <cassert>
#include <chrono>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>

#include <boost/make_shared.hpp>

#include "zcamera.h"

using namespace std;

/*************/
int main(int argc, char** argv)
{
    auto zcamera = posture::ZCamera();
    zcamera.setDeviceIndex(0);
    zcamera.setBuildMesh(true);
    zcamera.setCaptureMode(posture::ZCamera::CaptureMode_QQVGA_30Hz);

    auto cloudSerialized = vector<char>();
    mutex cloudSerializedMutex;
    condition_variable cloudSerializedCondition;

    auto cloudPtr = boost::make_shared<pcl::PointCloud<pcl::PointXYZRGBNormal>>();
    mutex cloudMutex;
    condition_variable cloudCondition;

    zcamera.setCallbackCloud([&](void*, vector<char>&& cloud) {
        if (!cloudSerializedMutex.try_lock())
            return;
        cloudSerialized = cloud;
        cloudSerializedCondition.notify_all();
        cloudSerializedMutex.unlock();
    }, nullptr);

    zcamera.setCallbackCloud([&](void*, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud) {
        if (!cloudMutex.try_lock())
            return;
        *cloudPtr = *cloud;
        cloudCondition.notify_all();
        cloudMutex.unlock();
    }, nullptr);

    try
    {
        zcamera.start();
    }
    catch (...)
    {
        cout << "Cannot connect to the camera, skipping" << endl;
    }

    if (zcamera.isReady())
    {
        unique_lock<mutex> cloudSerializedLock(cloudSerializedMutex);
        cloudSerializedCondition.wait(cloudSerializedLock);

        unique_lock<mutex> cloudLock(cloudMutex);
        cloudCondition.wait(cloudLock);

        assert(cloudSerialized.size() > 0);
        assert(cloudPtr->size() > 0);

        zcamera.stop();
    }

    return 0;
}
