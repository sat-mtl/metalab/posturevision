QMAKE_CXXFLAGS += -std=gnu++11
TARGET = check_sculptGL
TEMPLATE = app

ROOT_DIR = ..

SOURCES += check_sculptGL.cpp #$$ROOT_DIR/posture/glEngine.cpp

INCLUDEPATH += $$ROOT_DIR/include /usr/local/include/pcl-1.8 /usr/include/eigen3
LIBS += -lposture-0.4
LIBS += -lX11 -lXi -lXrandr -lXxf86vm -lXinerama -lXcursor
LIBS += -lboost_system -lpcl_io -lpcl_gpu_octree

DESTDIR = .
OBJECTS_DIR = build
