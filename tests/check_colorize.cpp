#include <cassert>
#include <chrono>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>

#include <boost/make_shared.hpp>

#include "zcamera.h"
#include "colorize.h"

using namespace std;

/*************/
int main(int argc, char** argv)
{
    auto mesh = boost::make_shared<pcl::PolygonMesh>();
    auto coloMesh = boost::make_shared<pcl::TextureMesh>();
    vector<unsigned char> image = vector<unsigned char>();
    vector<vector<unsigned char>> images = vector<vector<unsigned char>>();
    vector<unsigned int> dim;
    vector<vector<unsigned int>> dims;

    posture::ZCamera::getRandomMesh(1, 1, 1, 12, mesh);

    dim = {256, 256, 3};
    for (int i=0; i<256*256; i++)
    {
        image.push_back((int)rand()/RAND_MAX*255);
        image.push_back((int)rand()/RAND_MAX*255);
        image.push_back((int)rand()/RAND_MAX*255);
    }
    images.push_back(image);
    dims.push_back(dim);

    auto colorizeGL = unique_ptr<posture::Colorize>(new posture::Colorize ());

    colorizeGL->setInput(mesh, images, dims);

    unsigned int width = 0;
    unsigned int height = 0;
    colorizeGL->getTexturedMesh(coloMesh);
    image = colorizeGL->getTexture(width, height);

    assert(width == 256);
    assert(height == 256);
    assert(image.size() > 0);
    assert(coloMesh->tex_polygons.size() > 0);
    assert(coloMesh->tex_polygons[0].size() > 0);

    // serialised mesh
    auto meshSerialized = vector<unsigned char>();
    auto coloMeshSerialized = vector<unsigned char>();

    posture::ZCamera::getRandomMesh(1, 1, 1, 12, meshSerialized);

    colorizeGL->setInput(meshSerialized, images, dims);

    width = 0;
    height = 0;
    colorizeGL->getTexturedMesh(coloMeshSerialized);
    image = colorizeGL->getTexture(width, height);

    assert(width == 256);
    assert(height == 256);
    assert(image.size() > 0);
    assert(coloMeshSerialized.size() > 0);

    return 0;
}
