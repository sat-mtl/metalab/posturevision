#include <cassert>
#include <chrono>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>

#include <boost/make_shared.hpp>

#include "zcamera.h"
#include "solidify.h"

using namespace std;

/*************/
int main(int argc, char** argv)
{
    auto pointCloud = boost::make_shared<pcl::PointCloud<pcl::PointXYZRGBNormal>>();
    auto returnMesh = boost::make_shared<pcl::PolygonMesh>();

    auto pointCloudSerialized = std::vector<char>();
    auto returnMeshSerialized = std::vector<unsigned char>();

    auto solid = posture::Solidify ();

    posture::ZCamera::getNoise (1.0f, 1.0f, 1.0f, 512, pointCloud);

    solid.setInputCloud(pointCloud);
    solid.getMesh (returnMesh);

    assert(returnMesh->polygons.size() > 0);

    posture::ZCamera::getNoise (1.0f, 1.0f, 1.0f, 512, pointCloudSerialized);

    solid.setInputCloud (pointCloudSerialized, false);
    solid.getMesh (returnMeshSerialized);

    assert(returnMeshSerialized.size() > 0);

    if (argc == 2)
    {
        //posture::ZCamera::getEmptyCubeNoise(0.1f, 0.1f, 0.1f, pointCloud);
        posture::ZCamera::loadFromPLY(argv[1], pointCloud);

        solid.setSaveMesh(true);
        solid.setInputCloud(pointCloud);
        solid.getMesh(returnMesh);

        assert(returnMesh->polygons.size() > 0);
    }

    return 0;
}
